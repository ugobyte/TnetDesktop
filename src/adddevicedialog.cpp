#include "adddevicedialog.h"
#include "ui_adddevicedialog.h"

/**
 * @brief AddDeviceDialog::AddDeviceDialog
 * @param parent
 */
AddDeviceDialog::AddDeviceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDeviceDialog)
{
    ui->setupUi(this);
//S    setAttribute(Qt::WA_DeleteOnClose, true);
}

/**
 * @brief AddDeviceDialog::~AddDeviceDialog
 */
AddDeviceDialog::~AddDeviceDialog()
{
    delete ui;
}

/**
 * @brief AddDeviceDialog::getIpAddres
 * @return
 */
const QString AddDeviceDialog::getIpAddres()
{
    return ui->lineEdit->text();
}

/**
 * @brief AddDeviceDialog::on_cancelButton_clicked
 */
void AddDeviceDialog::on_cancelButton_clicked()
{
    reject();
}

/**
 * @brief AddDeviceDialog::on_connectButton_clicked
 */
void AddDeviceDialog::on_connectButton_clicked()
{
    accept();
}
