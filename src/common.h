#ifndef COMMON_H
#define COMMON_H

// icons
#define ICON_16_TICK                        ":/icons/16/tick.png"
#define ICON_16_CROSS                       ":/icons/16/cross.png"

#define ICON_AC                             ":/icons/48/ac.png"
#define ICON_AUDIO                          ":/icons/48/audio.png"
#define ICON_BAT_FULL                       ":/icons/48/batfull.png"
#define ICON_BAT_GOOD                       ":/icons/48/batgood.png"
#define ICON_BAT_LOW                        ":/icons/48/batlow.png"
#define ICON_BAT_MED                        ":/icons/48/batmed.png"
#define ICON_STAT_A0                        ":/icons/48/tick.png"
#define ICON_RED_CROSS                      ":/icons/48/redcross.png"
#define ICON_STAT_A1                        ":/icons/48/bluealarm.png"
#define ICON_STAT_A2                        ":/icons/48/redalarm.png"
#define ICON_CANCEL                         ":/icons/48/cancel.png"
#define ICON_CHECK                          ":/icons/48/checked.png"
#define ICON_UNCHECK                        ":/icons/48/unchecked.png"
#define ICON_DOWN                           ":/icons/48/down.png"
#define ICON_UP                             ":/icons/48/up.png"
#define ICON_PLAY                           ":/icons/48/play.png"
#define ICON_PAUSE                          ":/icons/48/pause.png"
#define ICON_LEFT                           ":/icons/48/left.png"
#define ICON_RIGHT                          ":/icons/48/right.png"
#define ICON_MUTE                           ":/icons/48/mute.png"
#define ICON_TICK                           ":/icons/48/tick.png"
#define ICON_USB                            ":/icons/48/usb.png"
#define ICON_VPN                            ":/icons/48/vpnon.png"
#define ICON_NOVPN                          ":/icons/48/vpnoff.png"
#define ICON_WIFI                           ":/icons/48/wifi.png"
#define ICON_WIFI_MED                       ":/icons/48/wifimed.png"
#define ICON_WIFI_LOW                       ":/icons/48/wifilow.png"
#define ICON_ETH                            ":/icons/48/ethernet.png"
#define ICON_CELL                           ":/icons/48/3g.png"
#define ICON_SLIDER_OFF                     ":/icons/48/slideoff.png"
#define ICON_SLIDER_ON                      ":/icons/48/slideon.png"
#define ICON_SECURITY                       ":/icons/48/security.png"
#define ICON_LOADER                         ":/icons/48/loader.png"
#define ICON_THERMO                         ":/icons/48/thermometer.png"
#define ICON_CONFIRM_OK                     ":/icons/48/confirmok.png"
#define ICON_CONFIRM_KO                     ":/icons/48/confirmko.png"
#define ICON_EMAIL                          ":/icons/48/email.png"
#define ICON_FINGER_PRINT                   ":/icons/48/fingerprint.png"
#define ICON_PASSWORD                       ":/icons/48/password.png"
#define ICON_EDIT                           ":/icons/48/edit.png"
#define ICON_FOLDER                         ":/icons/48/folder.png"

#define ICON_SENSOR_LINK                    ":/icons/32/link.png"
#define ICON_SENSOR_A1                      ":/icons/32/bluealarm.png"
#define ICON_SENSOR_A2                      ":/icons/32/redalarm.png"

#define ICON_TRAY_NO_ALARM                  ":/icons/22/trayA0.jpg"
#define ICON_TRAY_ALARM1                    ":/icons/22/trayA1.jpg"
#define ICON_TRAY_ALARM2                    ":/icons/22/trayA2.jpg"

#define GIF_SPINNER                         ":/icons/Spinner.gif"
#define ICON_LOGO                           ":/icons/tnetlogo.jpg"



#include <QChar>
const QChar degreeChar(0260);


enum SessionAction { saIgnored,
                     saRestart,
                     saResume,
                     saCreateNew };

enum AuthAction {authActionGet,
                 authActionUpdate};

typedef struct {
    bool refPlotVisible;
    bool diffPlotVisible;
    bool a1PlotVisible;
    bool a2PlotVisible;
    bool hGridVisible;
    bool vGridVisible;
    bool legendVisible;
} ChartSettings_t;

typedef struct {
    bool degreeCelcius;
    quint32 screenSaverTime;
    ChartSettings_t chartSettings;
} SettingsContext_t;

struct SensorContext {
    quint8 pos;
    QString name;
    quint8 refPos;
    QString refName;
};

struct FilterContext {
    QString sessionNo;
    quint8 totalSensors;
    bool isCurrentSession;
    quint8 selectedSensorIndex;
    bool a1Checked;
    bool a2Checked;
    QList<struct SensorContext> sensorList;
    QString currentDataFile;
};

#endif // COMMON_H
