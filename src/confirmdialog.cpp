#include "confirmdialog.h"
#include "ui_confirmdialog.h"
#include "tnetportal.h"

/**
 * @brief ConfirmDialog::ConfirmDialog
 * @param parent
 */
ConfirmDialog::ConfirmDialog(QWidget *parent, const QString cfmMessage) :
    QWidget(parent),
    ui(new Ui::ConfirmDialog)
{
    ui->setupUi(this);
    // delete on close
    setAttribute( Qt::WA_DeleteOnClose, true );

    // set to parent geometry
    setGeometry(0,0,parent->geometry().width(),parent->geometry().height());

    ui->messageLabel->setText(cfmMessage);
    TG_DEBUG("Confirm dialog constructing");
}

/**
 * @brief ConfirmDialog::~ConfirmDialog
 */
ConfirmDialog::~ConfirmDialog()
{
    TG_DEBUG("Confirm dialog destructing");
    delete ui;
}

/**
 * @brief ConfirmDialog::on_okButton_clicked
 */
void ConfirmDialog::on_okButton_clicked()
{
    emit confirmResult(true);
}

/**
 * @brief ConfirmDialog::on_cancelButton_clicked
 */
void ConfirmDialog::on_cancelButton_clicked()
{
    emit confirmResult(false);
}

