#ifndef CONFIRMDIALOG_H
#define CONFIRMDIALOG_H

#include <QWidget>

namespace Ui {
class ConfirmDialog;
}

class ConfirmDialog : public QWidget
{
    Q_OBJECT

public:
    explicit ConfirmDialog(QWidget *parent = 0, const QString cfmMessage="");
    ~ConfirmDialog();

signals:
    void confirmResult(bool);

private slots:
    void on_okButton_clicked();
    void on_cancelButton_clicked();

private:
    Ui::ConfirmDialog *ui;
};

#endif // CONFIRMDIALOG_H
