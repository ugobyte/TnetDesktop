#include "device.h"
#include "ui_device.h"

#include "virtualkeypaddialog.h"
#include "sensorlistitem.h"

/**
 * @brief Device::Device
 * @param parent
 * @param devicePath
 */
Device::Device(QWidget *parent, TgGateway * gateway) :
    QMainWindow(parent),
    ui(new Ui::Device),
    tggateway(gateway)
{

    ui->setupUi(this);

#if defined(TNET_OLIMEX)
    // create virtual keypad
    vKeypad = new VirtualKeypadDialog;
    vKeypad->setFocusPolicy(Qt::NoFocus);
    vKeypad->setGeometry(0,270,800,210);
#endif

    // connect to auth server to get hash
    authClient = new AuthClient;
    authAction = authActionGet;
    QObject::connect(authClient, SIGNAL(authConnected()), this, SLOT(onAuthConnected()));
    QObject::connect(authClient, SIGNAL(authKey(QByteArray)), this, SLOT(onAuthKeyGetResponse(QByteArray)));
    QObject::connect(authClient, SIGNAL(authResponse(bool)), this, SLOT(onAuthKeyUpdateResponse(bool)));

// connect local for olimex platform and use vpn ip for desktop
#if defined(TNET_OLIMEX)
    authClient->connect("127.0.0.1");
#else
    const Network_t& net = tggateway->getNetwork();
    authClient->connect(net.hamIp);
#endif

    logChart = NULL;
    streamChart = NULL;

    chartMenuHide();

    // gateway
    QObject::connect(tggateway, SIGNAL(streamReceived(TgEvStreamClass)), this, SLOT(onGatewayStreamReceived(TgEvStreamClass)));
    QObject::connect(tggateway, SIGNAL(configRequestResult(bool, QString)), this, SLOT(onGatewayConfigLoaded(bool, QString)));
    QObject::connect(tggateway, SIGNAL(sessionResumeResult(bool,QString)), this, SLOT(onGatewaySessionResume(bool,QString)));
    QObject::connect(tggateway, SIGNAL(sessionRestartResult(bool,QString)), this, SLOT(onGatewaySessionRestart(bool,QString)));
    QObject::connect(tggateway, SIGNAL(tempRecordsReady(bool,QString)), this, SLOT(onGatewayRecordsReady(bool,QString)));
    QObject::connect(tggateway, SIGNAL(wifiScanResultsReady(bool, int)), this, SLOT(onGatewayWifiScanResults(bool,int)));
    QObject::connect(tggateway, SIGNAL(wifiConnectResult(bool,QString)), this, SLOT(onGatewayWifiConnectResult(bool,QString)));

    // for olimex platform, set ip address as localhost
#if defined(TNET_OLIMEX)
    tggateway->setIpAddress("127.0.0.1");
    tggateway->loadConfigGateway();
    tggateway->loadConfigNetwork();
    tggateway->loadConfigTemperature();
    tggateway->connectStream();
#endif

    // datetime
    QObject::connect(&dateTimeTimer, SIGNAL(timeout()), this, SLOT(dateTimeTimerTimeout()));
    dateTimeTimerTimeout();
    dateTimeTimer.start(10000);

    // rotation
    rotationTimer.setInterval(4000);
    QObject::connect(&rotationTimer, SIGNAL(timeout()), this, SLOT(rotationTimerTimeout()));

    // alarm blink
    QObject::connect(&alarmBlinkTimer, SIGNAL(timeout()), this, SLOT(alarmBlinkTimerTimeout()));
    alarmBlinkTimer.setInterval(1205);
    alarmBlinkerOn = true;

    // incomming temperature timer
    incommingTempTimer.setInterval(783);
    QObject::connect(&incommingTempTimer, SIGNAL(timeout()), this, SLOT(incommingTempTimerTimeout()));

    sensorPageHeaderLoaded = false;

    // sensors
    muteButtonActive = false;
    a1CheckBoxChecked = false;
    a2CheckBoxChecked = false;
    playButtonActive = true;
    alarmBlinkerOn = false;
    playButtonState = true;

    clearStatusIcons();

    // do a sync on desktop applications
    // not required on olimex as records on same machine
#if defined(TNET_DESKTOP)
    tggateway->syncRecords();
#endif

    currentStackPage = spNotSet;
    pageChange(spHomePage);
}

/**
 * @brief Device::~Device
 */
Device::~Device()
{
    // delete and QObject::deleteLater() will never throw exception
    // so try catch is useless
    /*try {
        delete authClient;
    } catch (...) {}*/

    if (streamChart)
        delete streamChart;

    if (logChart)
        delete logChart;

#if defined(TNET_OLIMEX)
    // destroy virtual keypad
    delete vKeypad;
#endif

    delete ui;

    TG_DEBUG("Device destructed");
}

/**
 * @brief Device::closeEvent
 * @param event
 */
void Device::closeEvent(QCloseEvent *event)
{
    // accept event
    event->accept();

    // send signal to exporer widget to handle close
    emit userCloseRequest();
}

/**
 * @brief Device::clearStatusIcons
 */
void Device::clearStatusIcons()
{
    ui->StatusAlarmImage->clear();
    ui->StatusConnectImage->clear();
    ui->StatusHostConnect->clear();
    ui->StatusPowerImage->clear();
    ui->StatusVpnImage->clear();
    ui->StatusNotification->clear();
    ui->StatusIncommingTemp->clear();
}

/**
 * @brief Device::onGatewayStreamReceived
 */
void Device::onGatewayStreamReceived(TgEvStreamClass type)
{

    if (type == ecTemp) {
        ui->StatusIncommingTemp->setEnabled(true);
        ui->StatusIncommingTemp->setPixmap(QPixmap(ICON_THERMO));
        incommingTempTimerCount = 0;
        incommingTempTimer.start();
    }

    if (!rotationTimer.isActive() && playButtonActive)
        rotationTimer.start();
    else if (!rotationTimer.isActive() && !playButtonActive)
        popupateSensorList();

    if (!alarmBlinkTimer.isActive())
        alarmBlinkTimer.start();

    // alarm status is updated after every temperature read
    // update now
    updateStatus();
}

/**
 * @brief Device::updateStatus
 */
void Device::updateStatus()
{
    const GatewayId_t id = tggateway->getId();
    ui->hostnameLabel->setText(id.name);

    const Network_t net = tggateway->getNetwork();

    // wan connection ... sometimes hamachi reports logged in but there is no internet connection
    if (net.ham && net.netiface != ctNone)
        ui->StatusVpnImage->setPixmap(QPixmap(ICON_VPN));
    else
        ui->StatusVpnImage->setPixmap(QPixmap(ICON_NOVPN));

    // current network interface
    switch(net.netiface)
    {
       case ctEth:
           ui->StatusConnectImage->setPixmap(QPixmap(ICON_ETH));
           break;

       case ctWifi:
           ui->StatusConnectImage->setPixmap(QPixmap(ICON_WIFI));
           break;

       case ctCell:
           ui->StatusConnectImage->setPixmap(QPixmap(ICON_CELL));
           break;

       case ctNone:
            ui->StatusConnectImage->setPixmap(QPixmap(""));
                break;
        default:
            break;
    }

    const System_t sys = tggateway->getSystem();

    if (sys.ac)
        ui->StatusPowerImage->setPixmap(QPixmap(ICON_AC));
    else {
        if (sys.batlvl >= 95)
            ui->StatusPowerImage->setPixmap(QPixmap(ICON_BAT_FULL));
        else if (sys.batlvl >= 70)
            ui->StatusPowerImage->setPixmap(QPixmap(ICON_BAT_GOOD));
        else if (sys.batlvl >= 40)
            ui->StatusPowerImage->setPixmap(QPixmap(ICON_BAT_MED));
        else if (sys.batlvl >= 10)
            ui->StatusPowerImage->setPixmap(QPixmap(ICON_BAT_LOW));
        else
            ui->StatusPowerImage->setPixmap(QPixmap(ICON_BAT_LOW));
    }

    if (sys.usb)
        ui->StatusHostConnect->setPixmap(QPixmap(ICON_USB));
    else
        ui->StatusHostConnect->setPixmap(QPixmap(""));

    const Session_t session = tggateway->getSession();

    switch(session.alarmstate)
    {
       case daNone:
           ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A0));
           break;

       case daPastA1:
           ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A1));
           break;

       case daPastA2:
           ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A2));
           break;

       case daCurrentA1:
           ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A1));
           break;

       case daCurrentA2:
           ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A2));
           break;

       default:
        break;

    }
}



/**
 * @brief Device::sensorPageEntry
 */
void Device::sensorPageEntry()
{
    if (!sensorPageHeaderLoaded) {

        const QString ss = ui->tableWidget->styleSheet();
        ui->tableWidget->clear();
        ui->tableWidget->setStyleSheet(ss);
        ui->tableWidget->setSelectionMode(QTableWidget::NoSelection);

        ui->tableWidget->horizontalHeader()->setVisible(true);
        //ui->tableWidget->setRowCount(TABLE_WIDGET_TOTAL_ROWS);
        ui->tableWidget->setColumnCount(8);

        /*for (int i=0; i<TABLE_WIDGET_TOTAL_ROWS; i++)
            ui->tableWidget->setRowHeight(i,60);*/

        QFont font("Monospace",18);
        font.setBold(true);

        ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Pos"));
        ui->tableWidget->horizontalHeaderItem(0)->setTextAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        ui->tableWidget->horizontalHeaderItem(0)->setFont(font);
        ui->tableWidget->setColumnWidth(0,60);

        ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Alias"));
        ui->tableWidget->horizontalHeaderItem(1)->setTextAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        ui->tableWidget->horizontalHeaderItem(1)->setFont(font);
        ui->tableWidget->setColumnWidth(1,280);

        ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Temp"));
        ui->tableWidget->horizontalHeaderItem(2)->setTextAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        ui->tableWidget->horizontalHeaderItem(2)->setFont(font);
        ui->tableWidget->setColumnWidth(2,100);

        ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("A1"));
        ui->tableWidget->horizontalHeaderItem(3)->setTextAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        ui->tableWidget->horizontalHeaderItem(3)->setFont(font);
        ui->tableWidget->setColumnWidth(3,90);

        ui->tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("A2"));
        ui->tableWidget->horizontalHeaderItem(4)->setTextAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        ui->tableWidget->horizontalHeaderItem(4)->setFont(font);
        ui->tableWidget->setColumnWidth(4,90);

        ui->tableWidget->setHorizontalHeaderItem(5, new QTableWidgetItem("State"));
        ui->tableWidget->horizontalHeaderItem(5)->setTextAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        ui->tableWidget->horizontalHeaderItem(5)->setFont(font);
        ui->tableWidget->setColumnWidth(5,80);

        ui->tableWidget->setHorizontalHeaderItem(6, new QTableWidgetItem(""));
        ui->tableWidget->horizontalHeaderItem(6)->setTextAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        ui->tableWidget->horizontalHeaderItem(6)->setFont(font);
        ui->tableWidget->setColumnWidth(6,50);

        ui->tableWidget->setHorizontalHeaderItem(7, new QTableWidgetItem(""));
        ui->tableWidget->horizontalHeaderItem(7)->setTextAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        ui->tableWidget->horizontalHeaderItem(7)->setFont(font);
        ui->tableWidget->setColumnWidth(7,50);

        sensorPageHeaderLoaded = true;
    }



    // a1/a2 checked
    if (a1CheckBoxChecked || a2CheckBoxChecked)
        loadFilteredList();
    else {
        loadSensorList();
        popupateSensorList();
        playButtonOn();
    }

    updateStatus();

    TG_DEBUG("Initialising sensor view");
}

/**
 * @brief Device::sensorPageExit
 */
void Device::sensorPageExit()
{

}

/**
 * @brief Device::on_tableWidget_clicked
 * @param index
 */
void Device::on_tableWidget_clicked(const QModelIndex &index)
{
    qint16 sensorListSize;
    qint16 thisRowIndex;
    const Session_t session = tggateway->getSession();

    sensorListSize = sensorList.count();
    if (sensorListSize == 0)
        return;

    thisRowIndex = sensorListTopItem + index.row();

    if (thisRowIndex >= sensorListSize)
        thisRowIndex = thisRowIndex - sensorListSize;

    quint8 sensorIndex = sensorList.at(thisRowIndex);
    if (sensorIndex >= session.totalSensors) {
        TG_DEBUG("Index out of range << " << sensorIndex);
        return;
    }

    selectedSensorIndex = sensorIndex;

    TG_DEBUG("Selected sensor " << selectedSensorIndex);
    pageChange(spRealtimeChartPage);

}

/**
 * @brief Device::on_PlayButton_clicked
 */
void Device::on_PlayButton_clicked()
{
    if (a1CheckBoxChecked || a2CheckBoxChecked)
        return;

    if (playButtonActive)
        playButtonOff();
    else
        playButtonOn();

     TG_DEBUG("Play button clicked");
}

/**
 * @brief Device::on_A1Checkbox_clicked
 */
void Device::on_A1Checkbox_clicked()
{
    if (a1CheckBoxChecked)
    {
        // return button state
        if (playButtonState)
            playButtonOn();
        a1CheckBoxChecked = false;
        ui->A1Checkbox->setIcon(QIcon(ICON_UNCHECK));
        loadFilteredList();
        TG_DEBUG("A1 checkbox unchecked");
    }
    else
    {
        // preserve play button state
        playButtonState = playButtonActive;
        if (playButtonState)
            playButtonOff();
        a1CheckBoxChecked = true;
        ui->A1Checkbox->setIcon(QIcon(ICON_CHECK));
        loadFilteredList();
        TG_DEBUG("A1 checkbox checked");
    }


}

/**
 * @brief Device::on_A2Checkbox_clicked
 */
void Device::on_A2Checkbox_clicked()
{
    if (a2CheckBoxChecked)
    {
        // return button state
        if (playButtonState)
            playButtonOn();
        a2CheckBoxChecked = false;
        ui->A2Checkbox->setIcon(QIcon(ICON_UNCHECK));
        loadFilteredList();
        TG_DEBUG("A2 checkbox unchecked");
    }
    else
    {
        // preserve play button state
        playButtonState = playButtonActive;
        if (playButtonState)
            playButtonOff();
        a2CheckBoxChecked = true;
        ui->A2Checkbox->setIcon(QIcon(ICON_CHECK));
        loadFilteredList();
        TG_DEBUG("A2 checkbox checked");
    }
}

/**
 * @brief Device::on_UpButton_clicked
 */
void Device::on_UpButton_clicked()
{
    rotateListUp();
}

/**
 * @brief Device::rotationTimerTimeout
 */
void Device::rotationTimerTimeout()
{
    rotateListDown();
}

/**
 * @brief Device::dateTimeTimerTimeout
 */
void Device::dateTimeTimerTimeout()
{
    QDateTime dateTime = dateTime.currentDateTime();
    ui->TimeLabel->setText(dateTime.toString("hh:mm"));
    ui->DateLabel->setText(dateTime.toString("ddd dd-MMM-yy"));
}

/**
 * @brief Device::playButtonOn
 */
void Device::playButtonOn()
{
    playButtonActive = true;
    rotationTimer.start();
    ui->PlayButton->setIcon(QIcon(ICON_PAUSE));

}

/**
 * @brief Device::playButtonOff
 */
void Device::playButtonOff()
{
    playButtonActive = false;
    rotationTimer.stop();
    ui->PlayButton->setIcon(QIcon(ICON_PLAY));
}












/**
 * @brief Device::alarmBlinkTimerTimeout
 */
void Device::alarmBlinkTimerTimeout()
{
    if (alarmBlinkerOn)
        alarmBlinkerOn = false;
    else
        alarmBlinkerOn = true;

    blinker();
}



/**
 * @brief Device::onGatewayConfigLoaded
 */
void Device::onGatewayConfigLoaded(bool result, QString repr)
{
    connectTimer.stop();
    if (result)
        tggateway->connectStream();
    else {
        TG_INFO("Failed " << repr.toLatin1().data());
        // TODO: try again
        tggateway->requestAllConfig();
    }
}

/**
 * @brief Device::onGatewaySessionChange
 */
void Device::onGatewaySessionChange()
{
    loadSensorList();
}

/**
 * @brief Device::on_MuteButton_clicked
 */
void Device::on_MuteButton_clicked()
{
    if (muteButtonActive)
        muteDisable();
    else
        muteEnable();
}

/**
 * @brief Device::muteEnable
 */
void Device::muteEnable()
{
    // TODO: Add gateway api to mute alarm

    // draw icon
    muteButtonActive = true;
    ui->MuteButton->setIcon(QIcon(ICON_MUTE));
    TG_INFO("Mute enabled");
}

/**
 * @brief Device::muteDisable
 */
void Device::muteDisable()
{
    // TODO: Add gateway api to mute alarm

    muteButtonActive = false;
    // draw icon
    ui->MuteButton->setIcon(QIcon(ICON_AUDIO));
    TG_INFO("Mute disabled");
}

/**
 * @brief Device::blinker
 */
void Device::blinker()
{
    const QVector<Sensor_t>& sensors = tggateway->getSensors();
    const Session_t& session = tggateway->getSession();
    const InputOutput_t& inout = tggateway->getInout();

    if (session.alarmstate == daCurrentA2) {

        if (alarmBlinkerOn) {
            ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A2));
            if (inout.visualalert)
                ui->SensorPage->setStyleSheet(QString(TABLE_WIDGET_A2_ALARM));  //tableWidget->setStyleSheet(QString(TABLE_WIDGET_A2_ALARM));
            else
                ui->SensorPage->setStyleSheet(QString(TABLE_WIDGET_NO_ALARM));      //tableWidget->setStyleSheet(QString(TABLE_WIDGET_NO_ALARM));
        }
        else {
            ui->SensorPage->setStyleSheet(QString(TABLE_WIDGET_NO_ALARM));
            ui->StatusAlarmImage->setPixmap(QPixmap(""));
        }
    } else if (session.alarmstate == daCurrentA1) {

        if (alarmBlinkerOn)
        {
            ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A1));
            if (inout.visualalert)
                ui->SensorPage->setStyleSheet(QString(TABLE_WIDGET_A1_ALARM));
            else
                ui->SensorPage->setStyleSheet(QString(TABLE_WIDGET_NO_ALARM));
        }
        else {
            ui->SensorPage->setStyleSheet(QString(TABLE_WIDGET_NO_ALARM));
            ui->StatusAlarmImage->setPixmap(QPixmap(""));
        }
    } else {
        ui->SensorPage->setStyleSheet(QString(TABLE_WIDGET_NO_ALARM));
    }

    // check visual alarm is on


    qint16 thisRowIndex;
    qint16 sensorListSize = sensorList.count();
    qint16 sensorPosition;

    const Sensor_t * sensor;
    for (int i=0; i<sensorListSize; i++) {

        ui->tableWidget->setRowHeight(i,60);
        thisRowIndex = sensorListTopItem + i;

        if (thisRowIndex >= sensorListSize)
            thisRowIndex = thisRowIndex - sensorListSize;

        sensorPosition = sensorList.at(thisRowIndex);

        // check if in filtered list
        if (!sensorList.contains(sensorPosition))
            continue;

        sensor = &sensors.at(sensorPosition);

        QLabel *label = new QLabel;
        label->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
        label->setAlignment(Qt::AlignVCenter | Qt::AlignCenter);

        switch((TgAlarmStatus)sensor->alarmStatus){
            case daCurrentA2:
                if (alarmBlinkerOn)
                    label->setPixmap( QPixmap(ICON_SENSOR_A2));
                break;

            case daCurrentA1:
                if (alarmBlinkerOn)
                    label->setPixmap( QPixmap(ICON_SENSOR_A1));
                break;

            case daPastA2:
                label->setPixmap( QPixmap(ICON_SENSOR_A2));
                break;

            case daPastA1:
                label->setPixmap( QPixmap(ICON_SENSOR_A1));
                break;

            case daNone:
                label->setPixmap( QPixmap());
                break;

        }

        ui->tableWidget->setCellWidget(i,5,label);
    }
}

/**
 * @brief Device::on_DownButton_clicked
 */
void Device::on_DownButton_clicked()
{
    rotateListDown();
}

/**
 * @brief Device::rotateListUp
 */
void Device::rotateListUp()
{
    sensorListTopItem++;

    if (sensorListTopItem >= sensorList.count())
        sensorListTopItem = 0;

    popupateSensorList();
}

/**
 * @brief Device::rotateListDown
 */
void Device::rotateListDown()
{
    sensorListTopItem--;

    if (sensorListTopItem < 0)
        sensorListTopItem = sensorList.count() -1;

    popupateSensorList();
}

/**
 * @brief Device::loadSensorList
 */
void Device::loadSensorList()
{
     sensorList.clear();
     const Session_t session = tggateway->getSession();
     for (int i=0; i<session.totalSensors; i++)
        sensorList.append(i);

     sensorListTopItem = 0;
}

/**
 * @brief Device::loadFilteredList
 */
void Device::loadFilteredList()
{
    int i;
    sensorList.clear();
    const QVector<Sensor_t>& sensors = tggateway->getSensors();
    const Sensor_t * sensor;
    const Session_t& session = tggateway->getSession();

    for (i=0; i<session.totalSensors; i++){
        sensor = &sensors.at(i);

        if (a2CheckBoxChecked && a1CheckBoxChecked){
            if (sensor->alarmStatus != daNone)
                sensorList.append(i);   
        }

        else if (a2CheckBoxChecked){
             if (sensor->alarmStatus == daPastA2 || sensor->alarmStatus == daCurrentA2)
                 sensorList.append(i);
        }

        else if (a1CheckBoxChecked)
        {
            if (sensor->alarmStatus != daNone)
                sensorList.append(i);
        }

    }

    if (!a1CheckBoxChecked && !a2CheckBoxChecked) {
        for (i=0; i<session.totalSensors; i++)
           sensorList.append(i);
    }

    sensorListTopItem = 0;

    popupateSensorList();
}

/**
 * @brief Device::popupateSensorList
 */
void Device::popupateSensorList()
{
    const QVector<Sensor_t>& sensors = tggateway->getSensors();
    const Sensor_t * sensor;

    QString tempValue;
    qint16 thisRowIndex;
    qint16 sensorListSize = sensorList.count();
    qint16 sensorPosition;
    qint16 i;

    const QString ss = ui->tableWidget->styleSheet();
    ui->tableWidget->clearContents();
    ui->tableWidget->setStyleSheet(ss);

    ui->tableWidget->setRowCount(sensorListSize);
    QFont font("Monospace",14);

    for (i=0; i<sensorListSize; i++)
    {
        ui->tableWidget->setRowHeight(i,60);
        thisRowIndex = sensorListTopItem + i;

        if (thisRowIndex >= sensorListSize)
            thisRowIndex = thisRowIndex - sensorListSize;

        sensorPosition = sensorList.at(thisRowIndex);
        sensor = &sensors.at(sensorPosition);

        QLabel *pos = new QLabel(QString("%1").arg(sensor->pos));
        pos->setAlignment(Qt::AlignVCenter | Qt::AlignCenter);
        pos->setFont(font);
        pos->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
        ui->tableWidget->setCellWidget(i,0,pos);

        QLabel *aliasCell = new QLabel(sensor->alias);
        aliasCell->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        aliasCell->setFont(font);
        aliasCell->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
        ui->tableWidget->setCellWidget(i,1,aliasCell);

        if (sensor->temp == 200.0)
            tempValue = QString("FAULT");
        else
            tempValue = QString::number(sensor->temp, 'f', 1);

        QLabel *tempCell = new QLabel(tempValue + QString("%1C").arg(G_UNIT_DEGREES));
        tempCell->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        tempCell->setFont(font);
        tempCell->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
        ui->tableWidget->setCellWidget(i,2,tempCell);

        QLabel *a1 = new QLabel(QString("%1%2C").arg(sensor->a1).arg(G_UNIT_DEGREES));
        a1->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        a1->setFont(font);
        a1->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
        ui->tableWidget->setCellWidget(i,3,a1);

        QLabel *a2 = new QLabel(QString("%1%2C").arg(sensor->a2).arg(G_UNIT_DEGREES));
        a2->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
        a2->setFont(font);
        a2->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
        ui->tableWidget->setCellWidget(i,4,a2);

        QLabel *label = new QLabel;
        label->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);

        switch((TgAlarmStatus)sensor->alarmStatus){
            case daCurrentA2:
                if (alarmBlinkerOn)
                    label->setPixmap( QPixmap(ICON_SENSOR_A2));
                break;

            case daCurrentA1:
                if (alarmBlinkerOn)
                    label->setPixmap( QPixmap(ICON_SENSOR_A1));
                break;

            case daPastA2:
                label->setPixmap( QPixmap(ICON_SENSOR_A2));
                break;

            case daPastA1:
                label->setPixmap( QPixmap(ICON_SENSOR_A1));
                break;

            case daNone:
                label->setPixmap( QPixmap());
                break;

        }

        label->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
        ui->tableWidget->setCellWidget(i,5,label);


        QLabel *deltaIcon = new QLabel;
        deltaIcon->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
        QLabel *deltaValue = new QLabel(QString("%1").arg(sensor->diffmode));
        deltaValue->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
        // differential measurement
        if (sensor->diffmode != sensor->pos && sensor->diffmode != 0) {
            deltaIcon->setAlignment(Qt::AlignVCenter | Qt::AlignCenter);
            deltaIcon->setPixmap(QPixmap(ICON_SENSOR_LINK));
            ui->tableWidget->setCellWidget(i,6,deltaIcon);

            deltaValue->setAlignment(Qt::AlignVCenter | Qt::AlignCenter);
            deltaValue->setFont(font);
            ui->tableWidget->setCellWidget(i,7,deltaValue);
        }
    }

    // add the rest with empty strings, important else when alarm is on
    // there is 2 shades of red/blue.

   /* if (rowCount < TABLE_WIDGET_TOTAL_ROWS)
    {
        for (i=rowCount; i<TABLE_WIDGET_TOTAL_ROWS; i++)
        {
            QLabel *pos = new QLabel;
            pos->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
            ui->tableWidget->setCellWidget(i,0,pos);

            QLabel *aliasCell = new QLabel;
            aliasCell->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
            ui->tableWidget->setCellWidget(i,1,aliasCell);

            QLabel *tempCell = new QLabel;
            tempCell->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
            ui->tableWidget->setCellWidget(i,2,tempCell);

            QLabel *a1 = new QLabel;
            a1->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
            ui->tableWidget->setCellWidget(i,3,a1);

            QLabel *a2 = new QLabel;
            a2->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
            ui->tableWidget->setCellWidget(i,4,a2);

            QLabel *label = new QLabel;
            label->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
            ui->tableWidget->setCellWidget(i,5,label);

            QLabel *deltaIcon = new QLabel;
            deltaIcon->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
            ui->tableWidget->setCellWidget(i,6,deltaIcon);

            QLabel *deltaValue = new QLabel;
            deltaValue->setStyleSheet(QString("background-color: rgba(0,0,0,0);"));
            ui->tableWidget->setCellWidget(i,7,deltaValue);
        }
    }*/
}






/**
 * @brief Device::shutdownPageEntry
 */
void Device::shutdownPageEntry()
{
    ui->ControlBar->setCurrentIndex(spMenuPage);
    ui->ControlBarMenuTitleLabel->setText("Shutdown");
}

/**
 * @brief Device::shutdownPageExit
 */
void Device::shutdownPageExit()
{

}

/**
 * @brief Device::shutdownHaltConfirm
 * @note Call server shutdown post method instead of calling directly
 */
void Device::shutdownHaltConfirm()
{
    QProcess process;
    TG_WARNING("Shutdown confirmed");
    process.start("shutdown -h now");
    process.waitForFinished();
}

/**
 * @brief Device::shutdownRestartConfirm
 * @note Call server restart post method instead of calling directly
 */
void Device::shutdownRestartConfirm()
{
    QProcess process;
    TG_WARNING("Restart confirmed");
    process.start("shutdown -r now");
    process.waitForFinished();
}

/**
 * @brief Device::on_ShutdownButton_clicked
 */
void Device::on_ShutdownButton_clicked()
{
    confirmContext = ctxSystemHalt;
    TG_WARNING("Shutdown button pressed");
    showConfirm(QString("Power off"));
}

/**
 * @brief Device::on_RestartButton_clicked
 */
void Device::on_RestartButton_clicked()
{
    confirmContext = ctxSystemRestart;
    TG_WARNING("Restart button pressed");
    showConfirm(QString("Restart"));
}

/**
 * @brief Device::on_MenuButton_clicked
 */
void Device::on_MenuButton_clicked()
{
    TG_DEBUG("Sensor menu button pressed");
    pageChange(spMenuPage);
}

/**
 * @brief Device::on_MenuPowerButton_clicked
 */
void Device::on_MenuPowerButton_clicked()
{
    pageChange(spShutdownPage);
}



/************************************************
 *                  PAGE HANDLING API
 ************************************************/

/**
 * @brief Device::pageChange
 * @param newPage
 */
void Device::pageChange(WidgetPage newPage)
{
    pageExit(currentStackPage);
    previousStackPage = currentStackPage;
    ui->Body->setCurrentIndex(newPage);
    ui->ControlBar->setCurrentIndex(newPage);
    currentStackPage = newPage;
    pageEntry();
}

/**
 * @brief Device::pageEntry
 */
void Device::pageEntry()
{
    switch (currentStackPage) {
        case spNotSet:
            loadPageEntry();
            break;

        case spHomePage:
            sensorPageEntry();
            break;

        case spConnectPage:
            connectPageEntry();
            break;

        case spWifiPage:
            wifiPageEntry();
            break;

        case spEventPage:
            eventPageEntry();
            break;

        case spShutdownPage:
            shutdownPageEntry();
            break;

        case spMenuPage:
            menuPageEntry();
            break;

        case spAboutPage:
            aboutPageEntry();
            break;

        case spLogChartPage:
            logChartPageEntry();
            break;

        case spRealtimeChartPage:
            // only go if there are points
            if (tggateway->getCacheSize() > 0)
                streamChartPageEntry();
            break;

        case spSessionConfigPage:
            sessionConfigPageEntry();
            break;

        case spSensorListPage:
            sensorListPageEntry();
            break;

        case spSensorConfigPage:
            sensorConfigPageEntry();
            break;

        case spSettingsOptionsPage:
            ui->ControlBar->setCurrentIndex(spMenuPage);
            break;

        case spPasswordPage:
            passwordPageEntry();
            break;

        case spTableDataPage:


    default:
        qWarning() << "Unknown page";
    }
}

/**
 * @brief Device::pageExit
 */
void Device::pageExit(WidgetPage currentPage)
{

    switch (currentPage) {
        case spNotSet:
            //LoadPageExit();
            break;

        case spHomePage:
            sensorPageExit();
            break;

        case spMenuPage:
            //MenuPageExit();
            break;

        case spAboutPage:
            break;

        case spWifiPage:
            wifiPageExit();
            break;

        case spConnectPage:
            connectPageExit();
            break;

        case spEventPage:
            eventPageExit();
            break;

        case spLogChartPage:
            logChartPageExit();
            break;

        case spRealtimeChartPage:
            streamChartPageExit();
            break;

        case spShutdownPage:
            shutdownPageExit();
            break;

        case spSessionConfigPage:
            sessionConfigPageExit();
            break;

        case spSensorListPage:
            sensorListPageExit();
            break;

        case spSensorConfigPage:
            sensorConfigPageExit();
            break;

        case spSettingsOptionsPage:
            break;

        case spPasswordPage:
            passwordPageExit();
            break;

        default:
            qWarning() << "Unknown page";
    }
}

/**
 * @brief Device::on_ControlBarMenuBackButton_clicked
 */
void Device::on_ControlBarMenuBackButton_clicked()
{
    switch (currentStackPage) {
        case spMenuPage:
            pageChange(spHomePage);
            break;

        case spConnectPage:
        case spWifiPage:
        case spSettingsOptionsPage:
        case spAboutPage:
        case spShutdownPage:
        case spSettingsPage:
            pageChange(spMenuPage);
            break;

        case spPasswordPage:
            pageChange(spSettingsOptionsPage);
            break;

        case spNotSet:
        case spHomePage:
        case spEthPage:
        case spCellPage:
        case spEventPage:
        case spLogChartPage:
        case spRealtimeChartPage:
        case spSessionConfigPage:
        case spSensorListPage:
        case spSensorConfigPage:
        break;

        }
}





















/*************************************************
 *                  LOAD PAGE API
 *************************************************/

/**
 * @brief Device::loadPageEntry
 */
void Device::loadPageEntry()
{
    // clear this text as name is not known yet
    ui->hostnameLabel->clear();
}

/**
 * @brief Device::loadPageExit
 */
void Device::loadPageExit()
{

}

/**
 * @brief Device::on_ControlBarLoadMenuButton_clicked
 */
void Device::on_ControlBarLoadMenuButton_clicked()
{
    pageChange(spMenuPage);
}






















/*************************************************
 *                  MAIN MENU PAGE API
 *************************************************/

/**
 * @brief Device::menuPageEnrty
 */
void Device::menuPageEntry()
{
    // set control menu to menu option
    ui->ControlBarMenuTitleLabel->setText("Main menu");
}

/**
 * @brief Device::menuPageExit
 */
void Device::menuPageExit()
{

}

/**
 * @brief Device::on_MenuConnectButton_clicked
 * @details Go to Connectivity Page
 */
void Device::on_MenuConnectButton_clicked()
{
    pageChange(spConnectPage);
}

/**
 * @brief Device::on_MenuAboutButton_clicked
 * @details Go to About page
 */
void Device::on_MenuAboutButton_clicked()
{
    pageChange(spAboutPage);
}

/**
 * @brief Device::on_MenuSensorsButton_clicked
 */
void Device::on_MenuSensorsButton_clicked()
{
    pageChange(spSessionConfigPage);
}










/*************************************************
 *             CONNECTIVITY PAGE API
 ************************************************/


/**
 * @brief Device::connectPageEntry
 */
void Device::connectPageEntry()
{
    ui->ControlBar->setCurrentIndex(spMenuPage);
    ui->ControlBarMenuTitleLabel->setText("Connectivity");
}

/**
 * @brief Device::connectPageExit
 */
void Device::connectPageExit()
{

}

/**
 * @brief Device::on_ConnectivityEthButton_clicked
 * @details Go to Ethernet page
 */
void Device::on_ConnectivityEthButton_clicked()
{
    pageChange(spEthPage);
}

/**
 * @brief Device::on_ConnectivityWifiButton_clicked
 * @details Go to Wifi page
 */
void Device::on_ConnectivityWifiButton_clicked()
{
    pageChange(spWifiPage);
}

/**
 * @brief Device::on_ConnectivityCellButton_clicked
 * @details Go to Cell page
 */
void Device::on_ConnectivityCellButton_clicked()
{
    pageChange(spCellPage);
}













/*************************************************
 *                  ABOUT PAGE API
 ************************************************/

/**
 * @brief Device::aboutPageEntry
 */
void Device::aboutPageEntry()
{
    // set
    ui->ControlBar->setCurrentIndex(spMenuPage);
    ui->ControlBarMenuTitleLabel->setText("About");

    const GatewayId_t id = tggateway->getId();
    const System_t sys = tggateway->getSystem();
    const Network_t net = tggateway->getNetwork();
    const InputOutput_t alerts = tggateway->getInout();
    const Session_t session = tggateway->getSession();

    // Device
    ui->AboutDeviceName->setText(id.name);
    ui->AboutDeviceSerial->setText(id.serial);
    ui->AboutDeviceDescription->setText(id.description);
    ui->AboutDeviceServerVersion->setText(id.serverversion);
    ui->AboutDeviceLcdVersion->setText(id.lcdversion);

    // System
    ui->AboutSystemCpuUsage->setText(QString("%1%").arg(sys.cpu));
    ui->AboutSystemMemoryUsage->setText(QString("%1%").arg(sys.mem));
    ui->AboutSystemDiskUsage->setText(QString("%1%").arg(sys.disk));
    ui->AboutSystemUptime->setText(utilsSecondsToHms(sys.uptime));
    if (sys.ac) {
        if (sys.batlvl == 100)
            ui->AboutSystemPowerStatus->setText("AC charged");
        else
            ui->AboutSystemPowerStatus->setText("AC charging");
    } else {
        ui->AboutSystemPowerStatus->setText(QString("Battery level %1%").arg(sys.batlvl));
    }

    // Connectivity
    switch (net.netiface) {
        case stNone:
        ui->AboutConnectNetInterface->setText("None");
        break;

        case ctEth:
            ui->AboutConnectNetInterface->setText("Ethernet");
            break;

        case ctWifi:
            ui->AboutConnectNetInterface->setText("Wifi");
            break;

        case ctCell:
            ui->AboutConnectNetInterface->setText("Mobile 3g");
            break;

    }

    ui->AboutConnectNetAccess->setText("");
    ui->AboutConnectNetAccess->setPixmap(QPixmap(ICON_16_TICK));
    ui->AboutConnectLocalIp->setText(net.netip);
    ui->AboutConnectExternalIp->setText(net.hamIp);
    ui->AboutConnectHamachiClientId->setText(net.hamClientId);
    if (net.ham)
        ui->AboutConnectHamachiStatus->setText("Connected");
    else
        ui->AboutConnectHamachiStatus->setText("Not connected");

    // Notifications
    if (alerts.visualalert)
        ui->AboutNotificationsVisual->setPixmap(QPixmap(ICON_16_TICK));
    else
        ui->AboutNotificationsVisual->setPixmap(QPixmap(ICON_16_CROSS));

    if (alerts.audioalert)
        ui->AboutNotificationsEmail->setPixmap(QPixmap(ICON_16_TICK));
    else
        ui->AboutNotificationsVisual->setPixmap(QPixmap(ICON_16_CROSS));


    // Session
    ui->AboutSessionNumber->setText(QString("%1").arg(session.sessionnumber));
    ui->AboutSessionName->setText(QString("%1").arg(session.sessionname));
    ui->AboutSessionTotalSensors->setText(QString("%1").arg(session.totalSensors));

    switch (session.alarmtype) {
        case aiHH:
            ui->AboutSessionAlarmInterpretation->setText("High/high interpretation");
            break;

        case aiHL:
            ui->AboutSessionAlarmInterpretation->setText("High/low interpretation");
            break;

        case aiLL:
            ui->AboutSessionAlarmInterpretation->setText("Low/low interpretation");
            break;

        case aiNone:
            ui->AboutSessionAlarmInterpretation->setText("Not set");
            break;
    }


}

/**
 * @brief Device::aboutPageExit
 */
void Device::aboutPageExit()
{

}
















/*************************************************
 *                  WIFI PAGE API
 ************************************************/

/**
 * @brief Device::wifiPageEntry
 */
void Device::wifiPageEntry()
{
    ui->ControlBar->setCurrentIndex(spMenuPage);
    ui->ControlBarMenuTitleLabel->setText("Wifi");

    ui->wifiEnableButton->setChecked(true);
    ui->wifiEnableStatus->setPixmap(QPixmap(""));
    ui->wifiEnableLabel->setText("Enable Wifi");

    // do scan
    if (!tggateway->isWifiScanInProgress()) {
        tggateway->requestWifiScan();
        showLoader("Scanning ...",true);
    }
}

/**
 * @brief Device::wifiPageExit
 */
void Device::wifiPageExit()
{
}

/**
 * @brief Device::on_wifiEnableButton_toggled
 * @param checked
 */
void Device::on_wifiEnableButton_toggled(bool checked)
{
    if (checked) {
        if (!tggateway->isWifiScanInProgress()) {
            tggateway->requestWifiScan();
            showLoader(QString("Scanning ..."), true);
        }
    // clear table
    } else
        ui->WifiNetworkTable->clear();
}

/**
 * @brief Device::onGatewayWifiScanResults
 * @param result
 * @param total
 */
void Device::onGatewayWifiScanResults(bool result, int total)
{
    hideLoader();

    if (!result) {
        TG_WARNING("Wifi scan result failed");
        showResult(QString("Scan failed"),false);
        return;
    }

    if (total) {
        const QVector<WifiNetwork_t> wifiNetworks = tggateway->getWifiNetworks();
        const WifiNetwork_t * wifiNetwork;

        ui->WifiNetworkTable->clear();
        ui->WifiNetworkTable->setColumnWidth(0,50);
        ui->WifiNetworkTable->setColumnWidth(1,650);
        ui->WifiNetworkTable->setColumnWidth(2,50);
        ui->WifiNetworkTable->setColumnWidth(3,50);
        ui->WifiNetworkTable->setRowCount(total);
        for (int i=0; i<total; i++)
        {
            ui->WifiNetworkTable->setRowHeight(i, 50);


            wifiNetwork = &wifiNetworks.at(i);
            // connected state
            if (wifiNetwork->state == wsOnline){
                QLabel *state = new QLabel;
                state->setPixmap(QPixmap(ICON_TICK));
                state->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
                ui->WifiNetworkTable->setCellWidget(i,0,state);
            }

            // ssid
            QLabel *ssid = new QLabel(wifiNetwork->ssid);
            ssid->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
            ssid->setFont(QFont(QString("Monospace"), 14));
            ui->WifiNetworkTable->setCellWidget(i,1,ssid);

            // signal
            QLabel *signal = new QLabel;
            if (wifiNetwork->signal > 50)
                signal->setPixmap(QPixmap(ICON_WIFI));
            else if (wifiNetwork->signal > 40)
                signal->setPixmap(QPixmap(ICON_WIFI_MED));
            else
                signal->setPixmap(QPixmap(ICON_WIFI_LOW));

            signal->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
            ui->WifiNetworkTable->setCellWidget(i,2,signal);

            // security

            QLabel *security = new QLabel;
            if (wifiNetwork->security == wsWpa)
                security->setPixmap(QPixmap(ICON_SECURITY));

            security->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
            ui->WifiNetworkTable->setCellWidget(i,3,security);
        }
    } else {
        showResult(QString("No networks"),false);
        ui->wifiEnableLabel->setText("No networks");
    }
}

/**
 * @brief Device::onGatewayWifiConnectResult
 */
void Device::onGatewayWifiConnectResult(bool result, QString ipaddr)
{
    // hide loader
    hideLoader();


    // if success show confirm with success
    if(result) {
        showResult("Connected",true);
        TG_INFO("Wifi connect success ipaddress = " << ipaddr);
        ui->wifiEnableStatus->setPixmap(QPixmap(ICON_TICK));
    } else {
        showResult("Failed", false);
        TG_WARNING("Wifi connect failed");
    }

}

/**
 * @brief Device::wifiFormOpen
 * @param ssid
 */
void Device::wifiFormOpen(const QString& ssid)
{
    wifiDialog = new WifiDialog(this, ssid);
    QObject::connect(wifiDialog, SIGNAL(onOk(QString,QString)), this, SLOT(onWifiOk(QString,QString)));
    QObject::connect(wifiDialog, SIGNAL(onCancel()), this, SLOT(onWifiCancel()));
    wifiDialog->show();
#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif
}

/**
 * @brief Device::WifiFormClose
 */
void Device::wifiFormClose()
{
#if defined(TNET_OLIMEX)
    vKeypad->hide();
#endif
    wifiDialog->close();
}

/**
 * @brief Device::onWifiOk
 * @param ssid
 * @param psk
 */
void Device::onWifiOk(const QString ssid, const QString psk)
{
    tggateway->connectWifiNetwork(ssid, psk);
    wifiFormClose();
    showLoader("Joining ...", true);
}

/**
 * @brief Device::onWifiCancel
 */
void Device::onWifiCancel()
{
#if defined(TNET_OLIMEX)
    vKeypad->hide();
#endif
}

/**
 * @brief Device::on_WifiNetworkTable_clicked
 * @param index
 */
void Device::on_WifiNetworkTable_clicked(const QModelIndex &index)
{
    TG_DEBUG("Wifi network table clicked item.row=" << index.row() << " item.col=" << index.column());
    try {
        const QVector<WifiNetwork_t> wifiNetworks = tggateway->getWifiNetworks();
        WifiNetwork_t net = wifiNetworks.at(index.row());
        wifiFormOpen(net.ssid);
    } catch (std::exception &e) {
        qWarning() << "Widget: Wifi network table click exception e=" << e.what();
    }
}




















/*************************************************
 *                  Event Page API
 ************************************************/

/**
 * @brief Device::eventPageEntry
 */
void Device::eventPageEntry()
{
    /*eventView = new EventView(ui->EventPage, tgLog, tgConfig);
    eventView->setGeometry(0,0,800,380);
    eventView->raise();
    eventView->show();
    QObject::connect(eventView, SIGNAL(viewUpdated()), this, SLOT(eventViewUpdated()));
    showLoader("Loading events", true);
    eventView->updateView();*/
}

/**
 * @brief Device::eventPageExit
 */
void Device::eventPageExit()
{
    /*delete eventView;
    eventView = 0;*/
}

/**
 * @brief Device::eventViewUpdated
 */
void Device::eventViewUpdated()
{
    /*TG_DEBUG("Event view loaded");
    hideLoader();*/
}

/**
 * @brief Device::on_MenuEventButton_clicked
 */
void Device::on_MenuEventButton_clicked()
{
    pageChange(spEventPage);
}

/**
 * @brief Device::on_CBEventViewMenuButton_clicked
 */
void Device::on_CBEventViewMenuButton_clicked()
{
    pageChange(spMenuPage);
}

/**
 * @brief Device::on_CBEventViewSensorButton_clicked
 */
void Device::on_CBEventViewSensorButton_clicked()
{
    pageChange(spHomePage);
}




















/*************************************************
 *                  Log Chart Page API
 ************************************************/

/**
 * @brief Device::logChartPageEntry
 */
void Device::logChartPageEntry()
{
    if (logChart == NULL){
        logChart = new TgLogChart(ui->LogChartPage);
        ui->LogChartPage->layout()->addWidget(logChart);
    }

    // allow chart to receive touch events
    logChart->setGateway(tggateway);
    logChart->setChartActive(false);
    connect(logChart, SIGNAL(chartReady(bool,bool)), this, SLOT(logChartReadyRender(bool,bool)));

    // hide menu
    logChartAlarmSeachActive = false;
    chartMenuHide();

    TG_DEBUG("Selected sensor index = " << selectedSensorIndex);

    const QVector<Sensor_t> sensors = tggateway->getSensors();
    quint16 refPos = sensors.at(selectedSensorIndex).diffmode;

    QString refName;
    if (refPos == 0 || refPos == (selectedSensorIndex +1))
        refName = "";
    else
        refName = sensors.at(refPos-1).alias;

    logChart->loadSensorData(tggateway->getSession().sessionnumber,
                                       tggateway->getSession().totalSensors,
                                       sensors.at(selectedSensorIndex).pos,
                                       sensors.at(selectedSensorIndex).alias,
                                       refPos,
                                       refName,
                                       true);

    logChart->show();
}

/**
 * @brief Device::logChartPageExit
 */
void Device::logChartPageExit()
{
    //logChart->setChartActive(false);  
    ui->LogChartPage->layout()->removeWidget(logChart);
    logChart->deleteLater();
    logChart = NULL;

    chartMenuHide();
}

/**
 * @brief Device::logChartReadyRender
 * @param result
 */
void Device::logChartReadyRender(bool result, bool alarmsFound)
{
    TG_DEBUG("Load result = " << result << " alarms found = " << alarmsFound);
    // search active
    if (logChartAlarmSeachActive){
        logChartAlarmSeachActive = false;

        // alarms found, just hide
        if (alarmsFound)
            hideLoader();
        else
            setResultLoader("No alarms", false);
    }

    if (result)
        logChart->setChartActive(true);

}

/**
 * @brief Device::on_CBLogChartMenuButton_clicked
 */
void Device::on_CBLogChartMenuButton_clicked()
{
    if (ui->ChartMenuFrame->isVisible())
        chartMenuHide();
    else
        chartMenuShow();
}

/**
 * @brief Device::on_CBLogChartPrevDataButton_clicked
 * @details Go to previous data page
 */
void Device::on_CBLogChartPrevDataButton_clicked()
{
    logChart->panLeft();
}

/**
 * @brief Device::on_CBLogChartPrevSensorButton_clicked
 * @details Go to previous sensor
 */
void Device::on_CBLogChartPrevSensorButton_clicked()
{
    if (selectedSensorIndex > 0)
        selectedSensorIndex--;
    else
        selectedSensorIndex = tggateway->getSession().totalSensors -1;

    const QVector<Sensor_t> sensors = tggateway->getSensors();
    quint16 refPos = sensors.at(selectedSensorIndex).diffmode;

    QString refName;
    if (refPos == 0 || refPos == (selectedSensorIndex +1))
        refName = "";
    else
        refName = sensors.at(refPos-1).alias;

    logChart->loadSensorData(tggateway->getSession().sessionnumber,
                                       tggateway->getSession().totalSensors,
                                       sensors.at(selectedSensorIndex).pos,
                                       sensors.at(selectedSensorIndex).alias,
                                       refPos,
                                       refName,
                                       false);
}

/**
 * @brief Device::on_CBLogChartNextSensorButton_clicked
 * @details Go to next sensor
 */
void Device::on_CBLogChartNextSensorButton_clicked()
{
    if (selectedSensorIndex < tggateway->getSession().totalSensors -1)
        selectedSensorIndex++;
    else
        selectedSensorIndex = 0;

    const QVector<Sensor_t> sensors = tggateway->getSensors();
    quint16 refPos = sensors.at(selectedSensorIndex).diffmode;

    QString refName;
    if (refPos == 0 || refPos == (selectedSensorIndex +1))
        refName = "";
    else
        refName = sensors.at(refPos-1).alias;

    logChart->loadSensorData(tggateway->getSession().sessionnumber,
                                       tggateway->getSession().totalSensors,
                                       sensors.at(selectedSensorIndex).pos,
                                       sensors.at(selectedSensorIndex).alias,
                                       refPos,
                                       refName,
                                       false);
}

/**
 * @brief Device::on_CBLogChartNextDataButton_clicked
 * @details Go to next data page
 */
void Device::on_CBLogChartNextDataButton_clicked()
{
    logChart->panRight();
}

/**
 * @brief Device::on_CBLogChartOldestDataButton_clicked
 * @details Load oldest data
 */
void Device::on_CBLogChartOldestDataButton_clicked()
{
    logChart->loadOldest();
}

/**
 * @brief Device::on_CBLogChartLatestDataButton_clicked
 * @details Load latest data
 */
void Device::on_CBLogChartLatestDataButton_clicked()
{
    logChart->loadLatest();
}

/**
 * @brief Device::on_CBLogChartGoSensorButton_clicked
 */
void Device::on_CBLogChartGoSensorButton_clicked()
{
    pageChange(spHomePage);
}

/**
 * @brief Device::on_CBLogChartGoStreamChartButton_clicked
 */
void Device::on_CBLogChartGoStreamChartButton_clicked()
{
    pageChange(spRealtimeChartPage);
}

/**
 * @brief Device::on_CBLogChartAlarmSearchButton_clicked
 * @details Search alarm events. Every press searches an older data range for an alarm event
 */
void Device::on_CBLogChartAlarmSearchButton_clicked()
{
    logChartAlarmSeachActive = true;
    showLoader("Searching alarms", true);
    logChart->searchAlarms();
}









/**
 * @brief Device::on_ChartDiffPlotCheckbox_clicked
 */
void Device::on_ChartDiffPlotCheckbox_clicked()
{
    if (currentStackPage == spRealtimeChartPage)
        streamChart->toggleDiffPlotVisibility();
    else if (currentStackPage == spLogChartPage)
        logChart->toggleDiffPlotVisibility();
}

/**
 * @brief Device::on_ChartRefPlotCheckbox_clicked
 */
void Device::on_ChartRefPlotCheckbox_clicked()
{
    if (currentStackPage == spRealtimeChartPage)
        streamChart->toggleRefPlotVisibility();
    else if (currentStackPage == spLogChartPage)
        logChart->toggleRefPlotVisibility();
}

/**
 * @brief Device::on_ChartA1PlotCheckbox_clicked
 */
void Device::on_ChartA1PlotCheckbox_clicked()
{
    if (currentStackPage == spRealtimeChartPage)
        streamChart->toggleAlarm1PlotVisibility();
    else if (currentStackPage == spLogChartPage)
        logChart->toggleAlarm1PlotVisibility();
}

/**
 * @brief Device::on_ChartA2PlotCheckbox_clicked
 */
void Device::on_ChartA2PlotCheckbox_clicked()
{
    if (currentStackPage == spRealtimeChartPage)
        streamChart->toggleAlarm2PlotVisibility();
    else if (currentStackPage == spLogChartPage)
        logChart->toggleAlarm2PlotVisibility();
}

/**
 * @brief Device::on_ChartHGridCheckbox_clicked
 */
void Device::on_ChartHGridCheckbox_clicked()
{
    if (currentStackPage == spRealtimeChartPage)
        streamChart->toggleHGridVisibility();
    else if (currentStackPage == spLogChartPage)
        logChart->toggleHGridVisibility();
}

/**
 * @brief Device::on_ChartVGridCheckbox_clicked
 */
void Device::on_ChartVGridCheckbox_clicked()
{
    if (currentStackPage == spRealtimeChartPage)
        streamChart->toggleVGridVisibility();
    else if (currentStackPage == spLogChartPage)
        logChart->toggleVGridVisibility();
}

/**
 * @brief Device::on_ChartTooltipCheckbox_clicked
 */
void Device::on_ChartTooltipCheckbox_clicked()
{
    if (currentStackPage == spRealtimeChartPage)
        streamChart->toggleTooltipVisibility();
    else if (currentStackPage == spLogChartPage)
        logChart->toggleTooltipVisibility();
}

/**
 * @brief Device::on_ChartLegendCheckbox_clicked
 */
void Device::on_ChartLegendCheckbox_clicked()
{
    if (currentStackPage == spRealtimeChartPage)
        streamChart->toggleLegendVisibility();
    else if (currentStackPage == spLogChartPage)
        logChart->toggleLegendVisibility();
}















/*************************************************
 *                  Stream Chart Page API
 ************************************************/


/**
 * @brief Device::streamChartPageEntry
 */
void Device::streamChartPageEntry()
{
    if (streamChart == NULL){
        streamChart = new TgStreamChart(ui->StreamChartPage);
        ui->StreamChartPage->layout()->addWidget(streamChart);
    }

    streamChart->setGateway(tggateway);
    streamChart->setChartActive(false);

    // hide menu
    chartMenuHide();

    // selected plot saved under selectedSensorIndex
    const QVector<Sensor_t>& sensors = tggateway->getSensors();
    for(QVector<Sensor_t>::const_iterator it = sensors.begin(); it != sensors.end(); ++it) {
        if (it->pos == selectedSensorIndex+1) {
            // new plot
            SensorGraphData_t graphOpts;

            graphOpts.sensorPos = it->pos;
            graphOpts.sensorAlias = it->alias;

            if (it->diffmode != 0 && it->diffmode != it->pos) {
                graphOpts.sensorRefPos = it->diffmode;
                graphOpts.sensorRefAlias = sensors.at(graphOpts.sensorRefPos -1).alias;
            } else {
                graphOpts.sensorRefPos = 0;
                graphOpts.sensorRefAlias = "";
            }

            streamChart->newSensorGraph(graphOpts);
            streamChart->setChartActive(true);
            streamChart->refresh();
            break;
        }
    }

    streamChart->show();
}

/**
 * @brief Device::streamChartPageExit
 */
void Device::streamChartPageExit()
{
    //streamChart->setChartActive(false);
    ui->StreamChartPage->layout()->removeWidget(streamChart);
    streamChart->deleteLater();
    streamChart = NULL;

    chartMenuHide();
}

/**
 * @brief Device::on_CBStreamChartMenuButton_clicked
 */
void Device::on_CBStreamChartMenuButton_clicked()
{
    /*if (ui->ChartMenuFrame->isHidden())
        ui->ChartMenuFrame->setHidden(false);
    else
        ui->ChartMenuFrame->setHidden(true);*/

    if (ui->ChartMenuFrame->isVisible())
        chartMenuHide();
    else
        chartMenuShow();
}

/**
 * @brief Device::chartMenuHide
 */
void Device::chartMenuHide()
{
    ui->ChartMenuFrame->hide();

   //ui->ChartMenuFrame->setHidden(true);
   //ui->ChartMenuFrame->setGeometry(0,50,0,380);
}

/**
 * @brief Device::chartMenuShow
 */
void Device::chartMenuShow()
{
    if (currentStackPage == spRealtimeChartPage) {

        // set checkbox acocrding to values
        ui->ChartA1PlotCheckbox->setChecked(streamChart->a1PlotIsVisible());
        ui->ChartA1PlotCheckbox->setChecked(streamChart->a2PlotIsVisible());
        ui->ChartRefPlotCheckbox->setChecked(streamChart->refPlotIsVisible());
        ui->ChartDiffPlotCheckbox->setChecked(streamChart->diffPlotIsVisible());
        ui->ChartHGridCheckbox->setChecked(streamChart->hGridIsVisible());
        ui->ChartVGridCheckbox->setChecked(streamChart->vGridIsVisible());
        ui->ChartLegendCheckbox->setChecked(streamChart->legendIsVisible());

    } else if (currentStackPage == spLogChartPage) {
        ui->ChartA1PlotCheckbox->setChecked(logChart->a1PlotIsVisible());
        ui->ChartA1PlotCheckbox->setChecked(logChart->a2PlotIsVisible());
        ui->ChartRefPlotCheckbox->setChecked(logChart->refPlotIsVisible());
        ui->ChartDiffPlotCheckbox->setChecked(logChart->diffPlotIsVisible());
        ui->ChartHGridCheckbox->setChecked(logChart->hGridIsVisible());
        ui->ChartVGridCheckbox->setChecked(logChart->vGridIsVisible());
        ui->ChartLegendCheckbox->setChecked(logChart->legendIsVisible());
    }

    ui->ChartMenuFrame->show();
    ui->ChartTooltipCheckbox->setChecked(false);
    //ui->ChartMenuFrame->setHidden(false);
    //ui->ChartMenuFrame->setGeometry(0,50,180,380);
}



/**
 * @brief Device::on_CBStreamChartGoSensorButton_clicked
 */
void Device::on_CBStreamChartGoSensorButton_clicked()
{
    pageChange(spHomePage);
}

/**
 * @brief Device::on_CBStreamChartGoLogChartButton_clicked
 */
void Device::on_CBStreamChartGoLogChartButton_clicked()
{
    pageChange(spLogChartPage);
}

/**
 * @brief Device::on_CBStreamChartPrevSensorButton_clicked
 */
void Device::on_CBStreamChartPrevSensorButton_clicked()
{
    if (selectedSensorIndex > 0)
        selectedSensorIndex--;
    else
        selectedSensorIndex = tggateway->getSession().totalSensors -1;

    // selected plot saved under selectedSensorIndex
    const QVector<Sensor_t>& sensors = tggateway->getSensors();
    for(QVector<Sensor_t>::const_iterator it = sensors.begin(); it != sensors.end(); ++it) {
        if (it->pos == selectedSensorIndex+1) {
            // new plot
            SensorGraphData_t graphOpts;

            graphOpts.sensorPos = it->pos;
            graphOpts.sensorAlias = it->alias;

            if (it->diffmode != 0 && it->diffmode != it->pos) {
                graphOpts.sensorRefPos = it->diffmode;
                graphOpts.sensorRefAlias = sensors.at(graphOpts.sensorRefPos -1).alias;
            } else {
                graphOpts.sensorRefPos = 0;
                graphOpts.sensorRefAlias = "";
            }

            streamChart->newSensorGraph(graphOpts);
            streamChart->setChartActive(true);
            streamChart->refresh();
            break;
        }
    }
}

/**
 * @brief Device::on_CBStreamChartNextSensorButton_clicked
 */
void Device::on_CBStreamChartNextSensorButton_clicked()
{
    if (selectedSensorIndex < tggateway->getSession().totalSensors -1)
        selectedSensorIndex++;
    else
        selectedSensorIndex = 0;

    // selected plot saved under selectedSensorIndex
    const QVector<Sensor_t>& sensors = tggateway->getSensors();
    for(QVector<Sensor_t>::const_iterator it = sensors.begin(); it != sensors.end(); ++it) {
        if (it->pos == selectedSensorIndex+1) {
            // new plot
            SensorGraphData_t graphOpts;

            graphOpts.sensorPos = it->pos;
            graphOpts.sensorAlias = it->alias;

            if (it->diffmode != 0 && it->diffmode != it->pos) {
                graphOpts.sensorRefPos = it->diffmode;
                graphOpts.sensorRefAlias = sensors.at(graphOpts.sensorRefPos -1).alias;
            } else {
                graphOpts.sensorRefPos = 0;
                graphOpts.sensorRefAlias = "";
            }

            streamChart->newSensorGraph(graphOpts);
            streamChart->setChartActive(true);
            streamChart->refresh();
            break;
        }
    }
}















/*************************************************
 *                  Sensor config API
 ************************************************/

/**
 * @brief Device::sessionConfigPageEntry
 */
void Device::sessionConfigPageEntry()
{
}

/**
 * @brief Device::sessionConfigPageExit
 */
void Device::sessionConfigPageExit()
{

}

/**
 * @brief Device::sessionRestartConfirm
 * @details After confirming yes to restart session
 */
void Device::sessionRestartConfirm()
{
    tggateway->requestSessionRestart();
    showLoader("Session restart", true);
}

/**
 * @brief Device::on_CBSessionConfigBackButton_clicked
 */
void Device::on_CBSessionConfigBackButton_clicked()
{
    pageChange(spMenuPage);
}

/**
 * @brief Device::on_RestartSessionButton_clicked
 * @details Restart the current session
 */
void Device::on_RestartSessionButton_clicked()
{
    // show confirm
    confirmContext = ctxSessionRestart;
    TG_INFO("Restart session");
    showConfirm(QString("Restart session"));
}

/**
 * @brief Device::on_ResumeSessionButton_clicked
 */
void Device::on_ResumeSessionButton_clicked()
{
    confirmContext = ctxSessionResume;

    // make copy for editing
    const QVector<Sensor_t> sensors = tggateway->getSensors();
    const Session_t session = tggateway->getSession();

    sessionChange.sessionnumber = session.sessionnumber;
    sessionChange.sessionname = session.sessionname;
    sessionChange.totalSensors = session.totalSensors;
    sessionChange.alarmtype = session.alarmtype;
    sessionChange.triggerrate = session.triggerrate;

    sensorsChange.clear();
    for (int i=0; i< session.totalSensors; i++) {
        Sensor_t sensorCopy;
        sensorCopy.pos = sensors.at(i).pos;
        sensorCopy.serial = sensors.at(i).serial;
        sensorCopy.alias = sensors.at(i).alias;
        sensorCopy.a1 = sensors.at(i).a1;
        sensorCopy.a2 = sensors.at(i).a2;
        sensorCopy.diffmode = sensors.at(i).diffmode;
        sensorCopy.a1trig = sensors.at(i).a1trig;
        sensorCopy.a2trig = sensors.at(i).a2trig;
        sensorsChange.append(sensorCopy);
    }

    // go to sensor list page
    pageChange(spSensorListPage);
}

/**
 * @brief Device::on_StartnewSessionButton_clicked
 */
void Device::on_StartnewSessionButton_clicked()
{
    confirmContext = ctxSessionNew;

    const QVector<Sensor_t> sensors = tggateway->getSensors();
    const Session_t session = tggateway->getSession();

    sessionChange.sessionnumber = session.sessionnumber;
    sessionChange.sessionname = session.sessionname;
    sessionChange.totalSensors = session.totalSensors;
    sessionChange.alarmtype = session.alarmtype;
    sessionChange.triggerrate = session.triggerrate;

    sensorsChange.clear();
    for (int i=0; i< session.totalSensors; i++) {
        Sensor_t sensorCopy;
        sensorCopy.pos = sensors.at(i).pos;
        sensorCopy.serial = sensors.at(i).serial;
        sensorCopy.alias = sensors.at(i).alias;
        sensorCopy.a1 = sensors.at(i).a1;
        sensorCopy.a2 = sensors.at(i).a2;
        sensorCopy.diffmode = sensors.at(i).diffmode;
        sensorCopy.a1trig = sensors.at(i).a1trig;
        sensorCopy.a2trig = sensors.at(i).a2trig;
        sensorsChange.append(sensorCopy);
    }

    // go to sensor list page
    pageChange(spSensorListPage);
}










/**
 * @brief Device::sensorViewItemSelected
 * @param index
 * @details Selected sensor go to sensor config page
 */
void Device::sensorViewItemSelected(int index)
{
    TG_DEBUG("Sensor selected = " << index);
    selectedSensorIndex = index;
    // go to sensor config page
    pageChange(spSensorConfigPage);
}





/**
 * @brief Device::on_CBSensorListBackButton_clicked
 */
void Device::on_CBSensorListBackButton_clicked()
{
    pageChange(spSessionConfigPage);
}

/**
 * @brief Device::on_CBSensorListGoButton_clicked
 */
void Device::on_CBSensorListGoButton_clicked()
{
    //check context
    switch(confirmContext) {
        case ctxSessionResume:
            tggateway->requestSessionResume(&sessionChange, &sensorsChange);
            showLoader("Resume session", true);
            break;

        case ctxSessionNew:
           /// tggateway->requestSessionNew();
            showLoader("Resume session", true);
            break;

        default:
            break;
    }

}








/**
 * @brief Device::sensorConfigPageEntry
 */
void Device::sensorConfigPageEntry()
{
    ui->SensorNameLineEdit->setReadOnly(true);

    // selected sensor from list page
    const QVector<Sensor_t> sensors = tggateway->getSensors();
    Sensor_t sensor = sensors.at(selectedSensorIndex);

    //
    ui->SensorPositionLabel->setText(QString("%1").arg(sensor.pos));
    ui->SensorSerialNumberLabel->setText(sensor.serial);
    ui->SensorNameLineEdit->setText(sensor.alias);

    // if resume, can't edit these 3 below
    ui->Alarm1Spinbox->setValue(sensor.a1);
    ui->Alarm2Spinbox->setValue(sensor.a2);
    ui->ReferenceSpinbox->setValue(sensor.diffmode);
}

/**
 * @brief Device::sensorConfigPageExit
 */
void Device::sensorConfigPageExit()
{

}

/**
 * @brief Device::on_ReferenceSpinbox_valueChanged
 * @param arg1
 */
void Device::on_ReferenceSpinbox_valueChanged(int arg1)
{
    const QVector<Sensor_t> sensors = tggateway->getSensors();
    const Session_t session = tggateway->getSession();
    if (arg1 > session.totalSensors || arg1 < 0)
        ui->ReferenceSpinbox->setValue(sensors.at(selectedSensorIndex-1).diffmode);
}

/**
 * @brief Device::on_Alarm1Spinbox_valueChanged
 * @param arg1
 */
void Device::on_Alarm1Spinbox_valueChanged(int arg1)
{
    if (sessionChange.alarmtype == aiHH || sessionChange.alarmtype == aiHL) {
        if (arg1 >= ui->Alarm2Spinbox->value())
            ui->Alarm1Spinbox->setValue(arg1-1);
    } else if (sessionChange.alarmtype == aiLL) {
        if (arg1 <= ui->Alarm2Spinbox->value())
            ui->Alarm1Spinbox->setValue(arg1+1);
    }
}

/**
 * @brief Device::on_Alarm2Spinbox_valueChanged
 * @param arg1
 */
void Device::on_Alarm2Spinbox_valueChanged(int arg1)
{
    if (sessionChange.alarmtype == aiHH || sessionChange.alarmtype == aiHL) {
        if (arg1 <= ui->Alarm1Spinbox->value())
            ui->Alarm2Spinbox->setValue(arg1+1);
    } else if (sessionChange.alarmtype == aiLL) {
        if (arg1 >= ui->Alarm1Spinbox->value())
            ui->Alarm2Spinbox->setValue(arg1-1);
    }
}


/**
 * @brief Device::on_CBSensorConfigBackButton_clicked
 */
void Device::on_CBSensorConfigBackButton_clicked()
{
    // don't save and go back to sensor list page
    pageChange(spSensorListPage);
}

/**
 * @brief Device::on_CBSensorConfigGoButton_clicked
 */
void Device::on_CBSensorConfigGoButton_clicked()
{
    const QVector<Sensor_t> sensors = tggateway->getSensors();
    const Session_t session = tggateway->getSession();

    // save sensor at sensorIndex if changed

    if (sensors.at(selectedSensorIndex).alias != ui->SensorNameLineEdit->text()) {
        TG_DEBUG("Sensor " << sensorsChange.at(selectedSensorIndex).pos << " name change from " << sensors.at(selectedSensorIndex).alias <<
                 " to " << ui->SensorNameLineEdit->text() );
        sensorsChange[selectedSensorIndex].alias = ui->SensorNameLineEdit->text();
    }

    if (sensors.at(selectedSensorIndex).a1 != ui->Alarm1Spinbox->value()){
        TG_DEBUG("Sensor " << sensorsChange.at(selectedSensorIndex).pos << " A1 change from " << sensors.at(selectedSensorIndex).a1 <<
                 " to " << ui->Alarm1Spinbox->value() );
        sensorsChange[selectedSensorIndex].a1 = ui->Alarm1Spinbox->value();
    }

    if (sensors.at(selectedSensorIndex).a2 != ui->Alarm2Spinbox->value()){
        TG_DEBUG("Sensor " << sensorsChange.at(selectedSensorIndex).pos << " A2 change from " << sensors.at(selectedSensorIndex).a2 <<
                 " to " << ui->Alarm2Spinbox->value() );
        sensorsChange[selectedSensorIndex].a2 = ui->Alarm2Spinbox->value();
    }

    // go back to senso list page
    pageChange(spSensorListPage);
}

/**
 * @brief Device::onGatewaySessionRestart
 * @param repr
 */
void Device::onGatewaySessionRestart(bool result, QString repr)
{
    hideLoader();
    if (result)
        showResult("Success", true);
    else
        showResult("Failed", false);
}

/**
 * @brief Device::onGatewaySessionResume
 * @param repr
 */
void Device::onGatewaySessionResume(bool result, QString repr)
{
    hideLoader();
    if (result)
        showResult("Success", true);
    else
        showResult("Failed", false);
}

/**
 * @brief Device::onGatewaySessionNew
 * @param repr
 */
void Device::onGatewaySessionNew(bool result, QString repr)
{

}

/**
 * @brief Device::onGatewayRecordsReady
 * @param reslt
 * @param repr
 */
void Device::onGatewayRecordsReady(bool result, QString repr)
{
   // hideLoader();
    TG_DEBUG("Records ready result = " << result << " repr = " << repr);
}

/**
 * @brief Device::onPasswordEntered
 * @param password
 */
void Device::onPasswordEntered(const QString password)
{
    TG_DEBUG("Password = " << password);
    QByteArray hashed = authClient->hashPassword(password);
    if (hashed == authHash){
        TG_DEBUG("Password match");
        pskDialog->setResult(true);

        if (pskDeferred == spSettingsOptionsPage)
            pageChange(spSettingsOptionsPage);

    } else {
        TG_DEBUG("Password failed");
        pskDialog->setResult(false);
    }
}

/**
 * @brief Device::onPasswordCancel
 */
void Device::onPasswordCancel()
{
#if defined(TNET_OLIMEX)
    vKeypad->hide();
#endif
}























/*************************************************
 *                  Loader/Result/Confirm API
 ************************************************/



/**
 * @brief Device::connectTimerTimeout
 */
void Device::connectTimerTimeout()
{
    connectTimer.setInterval(30000);
    TG_WARNING("Connect timer timeout, try request config again");
    tggateway->requestAllConfig();
}

/**
 * @brief Device::incommingTempTimerTimeout
 */
void Device::incommingTempTimerTimeout()
{
    if (incommingTempTimerCount == 4) {
        ui->StatusIncommingTemp->setEnabled(false);
        ui->StatusIncommingTemp->setPixmap(QPixmap(""));
        incommingTempTimer.stop();
    }
    else {
        incommingTempTimerCount++;
        if (ui->StatusIncommingTemp->isEnabled()) {
            ui->StatusIncommingTemp->setEnabled(false);
            ui->StatusIncommingTemp->setPixmap(QPixmap(""));
        } else {
            ui->StatusIncommingTemp->setEnabled(true);
            ui->StatusIncommingTemp->setPixmap(QPixmap(ICON_THERMO));
        }
    }
}

/**
 * @brief Device::onAuthGetKey
 */
void Device::onAuthConnected()
{
    if (authAction == authActionGet){
        TG_DEBUG("Connected, requesting key");
        authClient->getKey();
    } else if (authAction == authActionUpdate) {
        TG_DEBUG("Connected, updating key");
        const QByteArray hash = authClient->hashPassword(authPassword);
        authClient->updateKey(hash);
    }
}


/**
 * @brief Device::onAuthKeyUpdateResponse
 * @param result
 */
void Device::onAuthKeyUpdateResponse(bool result)
{

}

/**
 * @brief Device::onAuthKeyGetResponse
 * @param key
 */
void Device::onAuthKeyGetResponse(QByteArray key)
{
    authHash = key;
    TG_DEBUG("Received key");
}

/**
 * @brief Device::showLoader
 * @param msg
 * @param spin
 */
void Device::showLoader(QString msg, bool spin)
{
    msgDialog = new MessageDialog(this, msg, spin);
    msgDialog->show();
}

/**
 * @brief Device::setResultLoader
 * @param msg
 * @param result
 */
void Device::setResultLoader(QString msg, bool result)
{
    msgDialog->setResult(msg, result);
}

/**
 * @brief Device::hideLoader
 */
void Device::hideLoader()
{
    msgDialog->close();
}

/**
 * @brief Device::showResult
 * @param result
 * @param msg
 */
void Device::showResult(QString msg, bool result)
{ 
    msgDialog = new MessageDialog(this, msg, false);
    msgDialog->setResult(msg, result);
    msgDialog->show();
}

/**
 * @brief Device::hideResult
 */
void Device::hideResult()
{
     msgDialog->close();
}

/**
 * @brief Device::showConfirm
 * @param msg
 */
void Device::showConfirm(QString msg)
{
    cfmDialog = new ConfirmDialog(this,msg);
    QObject::connect(cfmDialog, SIGNAL(confirmResult(bool)), this, SLOT(onConfirmResult(bool)));
    cfmDialog->show();
}

/**
 * @brief Device::hideConfirm
 */
void Device::hideConfirm()
{
    cfmDialog->close();
}

/**
 * @brief Device::onConfirmResult
 */
void Device::onConfirmResult(bool ok)
{

    hideConfirm();
    if (!ok)
        return;

    switch (confirmContext) {
        case ctxSessionRestart:
            sessionRestartConfirm();
            break;

        case ctxSessionResume:
            break;

        case ctxSessionNew:
            break;

        case ctxSystemHalt:
            shutdownHaltConfirm();
            break;

        case ctxSystemRestart:
            shutdownRestartConfirm();
            break;

        default:
            break;
    }
}


/**
 * @brief Device::on_MenuGeneralButton_clicked
 */
void Device::on_MenuGeneralButton_clicked()
{
    if (authHash.count() != 0){
        pskDeferred = spSettingsOptionsPage;
        showPskDialog();
    } else
        pageChange(spSettingsOptionsPage);
}

/**
 * @brief Device::showPskDialog
 */
void Device::showPskDialog()
{
    pskDialog = new PasswordDialog(this);
    QObject::connect(pskDialog, SIGNAL(passwordEntered(QString)), this, SLOT(onPasswordEntered(QString)));
    QObject::connect(pskDialog, SIGNAL(onCancel()), this, SLOT(onPasswordCancel()));
    pskDialog->show();
#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif
}

/**
 * @brief Device::hidePskDialog
 */
void Device::hidePskDialog()
{
#if defined(TNET_OLIMEX)
    vKeypad->hide();
#endif
    pskDialog->deleteLater();
}

















/*************************************************
 *                  Password config API
 ************************************************/



/**
 * @brief Device::passwordPageEntry
 */
void Device::passwordPageEntry()
{
    ui->NewPasswordLineEdit->clear();
    ui->ConfirmPasswordLineEdit->clear();
    ui->NewPasswordLineEdit->setFocus();
}

/**
 * @brief Device::passwordPageExit
 */
void Device::passwordPageExit()
{

}


/**
 * @brief Device::on_CBPasswordPageBack_clicked
 */
void Device::on_CBPasswordPageBack_clicked()
{
    pageChange(spMenuPage);
}

/**
 * @brief Device::on_CBPasswordPageGoButton_clicked
 */
void Device::on_CBPasswordPageGoButton_clicked()
{
    // save new password, upload
    showLoader("Updating ...", true);

}


/**
 * @brief Device::on_SettingsPasswordButton_clicked
 */
void Device::on_SettingsPasswordButton_clicked()
{
    pageChange(spPasswordPage);
}

void Device::on_NewPasswordLineEdit_returnPressed()
{
    ui->ConfirmPasswordLineEdit->setFocus();
}



void Device::on_pushButton_clicked()
{
    ui->SensorNameLineEdit->setReadOnly(false);
    ui->SensorNameLineEdit->setFocus();
#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif
}



/*************************************************
 *                  Sensor Add config API
 ************************************************/

/**
 * @brief Device::sensorListPageEntry
 */
void Device::sensorListPageEntry()
{
    sensorListTop = 0;
    ui->sensorListTableWidget->clearContents();

    // total rows and columns
    ui->sensorListTableWidget->setRowCount(sessionChange.totalSensors);//8);
    ui->sensorListTableWidget->setColumnCount(1);

    bool canRemoveSensors;
    canRemoveSensors = (confirmContext == ctxSessionNew)?true:false;

    for (int i=0; i<sessionChange.totalSensors; i++)
    {
        SensorListItem *sensorListItem = new SensorListItem(0,sensorsChange.at(i),canRemoveSensors);
        ui->sensorListTableWidget->setCellWidget(i,0,sensorListItem);
        QObject::connect(sensorListItem, SIGNAL(sensorEditPressed(int)), this, SLOT(on_sensorListItemEdit(int)));
        QObject::connect(sensorListItem, SIGNAL(sensorRemoved(int)), this, SLOT(on_sensorListItemRemoved(int)));
    }
}

/**
 * @brief Device::sensorListPageExit
 */
void Device::sensorListPageExit()
{
}

/**
 * @brief Device::sensorListPageExit
 */
void Device::on_sensorListItemEdit(const int pos)
{
    TG_WARNING("Sensor at pos " << pos << " edit");
    showSensorAddDialog(pos-1);
}

/**
 * @brief Device::sensorListPageExit
 */
void Device::on_sensorListItemRemoved(const int pos)
{
    TG_WARNING("Sensor at pos " << pos << " removed");
}


/**
 * @brief Device::on_CBSensorListScrollUpButton_clicked
 */
void Device::on_CBSensorListScrollUpButton_clicked()
{
    if (sensorListTop == sessionChange.totalSensors - 1)
        return;

    sensorListTop++;
    TG_DEBUG("Sensor list top = " << sensorListTop);
    ui->sensorListTableWidget->viewport()->scroll(0,-(ui->sensorListTableWidget->rowHeight(0)));
}

/**
 * @brief Device::on_CBSensorListScrollDownButton_clicked
 */
void Device::on_CBSensorListScrollDownButton_clicked()
{
    if (sensorListTop == 0)
        return;

    sensorListTop--;
    TG_DEBUG("Sensor list top = " << sensorListTop);
    ui->sensorListTableWidget->viewport()->scroll(0,ui->sensorListTableWidget->rowHeight(0));
}



void Device::showSensorAddDialog(int indexOfSensor)
{
    const Sensor_t& sensor = sensorsChange.at(indexOfSensor);

#if defined(TNET_OLIMEX)
    addSensorDialog = new SearchNetSensor(this,&sessionChange,sensor,(confirmContext==ctxSessionNew)?true:false,keypad);
#elif defined(TNET_DESKTOP)
    addSensorDialog = new SearchNetSensor(this,sessionChange,sensor,(confirmContext==ctxSessionNew)?true:false,0);
#endif
    QObject::connect(addSensorDialog, SIGNAL(finished(bool)), this, SLOT(onAddSensorFinished(bool)));
    addSensorDialog->show();
}

void Device::hideSensorAddDialog()
{
    addSensorDialog->close();
    addSensorDialog->deleteLater();
#if defined(TNET_OLIMEX)
    vKeypad->hide();
#endif
}


/**
 * @brief Device::on_CBSensorListAddSensorButton_clicked
 * @details Add a new sensor
 */
void Device::on_CBSensorListAddSensorButton_clicked()
{
    ++sessionChange.totalSensors;
    TG_WARNING("Adding new sensor, position = " << sessionChange.totalSensors);
    Sensor_t newSensor;
    newSensor.pos = sessionChange.totalSensors;
    sensorsChange.append(newSensor);
    showSensorAddDialog(sessionChange.totalSensors-1);
}

void Device::onAddSensorFinished(bool ok)
{

    // if ok is false, cancel was pressed
    if (ok) {
        // get change config
        const Sensor_t sensorConfig = addSensorDialog->getNewConfig();

        Sensor_t * sensor = sensorsChange.data();

        // update in vector
        sensor[selectedSensorIndex] = sensorConfig;
        sensor[selectedSensorIndex].a1trig = false;
        sensor[selectedSensorIndex].a2trig = false;
        sensor[selectedSensorIndex].alarmStatus = daNone;
        sensor[selectedSensorIndex].temp = 0;
        sensor[selectedSensorIndex].watch = false;

        // update the table widget item
        SensorListItem * sensorItem = dynamic_cast<SensorListItem *>(ui->sensorListTableWidget->item(sensorConfig.pos-1,0));
        sensorItem->updateModel(sensorConfig);
    }

    hideSensorAddDialog();
}

/**
 * @brief Device::on_CBSensorListClearButton_clicked
 * @details CLear the list
 */
void Device::on_CBSensorListClearButton_clicked()
{

}















/*************************************************
 *                  Table data API
 ************************************************/

/**
 * @brief Device::tableDataPageEntry
 */
void Device::tableDataPageEntry()
{
   /* const Session_t& session = tggateway->getSession();
    const QVector<Sensor_t>& sensors = tggateway->getSensors();
    const Sensor_t& sensor = sensors.at(selectedSensorIndex);
    tableDataLoader = new DataLoader(0,session.totalSensors, selectedSensorIndex, sensor.diffmode, )*/
}

/**
 * @brief Device::tableDataPageExit
 */
void Device::tableDataPageExit()
{

}


