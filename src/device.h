#ifndef DEVICE_H
#define DEVICE_H

#include <QMainWindow>
#include <QString>
#include <QSharedPointer>
#include <QLabel>
#include <QTimer>
#include <QFuture>
#include <QDateTime>
#include <QModelIndex>
#include <QTableWidgetItem>
#include <QScopedPointer>
#include "tnetportal.h"
#include "messagedialog.h"
#include "confirmdialog.h"
#include "wifidialog.h"
#include "passworddialog.h"
#include "searchnetsensor.h"
#include "common.h"

#include "uihomepage.h"

#if defined(TNET_OLIMEX)
#include "virtualkeypaddialog.h"
#endif

#define TABLE_WIDGET_TOTAL_ROWS     6

#define TABLE_WIDGET_NO_ALARM       "background-color: #303030;"
#define TABLE_WIDGET_A1_ALARM       "background-color: rgba(0, 85,255,100);"
#define TABLE_WIDGET_A2_ALARM       "background-color: rgba(255, 0, 0,100);"


enum ConfirmContext { ctxSessionRestart = 0,
                      ctxSessionResume,
                      ctxSessionNew,
                      ctxSystemHalt,
                      ctxSystemRestart};


enum ControlBarType {cbLoad=0, cbSensor, cbMenu};
enum TgShutdownType {stNone=0, stShutdown=1, stRestart=2};


namespace Ui {
class Device;
}

class Device : public QMainWindow
{
    Q_OBJECT

public:
    explicit Device(QWidget *parent = 0, TgGateway *gateway = NULL);
    ~Device();

protected:
    void closeEvent(QCloseEvent *event);

signals:
    void userCloseRequest(void);

public slots:
    void logChartReadyRender(bool result, bool alarmsFound);
    void eventViewUpdated();
    void sensorViewItemSelected(int index);

    void onConfirmResult(bool ok);

    // password dialog slots
    void onPasswordEntered(const QString password);
    void onPasswordCancel();

    // wifi connect dialog slots
    void onWifiOk(const QString ssid, const QString psk);
    void onWifiCancel();

    // net sensor input/search dialog
    void onAddSensorFinished(bool ok);
private slots:
    void on_A1Checkbox_clicked();
    void on_A2Checkbox_clicked();
    void on_PlayButton_clicked();
    void on_MuteButton_clicked();
    void on_UpButton_clicked();
    void on_DownButton_clicked();
    void on_tableWidget_clicked(const QModelIndex &index);
    void on_MenuButton_clicked();
    void on_RestartButton_clicked();
    void on_ShutdownButton_clicked();
    void on_MenuAboutButton_clicked();

    void on_CBStreamChartMenuButton_clicked();
    void on_ChartDiffPlotCheckbox_clicked();
    void on_ChartRefPlotCheckbox_clicked();
    void on_ChartA1PlotCheckbox_clicked();
    void on_ChartA2PlotCheckbox_clicked();
    void on_ChartHGridCheckbox_clicked();
    void on_ChartVGridCheckbox_clicked();
    void on_ChartTooltipCheckbox_clicked();
    void on_ChartLegendCheckbox_clicked();
    void on_CBStreamChartGoSensorButton_clicked();
    void on_CBLogChartMenuButton_clicked();
    void on_CBLogChartPrevDataButton_clicked();
    void on_CBLogChartPrevSensorButton_clicked();
    void on_CBLogChartNextSensorButton_clicked();
    void on_CBLogChartNextDataButton_clicked();
    void on_CBLogChartGoSensorButton_clicked();
    void on_CBStreamChartGoLogChartButton_clicked();
    void on_CBLogChartGoStreamChartButton_clicked();


    // timers
    void rotationTimerTimeout();
    void dateTimeTimerTimeout();
    void alarmBlinkTimerTimeout();
    void connectTimerTimeout();
    void incommingTempTimerTimeout();

    // auth slots
    void onAuthConnected(void);
    void onAuthKeyUpdateResponse(bool result);
    void onAuthKeyGetResponse(QByteArray key);

    // gateway slots
    void onGatewayStreamReceived(TgEvStreamClass type);
    void onGatewayConfigLoaded(bool,QString repr);
    void onGatewaySessionChange();
    void onGatewayWifiScanResults(bool result, int total);
    void onGatewayWifiConnectResult(bool result, QString ipaddr);
    void onGatewaySessionRestart(bool result,QString repr);
    void onGatewaySessionResume(bool result,QString repr);
    void onGatewaySessionNew(bool result,QString repr);
    void onGatewayRecordsReady(bool result, QString repr);

    void on_ControlBarMenuBackButton_clicked();
    void on_ControlBarLoadMenuButton_clicked();
    void on_MenuConnectButton_clicked();

    void on_ConnectivityEthButton_clicked();
    void on_ConnectivityWifiButton_clicked();
    void on_ConnectivityCellButton_clicked();
    void on_wifiEnableButton_toggled(bool checked);
    void on_WifiNetworkTable_clicked(const QModelIndex &index);


    void on_MenuPowerButton_clicked();
    void on_MenuEventButton_clicked();
    void on_CBEventViewMenuButton_clicked();
    void on_CBEventViewSensorButton_clicked();

    void on_CBLogChartOldestDataButton_clicked();
    void on_CBLogChartLatestDataButton_clicked();
    void on_CBStreamChartPrevSensorButton_clicked();
    void on_CBStreamChartNextSensorButton_clicked();
    void on_CBLogChartAlarmSearchButton_clicked();
    void on_MenuSensorsButton_clicked();
    void on_CBSensorConfigBackButton_clicked();
    void on_CBSessionConfigBackButton_clicked();
    void on_CBSensorListBackButton_clicked();
    void on_CBSensorListGoButton_clicked();
    void on_CBSensorConfigGoButton_clicked();
    void on_RestartSessionButton_clicked();
    void on_ResumeSessionButton_clicked();
    void on_StartnewSessionButton_clicked();

    void on_ReferenceSpinbox_valueChanged(int arg1);
    void on_Alarm1Spinbox_valueChanged(int arg1);
    void on_Alarm2Spinbox_valueChanged(int arg1);
    void on_CBSensorListScrollUpButton_clicked();
    void on_CBSensorListScrollDownButton_clicked();


    void on_CBPasswordPageBack_clicked();
    void on_CBPasswordPageGoButton_clicked();

    void on_MenuGeneralButton_clicked();
    void on_SettingsPasswordButton_clicked();
    void on_NewPasswordLineEdit_returnPressed();
    void on_pushButton_clicked();
    void on_CBSensorListAddSensorButton_clicked();




    void on_CBSensorListClearButton_clicked();
    void on_sensorListItemEdit(const int pos);
    void on_sensorListItemRemoved(const int pos);



private:
    Ui::Device *ui;
    TgGateway * tggateway;
    QList<qint16> sensorList;
    qint16 sensorListTopItem;  // sensor temp page
    quint16 sensorListTop; // sensorList page
    QTimer rotationTimer;
    QTimer dateTimeTimer;
    QTimer alarmBlinkTimer;
    QTimer connectTimer;
    QTimer incommingTempTimer;
    quint8 selectedSensorIndex;
    ConfirmContext confirmContext;
    bool logChartAlarmSeachActive;
    quint8 incommingTempTimerCount;
    bool shiftActive;
    WidgetPage currentStackPage;
    WidgetPage previousStackPage;

    QByteArray authHash;
    QString authPassword;
    AuthAction authAction;

    TgLogChart * logChart;
    TgStreamChart * streamChart;
    MessageDialog * msgDialog;
    ConfirmDialog * cfmDialog;
    WifiDialog * wifiDialog;
    PasswordDialog * pskDialog;
    SearchNetSensor * addSensorDialog;
    WidgetPage pskDeferred;

    // Table data
    DataLoader * tableDataLoader;
    QThread * workerThread;

    // widget
    UiHomePage * page;

    bool sensorPageHeaderLoaded;
    bool muteButtonActive;
    bool a1CheckBoxChecked;
    bool a2CheckBoxChecked;
    bool playButtonActive;
    bool alarmBlinkerOn;
    bool playButtonState;

    // for sensor config
    Session_t sessionChange;
    QVector<Sensor_t> sensorsChange;

#if defined(TNET_OLIMEX)
    VirtualKeypadDialog * vKeypad;
#endif


    // status
    void clearStatusIcons(void);

    // page handling api
    void pageChange(WidgetPage newPage);
    void pageEntry();
    void pageExit(WidgetPage currentPage);

    // about menu api
    void aboutPageEntry();
    void aboutPageExit();

    // connectivity page api
    void connectPageEntry();
    void connectPageExit();

    // wifi page api
    void wifiPageEntry();
    void wifiPageExit();
    void wifiFormOpen(const QString& ssid);
    void wifiFormClose();

    // load page api
    void loadPageEntry();
    void loadPageExit();

    // main menu page api
    void menuPageEntry();
    void menuPageExit();

    // sensor page api
    void sensorPageEntry();
    void sensorPageExit();
    void updateStatus();
    void muteEnable(void);
    void muteDisable(void);
    void playButtonOn();
    void playButtonOff();
    void blinker();
    void rotateListUp();
    void rotateListDown();
    void loadSensorList();
    void loadFilteredList();
    void popupateSensorList();



    void shutdownPageEntry();
    void shutdownPageExit();
    void shutdownHaltConfirm();
    void shutdownRestartConfirm();

    // event page
    void eventPageEntry();
    void eventPageExit();

    // log chart page
    void logChartPageEntry();
    void logChartPageExit();


    // stream chart page
    void streamChartPageEntry();
    void streamChartPageExit();

    // chart menu
    void chartMenuHide();
    void chartMenuShow();

    // session & sensors page
    void sessionConfigPageEntry();
    void sessionConfigPageExit();
    void sessionRestartConfirm();

    void sensorListPageEntry();
    void sensorListPageExit();
    void sensorConfigPageEntry();
    void sensorConfigPageExit();

    void passwordPageEntry();
    void passwordPageExit();

    void tableDataPageEntry();
    void tableDataPageExit();


    void showPskDialog();
    void hidePskDialog();

    void showSensorAddDialog(int indexOfSensor);
    void hideSensorAddDialog();

    void showResult(QString msg, bool result);
    void hideResult();

    void showLoader(QString msg, bool spin);
    void setResultLoader(QString msg, bool result);
    void hideLoader();

    void showConfirm(QString msg);
    void hideConfirm();
    //void InitInfoView();
    //void InitShutdownView();
};

#endif // DEVICE_H
