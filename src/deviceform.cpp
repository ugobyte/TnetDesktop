#include "deviceform.h"
#include "ui_deviceform.h"
#include "uirealtimechartpage.h"
#include "uilogchartpage.h"
#include "uitabledatapage.h"
#include "uimenupage.h"
#include "uiconnectivitypage.h"
#include "uiwifipage.h"
#include "uisessionmenupage.h"
#include "uisensorlistpage.h"
#include "uisensorconfigpage.h"
#include "uiusernotificationpage.h"
#include "uiuseraddeditpage.h"
#include "uipasswordeditpage.h"
#include "uicredentialspage.h"
#include "uipoweroffpage.h"
#include "uiaboutpage.h"
#include "uigeneralsettingsmenu.h"
#include "uisessionfolderviewpage.h"
#include "uisessionpreviewpage.h"
#include "uiaudiovisualpage.h"
#include "uiimagespage.h"

DeviceForm::DeviceForm(QWidget *parent, TgGateway * tggateway) :
    QMainWindow(parent),
    ui(new Ui::DeviceForm),
    gateway(tggateway),
    vKeypad(0),
    alarmBlink(false)
{
    ui->setupUi(this);

    // general settings
    initialiseGeneralSettings();

    // gateway signal slots
    QObject::connect(gateway, SIGNAL(connectionStateChange(bool)), this, SLOT(on_gatewayConnectStateChanged(bool)));
    QObject::connect(gateway, SIGNAL(streamReceived(TgEvStreamClass)), this, SLOT(on_gatewayStreamReceived(TgEvStreamClass)));

    // a few things for device
#if defined(TNET_OLIMEX)

    //
    //QObject::connect(&displayControlTimer, SIGNAL(timeout()), this, SLOT(on_displayControlTimerTimeout()));
    //displayControlTimer.start(5000);

    // create virtual keypad
    vKeypad = new VirtualKeypadDialog;
    vKeypad->setFocusPolicy(Qt::NoFocus);
    vKeypad->setGeometry(0,270,800,210);

    // set ip address as localhost
    tggateway->setIpAddress("127.0.0.1");

    // load from local system
    tggateway->loadConfigGateway();
    tggateway->loadConfigNetwork();
    tggateway->loadConfigTemperature();
    tggateway->loadConfigNotifications();
    tggateway->loadConfigAudioVisual();
    tggateway->loadConfigKey();

    // connect
    tggateway->connectStream();
#else
    showMaximized();
    // sync records
    gateway->syncRecords();
#endif

    initialiseFilterCtx();

    // goto home page
    uipage = new UiHomePage(ui->pageFrame, spHomePage, tggateway, vKeypad, &ctx, generalSettings.degreeCelcius);
    QObject::connect(uipage, SIGNAL(gotoPage(WidgetPage)), this, SLOT(on_pageGotoWidget(WidgetPage)));
    ui->pageFrame->layout()->addWidget(uipage);
    uipage->show();

    // set up datetime update timer
    QObject::connect(&dateTimeTimer, SIGNAL(timeout()), this, SLOT(on_dateTimeTimerTimeout()));
    dateTimeTimer.start(10000);
    on_dateTimeTimerTimeout();

    // set up incomming temperature timer
    incommingTempTimerCount = 0;
    incommingTempTimer.setInterval(783);
    QObject::connect(&incommingTempTimer, SIGNAL(timeout()), this, SLOT(on_incommingTempTimerTimeout()));

    // set up alarm state timer
    QObject::connect(&alarmStatusTimer, SIGNAL(timeout()), this, SLOT(on_alarmStatusTimeout()));

    // initialise status bar
    updateStatus(true);

    TG_DEBUG("Constructing device form");
}

DeviceForm::~DeviceForm()
{
    uipage->deleteLater();
    delete ui;

    TG_DEBUG("Destructing device form");
}

void DeviceForm::closeEvent(QCloseEvent *event)
{
    // accept event
    event->accept();

    // send signal to exporer widget to handle close
    emit formCloseRequest();
}

void DeviceForm::on_pageGotoWidget(WidgetPage page)
{
    QString sessionNo;
    const WidgetPage currentPageId = uipage->getPageId();

    // remove from layout
    ui->pageFrame->layout()->removeWidget(uipage);

    // session config menu to sensor list menu, get action {resume, createnew} and copy session,sensors for changing
    if (currentPageId == spSessionConfigPage) {
        sessionAction = static_cast<UiSessionMenuPage *>(uipage)->getSessionAction();
        // get session and sensors
        const Session_t session = gateway->getSession();
        const QVector<Sensor_t> sensors = gateway->getSensors();
        sessionCopy = session;
        sensorsCopy = sensors;
    }

    // sensor list to sensor config, get sensor item selected
    else if (currentPageId == spSensorListPage)
        selectedSensorItem = static_cast<UiSensorListPage *>(uipage)->getSelectedSensor();

    // user list page to user add/edit page, copy user
    else if (currentPageId == spNotificationPage)
        userSelected = static_cast<UiUserNotificationPage *>(uipage)->getSelectedUserItem();

    // return from previous session view page, need to initialise filter context again
    else if (currentPageId == spSessionFolderPage && page == spSessionConfigPage)
        initialiseFilterCtx();


    // delete current
    uipage->deleteLater();

    // create the new
    switch (page) {
        case spNotSet:
            break;

        case spHomePage:
            uipage = new UiHomePage(ui->pageFrame, spHomePage, gateway, vKeypad, &ctx, generalSettings.degreeCelcius);
            break;

        case spMenuPage:
            uipage = new uiMenuPage(ui->pageFrame, spMenuPage);
            break;

        case spAboutPage:
            uipage = new UiAboutPage(ui->pageFrame, spAboutPage, gateway);
            break;

        case spSettingsPage:
        case spConnectPage:
            uipage = new UiConnectivityPage(ui->pageFrame, spConnectPage);
            break;

        case spEthPage:
        case spWifiPage:
            uipage = new UiWifiPage(ui->pageFrame, spWifiPage, gateway, vKeypad);
            break;

        case spCellPage:
        case spEventPage:
        case spShutdownPage:
            uipage = new UiPowerOffPage(ui->pageFrame, spShutdownPage, gateway);
            break;

        case spLogChartPage:
            // if not coming from table view, set currentDataFile to "" so chart loads latest
            if (currentPageId != spTableDataPage)
                ctx.currentDataFile = "";
            uipage = new UiLogChartPage(ui->pageFrame, spLogChartPage, gateway, &ctx, &generalSettings);
            break;

        case spRealtimeChartPage:
            uipage = new UiRealtimeChartPage(ui->pageFrame, spRealtimeChartPage, gateway, &ctx, &generalSettings);
            break;

        case spSessionConfigPage:
            uipage = new UiSessionMenuPage(ui->pageFrame, spSessionConfigPage, gateway, &generalSettings);
            break;

        case spSensorListPage:
            // clear and initialise if creating new
            /*if (currentPageId == spSessionConfigPage && sessionAction == saCreateNew)  {
                sessionCopy.totalSensors = 0;
                sessionCopy.alarmtype = aiHH;
                sessionCopy.triggerrate = 3;
                sessionCopy.sessionname = "";
                sensorsCopy.clear();
            }*/
            uipage = new UiSensorListPage(ui->pageFrame, spSensorListPage, gateway, &sessionCopy, &sensorsCopy, sessionAction);
            break;

        case spSensorConfigPage:
            TG_DEBUG("Selected sensor = " << selectedSensorItem << " session action = " << sessionAction);
            uipage = new UiSensorConfigPage(ui->pageFrame, spSensorConfigPage, gateway, &sessionCopy, &sensorsCopy, selectedSensorItem, sessionAction, vKeypad);
            break;

        case spSettingsOptionsPage:
        case spPasswordPage:
            uipage = new UiPasswordEditPage(ui->pageFrame, spPasswordPage, gateway, vKeypad);
            break;
        case spTableDataPage:
            uipage = new UiTableDataPage(ui->pageFrame, spTableDataPage, gateway, &ctx, generalSettings.degreeCelcius);
            break;

        case spNotificationPage:

            // menu to notifications, need to make a modifiable copy notifications
            if (currentPageId == spGeneralSettingsPage) {
                usersCopy.clear();
                const QVector<UserInfo_t>& users = gateway->getUsers();
                usersCopy = users;
            }

            uipage = new UiUserNotificationPage(ui->pageFrame, spNotificationPage, gateway, &usersCopy);
            break;

        case spCredentialPage:
            uipage = new UiCredentialsPage(ui->pageFrame, spCredentialPage, gateway, vKeypad);
            break;

        case spUserAddEditPage:
            uipage = new UiUserAddEditPage(ui->pageFrame, spNotificationPage, gateway, &usersCopy, userSelected, vKeypad);
            break;

        case spAudioVisualPage:
            uipage = new UiAudioVisualPage(ui->pageFrame, spAudioVisualPage, gateway);
            break;

        case spGeneralSettingsPage:
            uipage = new UiGeneralSettingsMenu(ui->pageFrame, spGeneralSettingsPage, gateway);
            break;

        case spSessionFolderPage:
            uipage = new UiSessionFolderViewPage(ui->pageFrame, spSessionFolderPage, gateway, &ctx);
            break;

        case spSessionPreviewPage:
            uipage = new UiSessionPreviewPage(ui->pageFrame, spSessionPreviewPage, gateway, &ctx, generalSettings.degreeCelcius);
            break;

        case spImagesPage:
            uipage = new UiImagesPage(ui->pageFrame, spImagesPage, gateway);
            break;

    default:
        break;
    }


    // connect signal
    QObject::connect(uipage, SIGNAL(gotoPage(WidgetPage)), this, SLOT(on_pageGotoWidget(WidgetPage)));
    // add to layout
    ui->pageFrame->layout()->addWidget(uipage);
    // show
    uipage->show();
}

void DeviceForm::on_gatewayConnectStateChanged(bool connected)
{
    Q_UNUSED(connected);
    // update the rest status bar icons
    updateStatus(false);
}

void DeviceForm::on_gatewayStreamReceived(TgEvStreamClass type)
{
    // make the thermo symbol blink 3 times to indicate temperature stream arrived
    if (type == ecTemp) {
        ui->StatusIncommingTemp->setEnabled(true);
        ui->StatusIncommingTemp->setPixmap(QPixmap(ICON_THERMO));
        incommingTempTimerCount = 0;
        incommingTempTimer.start();
    }

    // update the rest status bar icons
    updateStatus(false);
}

void DeviceForm::on_dateTimeTimerTimeout()
{
    QDateTime dateTime = dateTime.currentDateTime();
    ui->TimeLabel->setText(dateTime.toString("hh:mm"));
    ui->DateLabel->setText(dateTime.toString("ddd dd-MMM-yy"));
}

void DeviceForm::on_incommingTempTimerTimeout()
{
    if (incommingTempTimerCount == 4) {
        ui->StatusIncommingTemp->setEnabled(false);
        ui->StatusIncommingTemp->setPixmap(QPixmap(""));
        incommingTempTimer.stop();
    }
    else {
        incommingTempTimerCount++;
        if (ui->StatusIncommingTemp->isEnabled()) {
            ui->StatusIncommingTemp->setEnabled(false);
            ui->StatusIncommingTemp->clear();
        } else {
            ui->StatusIncommingTemp->setEnabled(true);
            ui->StatusIncommingTemp->setPixmap(QPixmap(ICON_THERMO));
        }
    }
}

/**
 * @brief DeviceForm::on_alarmStatusTimeout
 * @details For individual sensor widgets in UiHomePage to synchronise with this timer,
 *          this slot must invoke another slot
 *
 */
void DeviceForm::on_alarmStatusTimeout()
{
    alarmBlink = !alarmBlink;

    if (!alarmBlink)
        ui->StatusAlarmImage->clear();
    else {
        if (alarmState == daCurrentA1)
            ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A1));
        else if (alarmState == daCurrentA2)
            ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A2));
    }

    const WidgetPage currentPageId = uipage->getPageId();
    if (currentPageId == spHomePage)
        static_cast<UiHomePage *>(uipage)->syncAlarmStateTimeout(alarmBlink);
}

void DeviceForm::initialiseFilterCtx()
{
    const Session_t& session = gateway->getSession();
    const QVector<Sensor_t>& sensors = gateway->getSensors();
    ctx.isCurrentSession = true;
    ctx.sessionNo = session.sessionnumber;
    ctx.selectedSensorIndex = 0;
    ctx.totalSensors = session.totalSensors;
    ctx.a1Checked = false;
    ctx.a2Checked = false;
    ctx.sensorList.clear();
    for (int i=0; i<sensors.count(); i++){
        struct SensorContext sctx;
        sctx.name = sensors.at(i).alias;
        sctx.pos = sensors.at(i).pos;
        quint8 refPos = sensors.at(i).diffmode;
        if (refPos != 0 && refPos != sctx.pos)
            sctx.refName = sensors.at(refPos-1).alias;
        else
            sctx.refName = "";
        sctx.refPos = refPos;
        ctx.sensorList.append(sctx);
    }
}

void DeviceForm::updateStatus(bool initialise)
{
    const GatewayId_t& id = gateway->getId();
    ui->hostnameLabel->setText(id.name);

    if (initialise){
        ui->StatusVpnImage->clear();
        ui->StatusConnectImage->clear();
        ui->StatusPowerImage->clear();
        ui->StatusHostConnect->clear();
        ui->StatusAlarmImage->clear();
        return;
    }

    const Network_t& net = gateway->getNetwork();

    // wan connection ... sometimes hamachi reports logged in but there is no internet connection
    if (gateway->isOnline()){
        ui->StatusVpnImage->setPixmap(QPixmap(ICON_VPN));

        // current network interface
        switch(net.netiface)
        {
           case ctEth:
               ui->StatusConnectImage->setPixmap(QPixmap(ICON_ETH));
               break;

           case ctWifi:
               ui->StatusConnectImage->setPixmap(QPixmap(ICON_WIFI));
               break;

           case ctCell:
               ui->StatusConnectImage->setPixmap(QPixmap(ICON_CELL));
               break;

           case ctNone:
                ui->StatusConnectImage->setPixmap(QPixmap(""));
                    break;
            default:
                break;
        }

        const System_t& sys = gateway->getSystem();

        if (sys.ac)
            ui->StatusPowerImage->setPixmap(QPixmap(ICON_AC));
        else {
            if (sys.batlvl >= 95)
                ui->StatusPowerImage->setPixmap(QPixmap(ICON_BAT_FULL));
            else if (sys.batlvl >= 70)
                ui->StatusPowerImage->setPixmap(QPixmap(ICON_BAT_GOOD));
            else if (sys.batlvl >= 40)
                ui->StatusPowerImage->setPixmap(QPixmap(ICON_BAT_MED));
            else if (sys.batlvl >= 10)
                ui->StatusPowerImage->setPixmap(QPixmap(ICON_BAT_LOW));
            else
                ui->StatusPowerImage->setPixmap(QPixmap(ICON_BAT_LOW));
        }

        if (sys.usb)
            ui->StatusHostConnect->setPixmap(QPixmap(ICON_USB));
        else
            ui->StatusHostConnect->clear();

    } else{
        ui->StatusVpnImage->setPixmap(QPixmap(ICON_NOVPN));
        ui->StatusConnectImage->clear();
        ui->StatusPowerImage->clear();
        ui->StatusHostConnect->clear();
    }

    const Session_t& session = gateway->getSession();

    alarmState = session.alarmstate;

    switch(alarmState)
    {
       case daNone:
           if (alarmStatusTimer.isActive())
               alarmStatusTimer.stop();
           ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A0));
           break;

       case daPastA1:
            if (alarmStatusTimer.isActive())
                alarmStatusTimer.stop();
            ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A1));
           break;

       case daPastA2:
            if (alarmStatusTimer.isActive())
                alarmStatusTimer.stop();
           ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A2));
           break;

       case daCurrentA1:
            if (!alarmStatusTimer.isActive()){
                alarmStatusTimer.start(2000);
                ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A1));
            }
#if defined(TNET_OLIMEX)
            displayOn();
#endif

           break;

       case daCurrentA2:
            if (!alarmStatusTimer.isActive()){
                alarmStatusTimer.start(1000);
                ui->StatusAlarmImage->setPixmap(QPixmap(ICON_STAT_A2));
            }
#if defined(TNET_OLIMEX)
            displayOn();
#endif
           break;

       default:
        break;

    }
}

void DeviceForm::initialiseGeneralSettings()
{
    generalSettings.degreeCelcius = true;
    generalSettings.screenSaverTime = 120000; // 120 seconds
    generalSettings.chartSettings.refPlotVisible = true;
    generalSettings.chartSettings.diffPlotVisible = true;
    generalSettings.chartSettings.legendVisible = true;
    generalSettings.chartSettings.a1PlotVisible = true;
    generalSettings.chartSettings.a2PlotVisible = true;
    generalSettings.chartSettings.vGridVisible = true;
    generalSettings.chartSettings.hGridVisible = false;

}



#if defined(TNET_OLIMEX)
void DeviceForm::displayOn()
{
    QProcess process;
    process.start("xset -display :0 dpms force on");
    process.waitForFinished();
}

void DeviceForm::displayOff()
{
    QProcess process;
    process.start("xset -display :0 dpms force off");
    process.waitForFinished();
}

bool DeviceForm::displayIsOn()
{
    return true;
}

void DeviceForm::on_displayControlTimerTimeout()
{
    QDateTime dateTime = dateTime.currentDateTime();
    qint64 elapsedtime = dateTime.toMSecsSinceEpoch() - lastTouch;
    TG_DEBUG("Last screen interaction = " << elapsedtime/1000 << " seconds");
    // if (alarm && !displayIsOn())
    //  displayOn();
    // else if (!alarm && displayIsOn() && lastTouch > generalSettings.screenSaverTime)
    // displayOff();
}

/**
 * @brief mousePressEvent
 * @param event
 * @details Try capture touch events to
 *          1) Put LCD screen to sleep in the event that there is no activity for 2 mins while no alarms occur
 *          2) Only turn display on when touched and don't trigger an event that activates ui widgets
 */
/*void DeviceForm::mousePressEvent(QMouseEvent *event)
{
    QDateTime dateTime = dateTime.currentDateTime();
    lastTouch = dateTime.toMSecsSinceEpoch();

    event->accept();
}*/

#endif
