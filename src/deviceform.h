#ifndef TESTDEVICEFORM_H
#define TESTDEVICEFORM_H

#include <QMainWindow>
#include <QTimer>
#include "tnetportal.h"
#include "uihomepage.h"
#include "uipagewidget.h"
#include "virtualkeypaddialog.h"

namespace Ui {
class DeviceForm;
}

class DeviceForm : public QMainWindow
{
    Q_OBJECT

public:
    explicit DeviceForm(QWidget *parent = 0, TgGateway *tggateway = 0);
    ~DeviceForm();

protected:
    void closeEvent(QCloseEvent *event);
/*#if defined(TNET_OLIMEX)
    void mousePressEvent(QMouseEvent *event);
#endif*/

signals:
    void formCloseRequest(void);

private slots:
    void on_pageGotoWidget(WidgetPage page);

    void on_gatewayConnectStateChanged(bool connected);
    void on_gatewayStreamReceived(TgEvStreamClass type);

    void on_dateTimeTimerTimeout();
    void on_incommingTempTimerTimeout();
    void on_alarmStatusTimeout();


#if defined(TNET_OLIMEX)
    void on_displayControlTimerTimeout();
#endif

private:
    Ui::DeviceForm *ui;
    TgGateway * gateway;
    VirtualKeypadDialog * vKeypad;
    UiPageWidget * uipage;
    struct FilterContext ctx;
    SettingsContext_t generalSettings;
    bool alarmBlink;
    TgAlarmStatus alarmState;

    QTimer connectTimer;
    QTimer dateTimeTimer;
    QTimer incommingTempTimer;
    QTimer alarmStatusTimer;

    int incommingTempTimerCount;

    // session/sensor config context objects
    enum SessionAction sessionAction;
    Session_t sessionCopy;
    QVector<Sensor_t> sensorsCopy;
    int selectedSensorItem;
    QVector<UserInfo_t> usersCopy;
    int userSelected;

#if defined(TNET_OLIMEX)
    QTimer displayControlTimer;
    qint64 lastTouch;
    void displayOn();
    void displayOff();
    bool displayIsOn();
#endif

    void initialiseFilterCtx();
    void updateStatus(bool initialise);
    void initialiseGeneralSettings();
};

#endif // TESTDEVICEFORM_H
