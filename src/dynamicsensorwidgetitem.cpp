#include <QString>
#include "dynamicsensorwidgetitem.h"
#include "ui_dynamicsensorwidgetitem.h"
#include "common.h"




DynamicSensorWidgetItem::DynamicSensorWidgetItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DynamicSensorWidgetItem)
{
    ui->setupUi(this);
    TG_DEBUG("Constructing dynamic sensor item");
}

DynamicSensorWidgetItem::~DynamicSensorWidgetItem()
{
    delete ui;
    TG_DEBUG("Destructing dynamic sensor item");
}

void DynamicSensorWidgetItem::clear()
{
    ui->posLabel->clear();
    ui->nameLabel->clear();
    ui->tempLabel->clear();
    ui->a1Label->clear();
    ui->a2Label->clear();
    ui->alarmStateLabel->clear();
    ui->refLabel->clear();
}

void DynamicSensorWidgetItem::syncAlarmStateTimeout(bool mark, TgAlarmStatus alarmState)
{
    if (alarmState < daCurrentA1)
        return;

    if (mark){
        if (alarmState == daCurrentA1)
            ui->alarmStateLabel->setPixmap(QPixmap(ICON_SENSOR_A1));
        else if (alarmState == daCurrentA2)
            ui->alarmStateLabel->setPixmap(QPixmap(ICON_SENSOR_A2));
    } else
        ui->alarmStateLabel->clear();
}

void DynamicSensorWidgetItem::updateWidget(const Sensor_t &sensor, bool degreeCelcius)
{
    ui->posLabel->setText(QString("%1").arg(sensor.pos));
    ui->nameLabel->setText(sensor.alias);

    if (sensor.temp == 200.0)
        ui->tempLabel->setText("FAULT");
    else if (degreeCelcius)
        ui->tempLabel->setText(QString::number(sensor.temp, 'f', 1) + QString("%1C").arg(degreeChar));
    else
        ui->tempLabel->setText(QString::number(CelciusToFahrheit(sensor.temp), 'f', 1) + QString("%1F").arg(degreeChar));

    if (degreeCelcius) {
        ui->a1Label->setText(QString("%1%2C").arg(sensor.a1).arg(degreeChar));
        ui->a2Label->setText(QString("%1%2C").arg(sensor.a2).arg(degreeChar));
    } else {
        ui->a1Label->setText(QString("%1%2F").arg(CelciusToFahrheit(sensor.a1)).arg(degreeChar));
        ui->a2Label->setText(QString("%1%2F").arg(CelciusToFahrheit(sensor.a2)).arg(degreeChar));
    }

    switch (sensor.alarmStatus) {
        case daNone:
            ui->alarmStateLabel->clear();
            break;

        case daPastA1:
            ui->alarmStateLabel->setPixmap(QPixmap(ICON_SENSOR_A1));
            break;

        case daPastA2:
            ui->alarmStateLabel->setPixmap(QPixmap(ICON_SENSOR_A2));
            break;

        // controlled by  syncAlarmStateTimeout()
        case daCurrentA1:
        case daCurrentA2:
            break;

        default:
            break;
    }

    if (sensor.diffmode != 0 && sensor.diffmode != sensor.pos)
        ui->refLabel->setPixmap(QPixmap(ICON_SENSOR_LINK));
    else
        ui->refLabel->clear();
}






