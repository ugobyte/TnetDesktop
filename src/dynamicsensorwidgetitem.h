#ifndef DYNAMICSENSORWIDGETITEM_H
#define DYNAMICSENSORWIDGETITEM_H

#include <QWidget>
#include "tnetportal.h"


namespace Ui {
class DynamicSensorWidgetItem;
}

class DynamicSensorWidgetItem : public QWidget
{
    Q_OBJECT

public:
    explicit DynamicSensorWidgetItem(QWidget *parent = 0);
    ~DynamicSensorWidgetItem();

    void clear(void);
    void updateWidget(const Sensor_t& sensor, bool degreeCelcius);
    void syncAlarmStateTimeout(bool mark, TgAlarmStatus alarmState);

private:
    Ui::DynamicSensorWidgetItem *ui;
};

#endif // DYNAMICSENSORWIDGETITEM_H
