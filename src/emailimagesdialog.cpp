#include "emailimagesdialog.h"
#include "ui_emailimagesdialog.h"
#include "emailtablewidgetitem.h"


EmailImagesDialog::EmailImagesDialog(QWidget *parent,
                                     QVector<EmailRecipient_t> *recipientsList,
                                     QVector<EmailImage_t> *imageList) :
    QWidget(parent),
    ui(new Ui::EmailImagesDialog),
    recipients(recipientsList),
    images(imageList)
{
    // delete on close
    setAttribute( Qt::WA_DeleteOnClose, true );

    ui->setupUi(this);

    // populate


    ui->recipientsTableWidget->setColumnCount(1);
    ui->recipientsTableWidget->setRowCount(recipients->count());
    for (int i=0; i<recipients->count(); i++){
        EmailTableWidgetItem * w = new EmailTableWidgetItem(0, i, QString("%1 (%2)").arg(recipients->at(i).name,recipients->at(i).email));
        QObject::connect(w,SIGNAL(on_itemToggled(int,bool)),this, SLOT(on_recipientToggled(int,bool)));
        ui->recipientsTableWidget->setCellWidget(i,0,w);
        ui->recipientsTableWidget->setRowHeight(i,40);
    }

    ui->imagesTableWidget->setColumnCount(1);
    ui->imagesTableWidget->setRowCount(images->count());
    for (int i=0; i<images->count(); i++){
        EmailTableWidgetItem * w = new EmailTableWidgetItem(0, i, images->at(i).name);
        QObject::connect(w,SIGNAL(on_itemToggled(int,bool)),this, SLOT(on_imageToggled(int,bool)));
        ui->imagesTableWidget->setCellWidget(i,0,w);
        ui->imagesTableWidget->setRowHeight(i,40);
    }

    TG_DEBUG("Constructing email images dialog");
}

EmailImagesDialog::~EmailImagesDialog()
{
    delete ui;

    TG_DEBUG("Destructing email images dialog");
}

void EmailImagesDialog::on_recipientToggled(int item, bool state)
{
    EmailRecipient_t& s = recipients->operator [](item);
    s.selected = state;
}

void EmailImagesDialog::on_imageToggled(int item, bool state)
{
    EmailImage_t& s = images->operator [](item);
    s.selected = state;
}



void EmailImagesDialog::on_cancelButton_clicked()
{
    emit exit(false);
}

void EmailImagesDialog::on_sendButton_clicked()
{
    emit exit(true);
}
