#ifndef EMAILIMAGESDIALOG_H
#define EMAILIMAGESDIALOG_H

#include <QWidget>
#include "tnetportal.h"
#include "messagedialog.h"

namespace Ui {
class EmailImagesDialog;
}

typedef struct {
    QString name;
    QString email;
    bool selected;
} EmailRecipient_t;

typedef struct {
    QString path;
    QString name;
    bool selected;
} EmailImage_t;

class EmailImagesDialog : public QWidget
{
    Q_OBJECT

public:
    explicit EmailImagesDialog(QWidget *parent = 0,
                               QVector<EmailRecipient_t> * recipientsList = 0,
                               QVector<EmailImage_t> * imageList = 0);
    ~EmailImagesDialog();

private slots:
    void on_recipientToggled(int item, bool state);
    void on_imageToggled(int item, bool state);

    void on_cancelButton_clicked();
    void on_sendButton_clicked();

signals:
    void exit(bool send);

private:
    Ui::EmailImagesDialog *ui;
    QVector<EmailRecipient_t> * recipients;
    QVector<EmailImage_t> * images;
};

#endif // EMAILIMAGESDIALOG_H
