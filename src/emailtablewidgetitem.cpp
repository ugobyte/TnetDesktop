#include "emailtablewidgetitem.h"
#include "ui_emailtablewidgetitem.h"
#include "tnetportal.h"

EmailTableWidgetItem::EmailTableWidgetItem(QWidget *parent, int itemNo, const QString itemText) :
    QWidget(parent),
    ui(new Ui::EmailTableWidgetItem),
    itemIndex(itemNo)
{
    ui->setupUi(this);
    ui->textLabel->setText(itemText);
    ui->checkbox->setChecked(false);
    TG_DEBUG("Constructing email widget item");
}

EmailTableWidgetItem::~EmailTableWidgetItem()
{
    delete ui;
    TG_DEBUG("Destructing email widget item");
}

void EmailTableWidgetItem::on_checkbox_toggled(bool checked)
{
    emit on_itemToggled(itemIndex, checked);
}
