#ifndef EMAILTABLEWIDGETITEM_H
#define EMAILTABLEWIDGETITEM_H

#include <QWidget>

namespace Ui {
class EmailTableWidgetItem;
}

class EmailTableWidgetItem : public QWidget
{
    Q_OBJECT

public:
    explicit EmailTableWidgetItem(QWidget *parent = 0, int itemNo=-1, const QString itemText = "");
    ~EmailTableWidgetItem();

signals:
    void on_itemToggled(int itemNo, bool state);

private slots:
    void on_checkbox_toggled(bool checked);

private:
    Ui::EmailTableWidgetItem *ui;
    int itemIndex;
};

#endif // EMAILTABLEWIDGETITEM_H
