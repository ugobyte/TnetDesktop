#include <QDir>
#include <QString>
#include <QStringList>
#include <QSystemTrayIcon>

#include "explorer.h"
#include "ui_explorer.h"
#include "adddevicedialog.h"
#include "gatewaywidgetitem.h"

/**
 * @brief Explorer::Explorer
 * @param parent
 * @param logger
 * @param config
 */
Explorer::Explorer(QWidget *parent, const QString appPath) :
    QMainWindow(parent),
    ui(new Ui::Explorer),
    deviceForm(0),
    rootPath(appPath), 
    msgDialog(0),
    newGateway(0),
    trayIcon(0),
    globalAlarmState(daNone),
    waitingNewDeviceConnect(false)
{
    createActions();
    createTrayIcon();

    trayIcon->show();

    showTrayMessage("Remote Temperature Monitoring Software");
    // for tray icon
    QObject::connect(trayIcon, SIGNAL(messageClicked()), this, SLOT(on_trayIconMessageClicked()));
    QObject::connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)) , this, SLOT(on_trayIconActivated(QSystemTrayIcon::ActivationReason)));


    ui->setupUi(this);
    showMaximized();

    // hide options menu
    ui->menuOptionsWidget->hide();

    loadAllGateways();

    checkAllGatewayAlarms();
    updateTrayState();

    TG_DEBUG("Constructing Explorer form");
}

/**
 * @brief Explorer::~Explorer
 */
Explorer::~Explorer()
{
    try {
        if (newGateway)
            delete newGateway;

        // delete gateways
        while (!gateways.empty()) {
            TgGateway * thisGateway = gateways.takeFirst();
            thisGateway->disconnectStream();
            delete thisGateway;
        }

    } catch (TgException &e){
        TG_ERROR("Excepion e = " << e.what());
    }

    delete deviceForm;

    delete ui;

    TG_DEBUG("Destructing Explorer form");
}

void Explorer::closeEvent(QCloseEvent *event)
{

#ifdef Q_OS_OSX
    if (!event->spontaneous() || !isVisible()) {
        return;
    }
#endif

    if (trayIcon->isVisible()) {
        QMessageBox::information(this, tr("Tempnetz"),
                                 tr("Tempnetz will keep running in the "
                                    "system tray. To terminate it, "
                                    "choose <b>Quit</b> in the context menu "
                                    "of the system tray entry."));
        hide();
        event->ignore();
    }

}


/**
 * @brief Explorer::updateTable
 */
void Explorer::updateTable()
{
    ui->tableWidget->clear();
    ui->tableWidget->setColumnCount(1);
    ui->tableWidget->setRowCount(gateways.count());

    for (int i=0; i<gateways.count(); i++) {
        ui->tableWidget->setRowHeight(i, 60);
        GatewayWidgetItem * item = new GatewayWidgetItem(0, gateways.at(i));
        ui->tableWidget->setCellWidget(i,0,item);
    }
}

void Explorer::createActions()
{
    minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, &QAction::triggered, this, &QWidget::hide);

    maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(maximizeAction, &QAction::triggered, this, &QWidget::showMaximized);

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, &QAction::triggered, this, &QWidget::showNormal);

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);
}

void Explorer::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(maximizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);

    QIcon icon(ICON_TRAY_NO_ALARM);
    trayIcon->setIcon(icon);
    setWindowIcon(icon);

    trayIcon->setToolTip("Tempnetz");
}

void Explorer::updateTrayState()
{
    QIcon icon;

    switch (globalAlarmState) {
        case daNone:
            trayIcon->setToolTip("Tempnetz - no alarm");
            icon.addPixmap(QPixmap(ICON_TRAY_NO_ALARM));
            break;

        case daPastA1:
            trayIcon->setToolTip("Tempnetz - past A1 alarm");
            icon.addPixmap(QPixmap(ICON_TRAY_ALARM1));
            break;

        case daPastA2:
            trayIcon->setToolTip("Tempnetz - past A2 alarm");
            icon.addPixmap(QPixmap(ICON_TRAY_ALARM2));
            break;

        case daCurrentA1:
            trayIcon->setToolTip("Tempnetz - current A1 alarm");
            icon.addPixmap(QPixmap(ICON_TRAY_ALARM1));
            break;

        case daCurrentA2:
            trayIcon->setToolTip("Tempnetz - current A2 alarm");
            icon.addPixmap(QPixmap(ICON_TRAY_ALARM2));
            break;

        default:
            trayIcon->setToolTip("Tempnetz");
            icon.addPixmap(QPixmap(ICON_TRAY_NO_ALARM));
            break;
    }

    trayIcon->setIcon(icon);
}

void Explorer::showTrayMessage(QString msg)
{
    trayIcon->showMessage("Tempnetz", msg, QIcon(ICON_LOGO), 5000);
}

/**
 * @brief Explorer::connectNewGateway
 * @param ipAddress
 */
void Explorer::connectNewGateway(const QString ipAddress)
{
    QString gatewayPath = QString(TNET_DEVICE_DIR).arg(rootPath, "NewDevice");

    QDir dir(gatewayPath);
    dir.mkdir(gatewayPath);

    if (!dir.exists()){
        TG_ERROR("Unable to create dir " << gatewayPath);
        return;
    }

    QString configPath = QString(TNET_DEVICE_CONFIG_DIR).arg(gatewayPath);
    dir.mkdir(configPath);

    TG_DEBUG("Adding new gateway at " << gatewayPath.toLatin1().data());

    // create gateway object
    newGateway = new TgGateway(this, gatewayPath);
    QObject::connect(newGateway, SIGNAL(requestResult(bool,QString)), this, SLOT(onGatewayConfigRequestDone(bool,QString)));
    newGateway->connectFirstTime(ipAddress);
    waitingNewDeviceConnect = true;
}

/**
 * @brief Explorer::addGateway
 * @param serialNumber
 */
void Explorer::addGateway(const QString serialNumber)
{
    // gateway path
    QString gatewayPath = QString(TNET_DEVICE_DIR).arg(rootPath, serialNumber);
    TG_DEBUG("Adding gateway " << serialNumber << " at " << gatewayPath.toLatin1().data());

    // create gateway object
    TgGateway * gw = new TgGateway(this, gatewayPath);
    QObject::connect(gw, SIGNAL(streamReceived(TgEvStreamClass)), this, SLOT(on_gatewayStreamReceived(TgEvStreamClass)));
    gateways.append(gw);
}

/**
 * @brief Explorer::loadAllGateways
 */
void Explorer::loadAllGateways()
{
    // devices dir
    QDir devicesDir(QString(TNET_DEVICES_DIR).arg(rootPath));

    if (!devicesDir.exists()) {
        TG_ERROR("Gateway path does not exist path=" << devicesDir.dirName());
        return;
    }

    QStringList deviceList = devicesDir.entryList(QStringList("tg*"));

    for (int i=0; i<deviceList.count(); i++)
        addGateway(deviceList.at(i));


    // create widget items and add to table
    updateTable();

    TG_INFO("Found " << gateways.count() << " gateways");
    if (gateways.count() == 0)
        ui->mainStackedPage->setCurrentIndex(spNoGateways);
    else
        ui->mainStackedPage->setCurrentIndex(spGatewayList);
}

/**
 * @brief Explorer::makeGatewaySpace
 */
void Explorer::makeGatewaySpace(const QString serial)
{
    QString newGatewayPath = QString(TNET_DEVICE_DIR).arg(rootPath,serial);
    QString oldGatewayPath = QString(TNET_DEVICE_DIR).arg(rootPath,"NewDevice");

    QDir dir;

    // create session folder
    QString gatewaySessionDir = QString(TNET_DEVICE_ROOT_SESSION_DIR).arg(oldGatewayPath);
    dir.mkdir(gatewaySessionDir);

    // create event folder
    QString gatewayEventDir = QString(TNET_DEVICE_EVENT_DIR).arg(oldGatewayPath);
    dir.mkdir(gatewayEventDir);

    // change {APP_FOLDER}/Device/"NewDevice" to "serial"
    if (!dir.rename(oldGatewayPath, newGatewayPath))
        TG_ERROR("Rename folder to " << serial << " failed");
    else
        TG_DEBUG("Folder space created for " << serial);
}

/**
 * @brief Explorer::showLoader
 * @param msg
 * @param spin
 */
void Explorer::showLoader(const QString msg, bool spin)
{
    if (msgDialog == Q_NULLPTR) {
        msgDialog = new MessageDialog(this, msg, spin);
        msgDialog->show();
    }
}

/**
 * @brief Explorer::hideLoader
 */
void Explorer::hideLoader()
{
    if (msgDialog != Q_NULLPTR){
        msgDialog->close();
        msgDialog->deleteLater();
        msgDialog = Q_NULLPTR;
    }
}


bool Explorer::checkAllGatewayAlarms()
{
    TgAlarmStatus preserveAlarmState = globalAlarmState;
    globalAlarmState = daNone;

    for (QList<TgGateway *>::const_iterator it = gateways.begin(); it != gateways.end(); ++it) {
        const Session_t& session = (*it)->getSession();
        TgAlarmStatus alarmstate = session.alarmstate;
        if ( alarmstate > globalAlarmState)
            globalAlarmState = alarmstate;
    }

    TG_DEBUG("Alarm state system wide = " << globalAlarmState);


    if (preserveAlarmState != globalAlarmState)
        return true;
    else
        return false;
}

/**
 * @brief Explorer::on_menuButton_clicked
 */
void Explorer::on_menuButton_clicked()
{
    if (ui->menuOptionsWidget->isVisible())
        ui->menuOptionsWidget->hide();
    else
        ui->menuOptionsWidget->show();
}


/**
 * @brief Explorer::onDeviceWindowCloseRequest
 */
void Explorer::onDeviceWindowCloseRequest()
{
    TG_DEBUG("Received close request from device window");
    deviceForm->deleteLater();
    deviceForm = Q_NULLPTR;
}

/**
 * @brief Explorer::onGatewayConfigRequestDone
 * @param result
 * @param repr
 */
void Explorer::onGatewayConfigRequestDone(bool result, QString repr)
{
    Q_UNUSED(repr);
    if(waitingNewDeviceConnect) {
        if (result) {
            waitingNewDeviceConnect = false;
            hideLoader();
            const GatewayId_t& id = newGateway->getId();
            makeGatewaySpace(id.serial);
            addGateway(id.serial);
            updateTable();
            ui->mainStackedPage->setCurrentIndex(spGatewayList);
            delete newGateway;
            newGateway = Q_NULLPTR;
            TG_DEBUG("New device added");
        } else {
            TG_ERROR("Failed to add new device");
            delete newGateway;
            newGateway = Q_NULLPTR;
        }
    }
}

/**
 * @brief Explorer::on_gatewayAddButton_clicked
 */
void Explorer::on_gatewayAddButton_clicked()
{
    AddDeviceDialog * addDeviceDlg = new AddDeviceDialog(this);
    int result = addDeviceDlg->exec();
    TG_DEBUG("Add device dialog result = " << result);
    if (result) {

        // show spinner
        connectNewGateway(addDeviceDlg->getIpAddres());
        delete addDeviceDlg;
        showLoader("Connecting ...", true);
    }

}

void Explorer::on_tableWidget_clicked(const QModelIndex &index)
{
    TG_DEBUG("Selected row = " << index.row());
    if (deviceForm == Q_NULLPTR){
        //deviceWindow = new Device(0, gateways.at(index.row()));
        deviceForm = new DeviceForm(0,gateways.at(index.row()));
        QObject::connect(deviceForm, SIGNAL(formCloseRequest()), this, SLOT(onDeviceWindowCloseRequest()));
        deviceForm->show();
    }
}

void Explorer::on_trayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
        case QSystemTrayIcon::Trigger:
        case QSystemTrayIcon::DoubleClick:
        case QSystemTrayIcon::MiddleClick:
            break;
        default:
            ;
    }
}

void Explorer::on_trayIconMessageClicked()
{

}

/**
 * @brief Explorer::on_gatewayStreamReceived
 * @param evtype
 * @details This is to update the tray icon with correct alarm status
 *          Everytime a gateway stream received, go find the overall alarm status i.e. Set global status as highest
 *          of all gateways
 */
void Explorer::on_gatewayStreamReceived(TgEvStreamClass evtype)
{
    if (evtype == ecLive){
        bool alarmStateChanged = checkAllGatewayAlarms();
        updateTrayState();
        if (alarmStateChanged)
            showTrayMessage("Alarm state changed");
    }
}
