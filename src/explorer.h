#ifndef EXPLORER_H
#define EXPLORER_H

#include <QMainWindow>


#include "tnetportal.h"
#include "device.h"
#include "messagedialog.h"
#include "deviceform.h"

typedef struct{
    QString name;
    QString desc;
    QString serial;
    bool online;
    QString netIp;
    TgAlarmStatus alarmStatus;
} GatewayItemModel_t;

namespace Ui {
class Explorer;
class QAction;
class QMenu;
class QStringList;
class QString;
class QDir;
class QSystemTrayIcon;
}


enum StackPage {spNoGateways = 0, spGatewayList, spCreateGateway};


class Explorer : public QMainWindow
{
    Q_OBJECT

public:
    explicit Explorer(QWidget *parent = 0, const QString appPath="");
    ~Explorer();

protected:
    void closeEvent(QCloseEvent * event);

public slots:
    void onDeviceWindowCloseRequest();
    void onGatewayConfigRequestDone(bool result, QString repr);

private slots:
    void on_menuButton_clicked();
    void on_gatewayAddButton_clicked();
    void on_tableWidget_clicked(const QModelIndex &index);

    void on_trayIconActivated(QSystemTrayIcon::ActivationReason reason);
    void on_trayIconMessageClicked();

    void on_gatewayStreamReceived(TgEvStreamClass evtype);
private:
    Ui::Explorer *ui;
    DeviceForm * deviceForm;
    const QString rootPath;
    QList<TgGateway *> gateways;
    MessageDialog * msgDialog;
    TgGateway * newGateway;
    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
    TgAlarmStatus globalAlarmState;
    bool waitingNewDeviceConnect;

    QAction *minimizeAction;
    QAction *maximizeAction;
    QAction *restoreAction;
    QAction *quitAction;

    void updateTable(void);
    void createActions();
    void createTrayIcon();
    void updateTrayState();
    void showTrayMessage(QString msg);

    void connectNewGateway(const QString ipAddress);
    void addGateway(const QString serialNumber);
    //void removeGateway(const QString serialNumber);
    void loadAllGateways(void);
    void makeGatewaySpace(const QString serial);

    void showLoader(const QString msg, bool spin);
    void hideLoader(void);

    bool checkAllGatewayAlarms(void);
};

#endif // EXPLORER_H
