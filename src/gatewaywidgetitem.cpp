#include "gatewaywidgetitem.h"
#include "ui_gatewaywidgetitem.h"
#include "common.h"

GatewayWidgetItem::GatewayWidgetItem(QWidget *parent, TgGateway *gateway) :
    QWidget(parent),
    ui(new Ui::GatewayWidgetItem),
    gatewayModel(gateway),
    blink(false),
    connected(false),
    gifSpinner(0)
{
    ui->setupUi(this);

    // connect timer timeout
    QObject::connect(&connectTimer, SIGNAL(timeout()), this, SLOT(on_connectTimerTimeout()));
    connectTimer.start(20000);

    // alarm state timer signal/slot
    QObject::connect(&alarmStateTimer, SIGNAL(timeout()), this, SLOT(on_alarmStateTimerTimeout()));

    // gateway stream received event signal/slot
    QObject::connect(gatewayModel, SIGNAL(streamReceived(TgEvStreamClass)), this, SLOT(on_gatewayStreamReceived(TgEvStreamClass)));
    QObject::connect(gatewayModel, SIGNAL(connectionStateChange(bool)), this, SLOT(on_gatewayConnectStateChanged(bool)));

    if (!gatewayModel->loadConfigGateway())
        TG_WARNING("Unable to load gateway config");

    if (!gatewayModel->loadConfigNetwork())
        TG_WARNING("Unable to load network config");

    if (!gatewayModel->loadConfigTemperature())
        TG_WARNING("Unable to load temperature config");

    // connect
    const Network_t& netinfo = gatewayModel->getNetwork();
    gatewayModel->setIpAddress(netinfo.hamIp);
    gatewayModel->requestAllConfig();
    gatewayModel->connectStream();

    // update widgets
    updateUi();

    TG_DEBUG("Constructing gateway widget item");
}

GatewayWidgetItem::~GatewayWidgetItem()
{
    delete ui;

    if (gifSpinner)
        delete gifSpinner;

    TG_DEBUG("Destructing gateway widget item");
}

void GatewayWidgetItem::on_connectTimerTimeout()
{
    connectTimer.stop();

    if (gifSpinner){
        gifSpinner->stop();
        delete gifSpinner;
        gifSpinner = 0;
    }

    ui->connectImage->setPixmap(QPixmap(ICON_NOVPN));
}

void GatewayWidgetItem::on_alarmStateTimerTimeout()
{
    // clear or set
    if (blink) {
        const Session_t& session = gatewayModel->getSession();

        if (session.alarmstate == daCurrentA1)
            ui->alarmImage->setPixmap(QPixmap(ICON_STAT_A1));
        else if (session.alarmstate == daCurrentA2)
            ui->alarmImage->setPixmap(QPixmap(ICON_STAT_A2));
        blink = false;
    } else {
        ui->alarmImage->setPixmap(QPixmap());
        blink = true;
    }
}

void GatewayWidgetItem::on_gatewayStreamReceived(TgEvStreamClass evType)
{
    // ignore live status events
    if (evType != ecLive)
        return;

    connectTimer.stop();
    connected = true;

    // update ui
    updateUi();
}

void GatewayWidgetItem::on_gatewayConnectStateChanged(bool connected)
{
    Q_UNUSED(connected);
    updateUi();
}

void GatewayWidgetItem::updateUi()
{
    const GatewayId_t& id = gatewayModel->getId();
    const Network_t& net = gatewayModel->getNetwork();
    const Session_t& session = gatewayModel->getSession();

    // set name
    ui->nameLabel->setText(id.name);

    // set connection state
    if (!connected){
        gifSpinner = new QMovie(GIF_SPINNER);
        gifSpinner->setScaledSize(QSize(50,50));
        if (!gifSpinner->isValid())
            TG_WARNING("Gif spinner qmovie not valid");
        ui->connectImage->setMovie(gifSpinner);
        gifSpinner->start();
    } else {
        if (gifSpinner){
            gifSpinner->stop();
            delete gifSpinner;
            gifSpinner = 0;
        }

        if (gatewayModel->isOnline())
            ui->connectImage->setPixmap(QPixmap(ICON_VPN));
        else
            ui->connectImage->setPixmap(QPixmap(ICON_NOVPN));
    }

    // set alarm state
    switch( session.alarmstate ) {
        case daNone:
            ui->alarmImage->setPixmap(QPixmap(ICON_STAT_A0));
            if (alarmStateTimer.isActive())
                alarmStateTimer.stop();
            break;
        case daPastA1:
            ui->alarmImage->setPixmap(QPixmap(ICON_STAT_A1));
            if (alarmStateTimer.isActive())
                alarmStateTimer.stop();
            break;
        case daPastA2:
            ui->alarmImage->setPixmap(QPixmap(ICON_STAT_A2));
            if (alarmStateTimer.isActive())
                alarmStateTimer.stop();
            break;
        case daCurrentA1:
            ui->alarmImage->setPixmap(QPixmap(ICON_STAT_A1));
            alarmStateTimer.start(2000);
            break;
        case daCurrentA2:
            ui->alarmImage->setPixmap(QPixmap(ICON_STAT_A2));
            alarmStateTimer.start(1000);
            break;

    default:
        break;
    }
}
