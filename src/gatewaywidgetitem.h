#ifndef GATEWAYWIDGETITEM_H
#define GATEWAYWIDGETITEM_H

#include <QWidget>
#include <QTimer>
#include <QMovie>
#include "tnetportal.h"

namespace Ui {
class GatewayWidgetItem;
}

class GatewayWidgetItem : public QWidget
{
    Q_OBJECT

public:
    explicit GatewayWidgetItem(QWidget *parent = 0, TgGateway * gateway = 0);
    ~GatewayWidgetItem();

private slots:
    void on_connectTimerTimeout();
    void on_alarmStateTimerTimeout();
    void on_gatewayStreamReceived(TgEvStreamClass evType);
    void on_gatewayConnectStateChanged(bool connected);

private:
    Ui::GatewayWidgetItem *ui;
    TgGateway * gatewayModel;
    QTimer alarmStateTimer;
    QTimer connectTimer;
    bool blink;
    bool connected;
    QMovie * gifSpinner;
    void updateUi();
};

#endif // GATEWAYWIDGETITEM_H
