#include "tnetportal.h"
#include <QApplication>
#include <QResource>

#if QT_VERSION >= QT_VERSION_CHECK(5,7,0)
#include "explorer.h"
#include <QStandardPaths>
#include <iostream>
#include "tnetsingleapplication.h"



void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "D%s: %s (%s:%u, %s)\n", QDateTime::currentDateTime().toString("ddMMyy hhmmss.zzz ").toLatin1().data(),
                localMsg.constData(),
                context.file,
                context.line,
                context.function);
        break;
    case QtInfoMsg:
        fprintf(stderr, "I%s: %s (%s:%u, %s)\n", QDateTime::currentDateTime().toString("ddMMyy hhmmss.zzz ").toLatin1().data(),
                localMsg.constData(),
                context.file,
                context.line,
                context.function);
        break;
    case QtWarningMsg:
        fprintf(stderr, "W%s: %s (%s:%u, %s)\n", QDateTime::currentDateTime().toString("ddMMyy hhmmss.zzz ").toLatin1().data(),
                localMsg.constData(),
                context.file,
                context.line,
                context.function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "C%s: %s (%s:%u, %s)\n", QDateTime::currentDateTime().toString("ddMMyy hhmmss.zzz ").toLatin1().data(),
                localMsg.constData(),
                context.file,
                context.line,
                context.function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "F%s: %s (%s:%u, %s)\n", QDateTime::currentDateTime().toString("ddMMyy hhmmss.zzz ").toLatin1().data(),
                localMsg.constData(),
                context.file,
                context.line,
                context.function);
        abort();
    }
}

int main(int argc, char *argv[])
{
    TnetSingleApplication a(argc, argv);

    // another instance already running ?
    if(!a.lock())
        return -42;

    // message handler
    qInstallMessageHandler(myMessageOutput);

    // app data path
    QString appDataPath = QStandardPaths::locate(QStandardPaths::GenericDataLocation, QString("Tempnetz"), QStandardPaths::LocateDirectory);
    TG_WARNING("App data path = " << appDataPath);

    // create explorer widget
    Explorer w(0, appDataPath);
    w.setWindowFlags( Qt::WindowMaximizeButtonHint | Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint );
    w.show();

    return a.exec();
}

#elif QT_VERSION <= QT_VERSION_CHECK(4,8,7)

//#include "device.h"
#include "deviceform.h"

void myMessageOutput(QtMsgType type, const char* msg)
{
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "D%s: %s\n", QDateTime::currentDateTime().toString("ddMMyy hhmmss.zzz ").toLatin1().data(),
                msg);
        break;

    case QtWarningMsg:
        fprintf(stderr, "W%s: %s\n", QDateTime::currentDateTime().toString("ddMMyy hhmmss.zzz ").toLatin1().data(),
                msg);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "C%s: %s\n", QDateTime::currentDateTime().toString("ddMMyy hhmmss.zzz ").toLatin1().data(),
                msg);
        break;
    case QtFatalMsg:
        fprintf(stderr, "F%s: %s\n", QDateTime::currentDateTime().toString("ddMMyy hhmmss.zzz ").toLatin1().data(),
                msg);
        abort();
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // log message handler
    qInstallMsgHandler(myMessageOutput);

    // load external resource tnetimgres.rcc
   /* if (!QResource::registerResource("/usr/lib/tnetimgres.rcc")){
        TG_WARNING("Unable to register external resource file");
    }*/

    // create gateway and set ip to localhost
    TgGateway gateway(0, "/home/tgard");
    gateway.setIpAddress("127.0.0.1");

    // create the main widget
    DeviceForm w(0,&gateway);

    //Device w(0, &gateway);
    w.setCursor(Qt::BlankCursor);
    w.setAttribute(Qt::WA_AcceptTouchEvents, true);
    w.show();
    return a.exec();
}

#endif


