#include "messagedialog.h"
#include "ui_messagedialog.h"
#include "common.h"
#include "tnetportal.h"

/**
 * @brief MessageDialog::MessageDialog
 * @param parent
 * @param message
 * @param showLoader
 */
MessageDialog::MessageDialog(QWidget *parent, const QString message, bool showLoader) :
    QWidget(parent),
    ui(new Ui::MessageDialog),
    loaderOn(showLoader),
    resultSet(false),
    gifSpinner(NULL)
{
    ui->setupUi(this);

    // delete on close
    setAttribute( Qt::WA_DeleteOnClose, true );

    // set to parent geometry
    setGeometry(0,0,parent->geometry().width(),parent->geometry().height());

    ui->messageLabel->setText(message);
    QObject::connect(&loaderTimer, SIGNAL(timeout()), this, SLOT(onLoaderTimerTimeout()));

    if (loaderOn){
        gifSpinner = new QMovie(GIF_SPINNER);
        gifSpinner->setScaledSize(QSize(50,50));
        if (!gifSpinner->isValid())
            TG_WARNING("Gif spinner qmovie not valid");
        ui->loaderImage->setMovie(gifSpinner);
        gifSpinner->start();
    }

    TG_DEBUG("Message dialog constructing");
}

/**
 * @brief MessageDialog::~MessageDialog
 */
MessageDialog::~MessageDialog()
{
    if (gifSpinner)
        delete gifSpinner;

    TG_DEBUG("Message dialog destructing");
    delete ui;
}

/**
 * @brief MessageDialog::setMessage
 * @param newMsg
 */
void MessageDialog::setMessage(const QString newMsg)
{
    ui->messageLabel->setText(newMsg);
}

/**
 * @brief MessageDialog::setResult
 * @param newMsg
 * @param result
 */
void MessageDialog::setResult(const QString newMsg, bool result)
{
    if (newMsg != "")
        ui->messageLabel->setText(newMsg);

    if (gifSpinner){
        gifSpinner->stop();
        delete gifSpinner;
        gifSpinner = NULL;
    }

    if (result)
        ui->loaderImage->setPixmap(QPixmap(ICON_CONFIRM_OK));
    else
        ui->loaderImage->setPixmap(QPixmap(ICON_CONFIRM_KO));

    resultSet = true;
    loaderTimer.start(3000);
}

/**
 * @brief MessageDialog::onLoaderTimerTimeout
 */
void MessageDialog::onLoaderTimerTimeout()
{
    loaderTimer.stop();
    close();
}

/**
 * @brief MessageDialog::on_cancelButton_clicked
 */
void MessageDialog::on_cancelButton_clicked()
{
    close();
}
