#ifndef MESSAGEDIALOG_H
#define MESSAGEDIALOG_H

#include <QWidget>
#include <QTimer>
#include <QMovie>

namespace Ui {
class MessageDialog;
}

class MessageDialog : public QWidget
{
    Q_OBJECT

public:
    explicit MessageDialog(QWidget *parent = 0, const QString message = "", bool showLoader=false);
    ~MessageDialog();

    void setMessage(const QString newMsg);
    void setResult(const QString newMsg="", bool result=false);

private slots:
    void onLoaderTimerTimeout(void);
    void on_cancelButton_clicked();

private:
    Ui::MessageDialog *ui;
    bool loaderOn;
    QTimer loaderTimer;
    bool resultSet;
    QMovie *gifSpinner;
};

#endif // MESSAGEDIALOG_H
