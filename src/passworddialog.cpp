#include "passworddialog.h"
#include "ui_passworddialog.h"
#include "common.h"
#include "tnetportal.h"

PasswordDialog::PasswordDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PasswordDialog)
{
    QObject::connect(&resultTimer, SIGNAL(timeout()), this, SLOT(onResultTimerTimeout()));
    ui->setupUi(this);

    // delete on close
    setAttribute( Qt::WA_DeleteOnClose, true );

    // set to parent geometry
    setGeometry(0,0,parent->geometry().width(),parent->geometry().height());

    ui->passwordLineEdit->setFocus();
    ui->AccessResult->clear();
}

PasswordDialog::~PasswordDialog()
{
    delete ui;
    TG_DEBUG("Password dialog destructing");
}

void PasswordDialog::setResult(bool result)
{
    ui->passwordLineEdit->setGeometry(0,0,0,0);
    resultTimer.start(2000);
    if (result)
        ui->AccessResult->setPixmap(QPixmap(ICON_CONFIRM_OK));
    else
        ui->AccessResult->setPixmap(QPixmap(ICON_CONFIRM_KO));
}

void PasswordDialog::onResultTimerTimeout()
{
    resultTimer.stop();
    close();
}

void PasswordDialog::on_passwordLineEdit_returnPressed()
{
    emit passwordEntered(ui->passwordLineEdit->text());
    ui->okButton->setEnabled(false);
}

void PasswordDialog::on_okButton_clicked()
{
    emit passwordEntered(ui->passwordLineEdit->text());
    ui->okButton->setEnabled(false);
}

void PasswordDialog::on_cancelButton_clicked()
{
    emit onCancel();
    close();
}
