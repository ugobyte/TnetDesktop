#ifndef PASSWORDDIALOG_H
#define PASSWORDDIALOG_H

#include <QWidget>
#include <QTimer>


namespace Ui {
class PasswordDialog;
}

class PasswordDialog : public QWidget
{
    Q_OBJECT

public:
    explicit PasswordDialog(QWidget *parent = 0);
    ~PasswordDialog();

    void setResult(bool result);

signals:
    void passwordEntered(const QString password);
    void onCancel();

private slots:
    void onResultTimerTimeout();
    void on_passwordLineEdit_returnPressed();

    void on_okButton_clicked();
    void on_cancelButton_clicked();

private:
    Ui::PasswordDialog *ui;
    QString hash;
    QTimer resultTimer;
};

#endif // PASSWORDDIALOG_H
