#include "searchnetsensor.h"
#include "ui_searchnetsensor.h"
#include "common.h"



SearchNetSensor::SearchNetSensor(QWidget *parent,
                                 const Session_t &sessionCfg,
                                 const Sensor_t &sensorCfg,
                                 bool createNewMode,
                                 VirtualKeypadDialog * virtualkeypad) :
    QWidget(parent),
    ui(new Ui::searchnetsensor),
    sensorConfig(sensorCfg),
    sessionConfig(sessionCfg),
    vkpad(virtualkeypad),
    scanActive(false)
{
    ui->setupUi(this);
    gifSpinner = 0;
    sensorCtl = 0;
    createMode = createNewMode;

    // create mode
    if (createMode) {
        ui->scanNetworkButton->setEnabled(true);
        ui->posSpinbox->setEnabled(true);
        ui->refSpinbox->setEnabled(true);
        ui->sensorSerialEdit->setEnabled(true);
        QObject::connect(&workerTimer, SIGNAL(timeout()), this, SLOT(on_workerTimerTimeout()));
    // resume mode
    } else {
        ui->scanNetworkButton->setEnabled(false);
        ui->posSpinbox->setEnabled(false);
        ui->refSpinbox->setEnabled(false);
        ui->sensorSerialEdit->setEnabled(false);
    }

    // populate fields
    ui->posSpinbox->setValue(sensorConfig.pos);
    ui->sensorNameEdit->setText(sensorConfig.alias);
    ui->sensorSerialEdit->setText(sensorConfig.serial);
    ui->a1Spinbox->setValue(sensorConfig.a1);
    ui->a2Spinbox->setValue(sensorConfig.a2);
    ui->refSpinbox->setValue(sensorConfig.diffmode);

    ui->sensorNameEdit->setFocus();

    TG_WARNING("Edit sensor dialog constructing");
}

SearchNetSensor::~SearchNetSensor()
{
    delete ui;
    if (gifSpinner)
        delete gifSpinner;

    if (sensorCtl)
        delete sensorCtl;

    TG_WARNING("Edit sensor dialog destructing");
}

const Sensor_t SearchNetSensor::getNewConfig()
{
    return sensorConfig;
}

void SearchNetSensor::on_okButton_clicked()
{
    sensorConfig.alias = ui->sensorNameEdit->text();
    if (createMode) {
        if (sensorConfig.serial != ui->sensorSerialEdit->text())
            sensorConfig.serial = ui->sensorSerialEdit->text();
    }

    if (sensorConfig.alias != ui->sensorNameEdit->text())
        sensorConfig.alias = ui->sensorNameEdit->text();

    emit finished(true);
}

void SearchNetSensor::on_cancelButton_clicked()
{
    emit finished(false);
}

void SearchNetSensor::onSensorFound(bool result, const QString serial)
{
    workerTimer.stop();
    ui->scanNetworkButton->setEnabled(true);
    scanActive = false;
    if (gifSpinner){
        gifSpinner->stop();
        delete gifSpinner;
        gifSpinner = 0;
    }

    if (result) {
        ui->loaderImage->setPixmap(QPixmap(ICON_CONFIRM_OK));
        ui->sensorSerialEdit->setText(serial);
    } else {
        ui->loaderImage->setPixmap(QPixmap(ICON_CONFIRM_KO));
    }
}

void SearchNetSensor::on_scanNetworkButton_clicked()
{
    workerTimer.start(10000);

    ui->scanNetworkButton->setEnabled(false);
    scanActive = true;
    gifSpinner = new QMovie(GIF_SPINNER);
    gifSpinner->setScaledSize(QSize(50,50));
    if (!gifSpinner->isValid())
        TG_WARNING("Gif spinner qmovie not valid");
    ui->loaderImage->setMovie(gifSpinner);
    gifSpinner->start();



    worker = new QThread;
    sensorCtl = new TnetSensorCtl;
    QObject::connect(sensorCtl, SIGNAL(sensorFound(bool,QString)), this, SLOT(onSensorFound(bool,QString)));
    QObject::connect(worker, SIGNAL(started()), sensorCtl, SLOT(findSensor()));
    QObject::connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));

    sensorCtl->moveToThread(worker);
}

void SearchNetSensor::on_sensorNameEdit_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1);
    Q_UNUSED(arg2);

#if defined(TNET_OLIMEX)
    vkpad->show();
#endif
}

void SearchNetSensor::on_sensorSerialEdit_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1);
    Q_UNUSED(arg2);

    if (scanActive)
        return;
#if defined(TNET_OLIMEX)
    vkpad->show();
#endif
}


void SearchNetSensor::on_posSpinbox_valueChanged(int arg1)
{
    sensorConfig.pos = arg1;
}

void SearchNetSensor::on_a1Spinbox_valueChanged(int arg1)
{
    if (sessionConfig.alarmtype == aiHH || sessionConfig.alarmtype == aiHL) {
        if (arg1 < (sensorConfig.a2 -2))
            sensorConfig.a1 = arg1;
        else {
            sensorConfig.a1 = sensorConfig.a2 - 5;
            ui->a1Spinbox->setValue(sensorConfig.a1);
        }


    } else if (sessionConfig.alarmtype == aiLL) {
        if (arg1 > (sensorConfig.a2 + 2))
            sensorConfig.a1 = arg1;
        else {
            sensorConfig.a1 = sensorConfig.a2 + 5;
            ui->a1Spinbox->setValue(sensorConfig.a1);
        }

    }
}

void SearchNetSensor::on_a2Spinbox_valueChanged(int arg1)
{
    if (sessionConfig.alarmtype == aiHH || sessionConfig.alarmtype == aiHL) {
        if (arg1 > (sensorConfig.a1 + 2))
            sensorConfig.a2 = arg1;
        else {
            sensorConfig.a2 = sensorConfig.a1 + 5;
            ui->a2Spinbox->setValue(sensorConfig.a2);
        }


    } else if (sessionConfig.alarmtype == aiLL) {
        if (arg1 < (sensorConfig.a1 -2))
            sensorConfig.a2 = arg1;
        else {
            sensorConfig.a2 = sensorConfig.a1 - 5;
            ui->a2Spinbox->setValue(sensorConfig.a2);
        }

    }
}

void SearchNetSensor::on_refSpinbox_valueChanged(int arg1)
{
    if (arg1 == sensorConfig.pos){
        ui->refSpinbox->setValue(sensorConfig.diffmode);
    } else
        sensorConfig.diffmode = arg1;

}

void SearchNetSensor::on_workerTimerTimeout()
{
    workerTimer.stop();
    worker->terminate();

    scanActive = false;
    if (gifSpinner){
        gifSpinner->stop();
        delete gifSpinner;
        gifSpinner = 0;
    }

    ui->loaderImage->setPixmap(QPixmap(ICON_CONFIRM_KO));
    ui->scanNetworkButton->setEnabled(true);
}
