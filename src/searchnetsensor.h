#ifndef SEARCHNETSENSOR_H
#define SEARCHNETSENSOR_H

#include <QWidget>
#include <QMovie>
#include <QTimer>
#include "tnetportal.h"
#include "virtualkeypaddialog.h"

namespace Ui {
class searchnetsensor;
}

class SearchNetSensor : public QWidget
{
    Q_OBJECT

public:
    explicit SearchNetSensor(QWidget *parent = 0,
                             const Session_t& sessionCfg = Session_t(),
                             const Sensor_t& sensorCfg = Sensor_t(),
                             bool createNewMode = false,
                             VirtualKeypadDialog * virtualkeypad = 0);
    ~SearchNetSensor();

    const Sensor_t getNewConfig(void);

private slots:
    void on_okButton_clicked();
    void on_cancelButton_clicked();
    void on_scanNetworkButton_clicked();
    void on_sensorNameEdit_cursorPositionChanged(int arg1, int arg2);
    void on_sensorSerialEdit_cursorPositionChanged(int arg1, int arg2);
    void on_posSpinbox_valueChanged(int arg1);
    void on_a1Spinbox_valueChanged(int arg1);
    void on_a2Spinbox_valueChanged(int arg1);
    void on_refSpinbox_valueChanged(int arg1);

    void on_workerTimerTimeout();

public slots:
    void onSensorFound(bool result, const QString serial);

signals:
    void finished(bool ok);


private:
    Ui::searchnetsensor *ui;
    QMovie * gifSpinner;
    QThread * worker;
    TnetSensorCtl * sensorCtl;
    Sensor_t sensorConfig;
    Session_t sessionConfig;
    VirtualKeypadDialog * vkpad;
    QTimer workerTimer;
    bool createMode;
    bool scanActive;
};

#endif // SEARCHNETSENSOR_H
