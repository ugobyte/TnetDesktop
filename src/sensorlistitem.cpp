#include "sensorlistitem.h"
#include "ui_sensorlistitem.h"
#include "common.h"


SensorListItem::SensorListItem(QWidget *parent, const Sensor_t &sensor, bool canRemove) :
    QWidget(parent),
    ui(new Ui::SensorListItem),
    model(sensor)
{
    ui->setupUi(this);
    ui->removeSensorButton->setEnabled(canRemove);

    ui->nameLabel->setText(model.alias);
    ui->positionLabel->setText(QString("%1").arg(model.pos));
    ui->a1Label->setText(QString("%1%2C").arg(model.a1).arg(G_UNIT_DEGREES));
    ui->a2Label->setText(QString("%1%2C").arg(model.a2).arg(G_UNIT_DEGREES));
    ui->refLabel->setText(QString("%1").arg(model.diffmode));

    TG_DEBUG("Constructing sensor list item");
}

SensorListItem::~SensorListItem()
{
    delete ui;
    TG_DEBUG("Destructing sensor list item");
}

void SensorListItem::updateModel(const Sensor_t &sensor)
{
    model = sensor;
    ui->nameLabel->setText(model.alias);
    ui->positionLabel->setText(QString("%1").arg(model.pos));
    ui->a1Label->setText(QString("%1%2C").arg(model.a1).arg(G_UNIT_DEGREES));
    ui->a2Label->setText(QString("%1%2C").arg(model.a2).arg(G_UNIT_DEGREES));
    ui->refLabel->setText(QString("%1").arg(model.diffmode));

    TG_DEBUG("Model updated, pos= " << model.pos << " name=" << model.alias << " A1=" << model.a1 << " A2=" << model.a2 << " ref=" << model.diffmode);
}

void SensorListItem::on_removeSensorButton_clicked()
{
    emit sensorRemoved(model.pos);
}
