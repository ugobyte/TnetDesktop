#ifndef SENSORLISTITEM_H
#define SENSORLISTITEM_H

#include <QWidget>
#include "tnetportal.h"

namespace Ui {
class SensorListItem;
}

class SensorListItem : public QWidget
{
    Q_OBJECT

public:
    explicit SensorListItem(QWidget *parent = 0, const Sensor_t& sensor = Sensor_t(), bool canRemove=false);
    ~SensorListItem();

    void updateModel(const Sensor_t& sensor);

private slots:
    void on_removeSensorButton_clicked();

signals:
    void sensorRemoved(const int pos);

private:
    Ui::SensorListItem *ui;
    Sensor_t model;
};

#endif // SENSORLISTITEM_H
