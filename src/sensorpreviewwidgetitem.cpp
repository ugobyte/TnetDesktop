#include "sensorpreviewwidgetitem.h"
#include "ui_sensorpreviewwidgetitem.h"
#include "common.h"

SensorPreviewWidgetItem::SensorPreviewWidgetItem(QWidget *parent, const Sensor_t &sensor, bool viewDegreesInCelcius) :
    QWidget(parent),
    ui(new Ui::SensorPreviewWidgetItem)
{
    ui->setupUi(this);

    ui->positionLabel->setText(QString("%1").arg(sensor.pos));
    ui->nameLabel->setText(sensor.alias);
    ui->serialLabel->setText(sensor.serial);

    if (viewDegreesInCelcius) {
        ui->a1Label->setText(QString("%1%2C").arg(sensor.a1).arg(degreeChar));
        ui->a2Label->setText(QString("%1%2C").arg(sensor.a2).arg(degreeChar));
    } else {
        ui->a1Label->setText(QString("%1%2F").arg(CelciusToFahrheit(sensor.a1)).arg(degreeChar));
        ui->a2Label->setText(QString("%1%2F").arg(CelciusToFahrheit(sensor.a2)).arg(degreeChar));
    }

    switch (sensor.alarmStatus) {
        case daNone:
            ui->alarmImage->clear();
            break;

        case daPastA1:
            ui->alarmImage->setPixmap(QPixmap(ICON_SENSOR_A1));
            break;

        case daPastA2:
            ui->alarmImage->setPixmap(QPixmap(ICON_SENSOR_A2));
            break;

        case daCurrentA1:
        case daCurrentA2:
            break;
        default:
            break;
    }

    if (sensor.diffmode != 0 && sensor.diffmode != sensor.pos){
        ui->refLabel->setText(QString("%1").arg(sensor.diffmode));
        ui->refImage->setPixmap(QPixmap(ICON_SENSOR_LINK));
    } else {
        ui->refLabel->clear();
        ui->refImage->clear();
    }

    TG_DEBUG("Constructing sensor preview widget item");
}

SensorPreviewWidgetItem::~SensorPreviewWidgetItem()
{
    delete ui;

    TG_DEBUG("Destructing sensor preview widget item");
}
