#ifndef SENSORPREVIEWWIDGETITEM_H
#define SENSORPREVIEWWIDGETITEM_H

#include <QWidget>
#include "tnetportal.h"

namespace Ui {
class SensorPreviewWidgetItem;
}

class SensorPreviewWidgetItem : public QWidget
{
    Q_OBJECT

public:
    explicit SensorPreviewWidgetItem(QWidget *parent = 0, const Sensor_t &sensor = Sensor_t(), bool viewDegreesInCelcius = true);
    ~SensorPreviewWidgetItem();

private:
    Ui::SensorPreviewWidgetItem *ui;
};

#endif // SENSORPREVIEWWIDGETITEM_H
