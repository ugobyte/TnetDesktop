#include "sessionfolderwidget.h"
#include "ui_sessionfolderwidget.h"
#include "tnetportal.h"

SessionFolderWidget::SessionFolderWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SessionFolderWidget)
{
    ui->setupUi(this);

    ui->sessionNameLabel->clear();
    ui->sessionNumberLabel->clear();
    ui->dateRangeLabel->clear();
    ui->totalSensorsLabel->clear();

    TG_DEBUG("Constructing session folder widget");
}

SessionFolderWidget::~SessionFolderWidget()
{
    delete ui;

    TG_DEBUG("Destructing session folder widget");
}

void SessionFolderWidget::updateData(const QString &name, const QString &number, const QString &dateRange, const quint8 totalSensors)
{
    ui->sessionNameLabel->setText(name);
    ui->sessionNumberLabel->setText(number);
    //ui->dateRangeLabel->setText(dateRange);
    //ui->totalSensorsLabel->setText(QString("Total sensors: %1").arg(totalSensors));
}
