#ifndef SESSIONFOLDERWIDGET_H
#define SESSIONFOLDERWIDGET_H

#include <QWidget>

namespace Ui {
class SessionFolderWidget;
}

class SessionFolderWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SessionFolderWidget(QWidget *parent = 0);
    ~SessionFolderWidget();

    void updateData(const QString& name, const QString& number, const QString& dateRange, const quint8 totalSensors);
private:
    Ui::SessionFolderWidget *ui;
};

#endif // SESSIONFOLDERWIDGET_H
