#include "tabledatawidgetitem.h"
#include "ui_tabledatawidgetitem.h"
#include "common.h"

TableDataWidgetItem::TableDataWidgetItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TableDataWidgetItem)
{
    ui->setupUi(this);
    TG_DEBUG("Constructing table data widget item");
}


TableDataWidgetItem::~TableDataWidgetItem()
{
    delete ui;

    TG_DEBUG("Destructing table data widget item");
}

void TableDataWidgetItem::updateData(const TableItemData_t &tableData, bool degreeCelcius)
{
    QDateTime dt;
    // fromSecsSinceEpoch only introduced in qt 5.8 so can't use in qt 4.8
    ui->dateLabel->setText(dt.fromMSecsSinceEpoch(tableData.dtutc*1000).toString("MMM dd/MM/yyyy hh:mm:ss"));

    if (tableData.temp == 200.0)
        ui->tempLabel->setText("FAULT");
    else if (degreeCelcius)
        ui->tempLabel->setText(QString::number(tableData.temp, 'f', 1) + QString("%1C").arg(degreeChar));
    else
        ui->tempLabel->setText(QString::number(CelciusToFahrheit(tableData.temp), 'f', 1) + QString("%1F").arg(degreeChar));

    if (degreeCelcius) {
        ui->a1Label->setText(QString("%1%2C").arg(tableData.a1).arg(degreeChar));
        ui->a2Label->setText(QString("%1%2C").arg(tableData.a2).arg(degreeChar));
    } else {
        ui->a1Label->setText(QString("%1%2F").arg(CelciusToFahrheit(tableData.a1)).arg(degreeChar));
        ui->a2Label->setText(QString("%1%2F").arg(CelciusToFahrheit(tableData.a2)).arg(degreeChar));
    }

    switch (tableData.alarmState) {
        case daNone:
            ui->alarmStateLabel->clear();
            break;
        case daPastA1:
        case daCurrentA1:
            ui->alarmStateLabel->setPixmap(QPixmap(ICON_SENSOR_A1));
            break;
        case daPastA2:
        case daCurrentA2:
            ui->alarmStateLabel->setPixmap(QPixmap(ICON_SENSOR_A2));
            break;
        default:
            break;
    }
}

void TableDataWidgetItem::clearData()
{
    ui->dateLabel->clear();
    ui->tempLabel->clear();
    ui->a1Label->clear();
    ui->a2Label->clear();
    ui->alarmStateLabel->clear();
}


