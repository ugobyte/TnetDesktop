#ifndef TABLEDATAWIDGETITEM_H
#define TABLEDATAWIDGETITEM_H

#include <QWidget>
#include "tnetportal.h"

namespace Ui {
class TableDataWidgetItem;
}

typedef struct {
    qint64 dtutc;
    float temp;
    quint16 a1;
    quint16 a2;
    TgAlarmStatus alarmState;
} TableItemData_t;

class TableDataWidgetItem : public QWidget
{
    Q_OBJECT

public:
    explicit TableDataWidgetItem(QWidget *parent = 0);
    ~TableDataWidgetItem();
    void updateData(const TableItemData_t& tableData, bool degreeCelcius);
    void clearData();
private:
    Ui::TableDataWidgetItem *ui;
};

#endif // TABLEDATAWIDGETITEM_H
