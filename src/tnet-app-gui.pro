#-------------------------------------------------
#
# Project created by QtCreator 2017-12-09T13:40:03
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tnet-app-gui
TEMPLATE = app
CONFIG += shared debug_and_release build_release
VERSION = 1.0.0

QMAKE_TARGET_COMPANY = "www.tempnetz.com"
QMAKE_TARGET_COPYRIGHT = "Copyright (C) by TempnetZ"


FORMS += \
    adddevicedialog.ui \
    confirmdialog.ui \
    explorer.ui \
    messagedialog.ui \
    passworddialog.ui \
    # searchnetsensor.ui \
    sensorlistitem.ui \
    tabledatawidgetitem.ui \
    uitabledatapage.ui \
    uihomepage.ui \
    dynamicsensorwidgetitem.ui \
    uirealtimechartpage.ui \
    uilogchartpage.ui \
    uimenupage.ui \
    uiconnectivitypage.ui \
    uiwifipage.ui \
    wifidialog.ui \
    wifinetworklistitem.ui \
    uisessionmenupage.ui \
    uisensorlistpage.ui \
    uisensorconfigpage.ui \
    uiusernotificationpage.ui \
    uiuseraddeditpage.ui \
    userlistitem.ui \
    uipasswordeditpage.ui \
    uicredentialspage.ui \
    uipoweroffpage.ui \
    uiaboutpage.ui \
    deviceform.ui \
    virtualkeypaddialog.ui \
    gatewaywidgetitem.ui \
    uisessionfolderviewpage.ui \
    uiaudiovisualpage.ui \
    sessionfolderwidget.ui \
    uisessionpreviewpage.ui \
    sensorpreviewwidgetitem.ui \
    emailimagesdialog.ui \
    emailtablewidgetitem.ui \
    uigeneralsettingsmenu.ui \
    uiimagespage.ui

HEADERS += \
    common.h \
    adddevicedialog.h \
    confirmdialog.h \
    explorer.h \
    messagedialog.h \
    passworddialog.h \
    # searchnetsensor.h \
    sensorlistitem.h \
    tabledatawidgetitem.h \
    uitabledatapage.h \
    uihomepage.h \
    dynamicsensorwidgetitem.h \
    uipagewidget.h \
    uirealtimechartpage.h \
    uilogchartpage.h \
    uimenupage.h \
    uiconnectivitypage.h \
    uiwifipage.h \
    wifidialog.h \
    wifinetworklistitem.h \
    uisessionmenupage.h \
    uisensorlistpage.h \
    uisensorconfigpage.h \
    uiusernotificationpage.h \
    uiuseraddeditpage.h \
    userlistitem.h \
    uipasswordeditpage.h \
    uicredentialspage.h \
    uipoweroffpage.h \
    uiaboutpage.h \
    deviceform.h \
    virtualkeypaddialog.h \
    gatewaywidgetitem.h \
    uisessionfolderviewpage.h \
    uiaudiovisualpage.h \
    sessionfolderwidget.h \
    uisessionpreviewpage.h \
    sensorpreviewwidgetitem.h \
    emailimagesdialog.h \
    emailtablewidgetitem.h \
    uigeneralsettingsmenu.h \
    uiimagespage.h \
    tnetsingleapplication.h

SOURCES += \
    adddevicedialog.cpp \
    confirmdialog.cpp \
    explorer.cpp \
    main.cpp \
    messagedialog.cpp \
    passworddialog.cpp \
    # searchnetsensor.cpp \
    sensorlistitem.cpp \
    tabledatawidgetitem.cpp \
    uitabledatapage.cpp \
    uihomepage.cpp \
    dynamicsensorwidgetitem.cpp \
    uipagewidget.cpp \
    uirealtimechartpage.cpp \
    uilogchartpage.cpp \
    uimenupage.cpp \
    uiconnectivitypage.cpp \
    uiwifipage.cpp \
    wifidialog.cpp \
    wifinetworklistitem.cpp \
    uisessionmenupage.cpp \
    uisensorlistpage.cpp \
    uisensorconfigpage.cpp \
    uiusernotificationpage.cpp \
    uiuseraddeditpage.cpp \
    userlistitem.cpp \
    uipasswordeditpage.cpp \
    uicredentialspage.cpp \
    uipoweroffpage.cpp \
    uiaboutpage.cpp \
    deviceform.cpp \
    virtualkeypaddialog.cpp \
    gatewaywidgetitem.cpp \
    uisessionfolderviewpage.cpp \
    uiaudiovisualpage.cpp \
    sessionfolderwidget.cpp \
    uisessionpreviewpage.cpp \
    sensorpreviewwidgetitem.cpp \
    emailimagesdialog.cpp \
    emailtablewidgetitem.cpp \
    uigeneralsettingsmenu.cpp \
    uiimagespage.cpp \
    tnetsingleapplication.cpp


# build for desktop x86_64
contains(QT_ARCH, x86_64){
DEFINES += "TNET_DESKTOP=\"1\""
}



# build for olimex lcd touchscreen
contains(QT_ARCH, arm){
DEFINES += "TNET_OLIMEX=\"1\""
message("Compiling for arm system")

# not these
SOURCES -= explorer.cpp adddevicedialog.cpp
HEADERS -= explorer.h adddevicedialog.h
FORMS -= explorer.ui adddevicedialog.ui
}

#RESOURCES += images.qrc

unix {
    DEFINES += "TNET_LINUX=\"1\""
    INCLUDEPATH += /usr/include
    LIBS += -L/usr/lib -ltnet-lib-core

    target.path = ../sbin
    INSTALLS += target

CONFIG(debug, debug|release) {
    DESTDIR = build/debug_$$QT_ARCH
}
CONFIG(release, debug|release) {
    DESTDIR = build/release_$$QT_ARCH
}

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.ui

}

win32 {
    DEFINES += "TNET_DESKTOP=1"
    DEFINES += "TNET_WINDOWS=1"
    INCLUDEPATH += ../../../Includes
    LIBS += -L../../../Libs -ltnetportal1
}



RESOURCES += \
    icons.qrc



