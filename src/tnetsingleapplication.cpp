#include "tnetsingleapplication.h"


TnetSingleApplication::TnetSingleApplication(int &argc, char **argv):
    QApplication(argc, argv)
{
    _singular = new QSharedMemory("TempnetzApplicationRunning", this);
}

TnetSingleApplication::~TnetSingleApplication()
{
    if(_singular->isAttached())
        _singular->detach();
}

bool TnetSingleApplication::lock()
{
    if(_singular->attach(QSharedMemory::ReadOnly)){
        _singular->detach();
        return false;
    }

    if(_singular->create(1))
        return true;

    return false;
}
