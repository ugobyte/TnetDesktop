#ifndef TNETSINGLEAPPLICATION_H
#define TNETSINGLEAPPLICATION_H

#include <QApplication>
#include <QSharedMemory>

/**
 * @ref https://forum.qt.io/topic/59223/qt-single-instance-application/6
 */

class TnetSingleApplication : public QApplication
{
    Q_OBJECT

public:
    TnetSingleApplication(int &argc, char **argv);
    ~TnetSingleApplication();

    bool lock();

private:
    QSharedMemory *_singular;
};

#endif // TNETSINGLEAPPLICATION_H
