#include "uiaboutpage.h"
#include "ui_uiaboutpage.h"

UiAboutPage::UiAboutPage(QWidget *parent, WidgetPage pageid, TgGateway *gateway) :
    UiPageWidget(parent, pageid, gateway),
    ui(new Ui::UiAboutPage)
{
    ui->setupUi(this);

    const System_t& sys = gwmodel->getSystem();
    const GatewayId_t& id = gwmodel->getId();
    const Network_t& net = gwmodel->getNetwork();

    ui->cpuLabel->setText(QString("%1%").arg(sys.cpu));
    ui->memoryLabel->setText(QString("%1%").arg(sys.mem));
    ui->uptimeLabel->setText(QString("%1 seconds").arg(sys.uptime));
    ui->diskLabel->setText(QString("%1%").arg(sys.disk));

    ui->nameLabel->setText(id.name);
    ui->serialNumberLabel->setText(id.serial);

    ui->networkLabel->setText(net.netip);
    ui->vpnLabel->setText(net.hamIp);

    ui->lcdVersionLabel->setText(id.lcdversion);
    ui->serverVersionLabel->setText(id.serverversion);

    TG_DEBUG("Constructing about page");
}

UiAboutPage::~UiAboutPage()
{
    delete ui;

    TG_DEBUG("Destructing about page");
}

void UiAboutPage::on_backButton_clicked()
{
    gotoPage(spMenuPage);
}
