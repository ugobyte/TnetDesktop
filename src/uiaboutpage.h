#ifndef UIABOUTPAGE_H
#define UIABOUTPAGE_H

#include <QWidget>
#include "uipagewidget.h"

namespace Ui {
class UiAboutPage;
}

class UiAboutPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiAboutPage(QWidget *parent = 0, WidgetPage pageid = spNotSet, TgGateway * gateway = 0);
    ~UiAboutPage();

private slots:
    void on_backButton_clicked();

private:
    Ui::UiAboutPage *ui;
};

#endif // UIABOUTPAGE_H
