#include "uiaudiovisualpage.h"
#include "ui_uiaudiovisualpage.h"

UiAudioVisualPage::UiAudioVisualPage(QWidget *parent, WidgetPage pageid, TgGateway *gateway) :
    UiPageWidget(parent, pageid, gateway),
    ui(new Ui::UiAudioVisualPage),
    msgDlg(0),
    audioVisual(0)
{
    ui->setupUi(this);

    audioVisual = new InputOutput_t;
    const InputOutput_t& io = gwmodel->getInout();
    const MuteStatus_t& muteStatus = gwmodel->getMuteStatus();

    audioVisual->audioalert = io.audioalert;
    audioVisual->mutecount = io.mutecount;
    audioVisual->mutetime = io.mutetime;
    audioVisual->visualalert = io.visualalert;

    TG_DEBUG("Audio alert = " << io.audioalert);
    TG_DEBUG("Visual alert = " << io.visualalert);
    TG_DEBUG("Mute timeout = " << io.mutetime);
    TG_DEBUG("Mute count = " << io.mutecount);
    TG_DEBUG("Mute on = " << muteStatus.muted);
    TG_DEBUG("Mute actions = " << muteStatus.muteActionsTaken);

    ui->visualAlertButton->setChecked(io.visualalert);
    ui->audioAlertButton->setChecked(io.audioalert);
    ui->muteCountSpinbox->setValue(io.mutecount);
    ui->muteTimeoutSpinbox->setValue(io.mutetime);

    TG_DEBUG("Constructing Audio visual page");
}

UiAudioVisualPage::~UiAudioVisualPage()
{
    delete ui;

    if (audioVisual)
        delete audioVisual;

    TG_DEBUG("Destructing Audio visual page");
}

void UiAudioVisualPage::on_visualAlertButton_toggled(bool checked)
{
    audioVisual->visualalert = checked;
}

void UiAudioVisualPage::on_audioAlertButton_toggled(bool checked)
{
    audioVisual->audioalert = checked;
}

void UiAudioVisualPage::on_uploadButton_clicked()
{
    audioVisual->mutetime = ui->muteTimeoutSpinbox->value();
    msgDlg = new MessageDialog(this,"Uploading ...",true);
    msgDlg->show();

    QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_avRequestResult(bool,QString)));
    gwmodel->requestAVConfig(audioVisual);
}

void UiAudioVisualPage::on_avRequestResult(bool result, QString repr)
{
    msgDlg->close();
    msgDlg = new MessageDialog(this,"",false);
    if (result)
        msgDlg->setResult("Success", true);

    else {
        TG_WARNING("AV config request failed, reason = " << repr.toLatin1().data());
        msgDlg->setResult("Failed", false);
    }

    msgDlg->show();
}

void UiAudioVisualPage::on_backButton_clicked()
{
    gotoPage(spGeneralSettingsPage);
}
