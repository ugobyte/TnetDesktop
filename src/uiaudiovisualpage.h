#ifndef UIAUDIOVISUALPAGE_H
#define UIAUDIOVISUALPAGE_H

#include <QWidget>
#include "uipagewidget.h"
#include "messagedialog.h"

namespace Ui {
class UiAudioVisualPage;
}

class UiAudioVisualPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiAudioVisualPage(QWidget *parent = 0, WidgetPage pageid = spNotSet, TgGateway * gateway = 0);
    ~UiAudioVisualPage();

private slots:
    void on_visualAlertButton_toggled(bool checked);
    void on_audioAlertButton_toggled(bool checked);
    void on_uploadButton_clicked();
    void on_avRequestResult(bool result, QString repr);
    void on_backButton_clicked();

private:
    Ui::UiAudioVisualPage *ui;
    MessageDialog * msgDlg;
    InputOutput_t * audioVisual;
};

#endif // UIAUDIOVISUALPAGE_H
