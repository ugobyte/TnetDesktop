#include "uiconnectivitypage.h"
#include "ui_uiconnectivitypage.h"

UiConnectivityPage::UiConnectivityPage(QWidget *parent, WidgetPage pageid) :
    UiPageWidget(parent,pageid,0),
    ui(new Ui::UiConnectivityPage)
{
    ui->setupUi(this);
    ui->ethernetButton->setEnabled(false);
    ui->cellButton->setEnabled(false);
    TG_DEBUG("Constructing connectivity page");
}

UiConnectivityPage::~UiConnectivityPage()
{
    delete ui;
    TG_DEBUG("Destructing connectivity page");
}

void UiConnectivityPage::on_backButton_clicked()
{
    gotoPage(spMenuPage);
}



void UiConnectivityPage::on_ethernetButton_clicked()
{
    gotoPage(spEthPage);
}

void UiConnectivityPage::on_wifiButton_clicked()
{
    gotoPage(spWifiPage);
}

void UiConnectivityPage::on_cellButton_clicked()
{
    gotoPage(spCellPage);
}
