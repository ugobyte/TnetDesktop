#ifndef UICONNECTIVITYPAGE_H
#define UICONNECTIVITYPAGE_H

#include <QWidget>
#include "uipagewidget.h"

namespace Ui {
class UiConnectivityPage;
}

class UiConnectivityPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiConnectivityPage(QWidget *parent = 0, WidgetPage pageid = spNotSet);
    ~UiConnectivityPage();

private slots:
    void on_backButton_clicked();


    void on_ethernetButton_clicked();

    void on_wifiButton_clicked();

    void on_cellButton_clicked();

private:
    Ui::UiConnectivityPage *ui;
};

#endif // UICONNECTIVITYPAGE_H
