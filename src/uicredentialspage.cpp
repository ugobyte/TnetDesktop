#include "uicredentialspage.h"
#include "ui_uicredentialspage.h"

UiCredentialsPage::UiCredentialsPage(QWidget *parent, WidgetPage pageid, TgGateway * gateway, VirtualKeypadDialog * keypad) :
    UiPageWidget(parent, pageid, gateway),
    ui(new Ui::UiCredentialsPage),
    vKeypad(keypad)
{
    ui->setupUi(this);

    const GatewayId_t& id = gwmodel->getId();

    ui->serialNumberLabel->setText(id.serial);
    ui->nameLineedit->setText(id.name);
    ui->descriptionLineedit->setText(id.description);

    ui->nameLineedit->setFocus();
#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif

    TG_DEBUG("Constructing credentials page");
}

UiCredentialsPage::~UiCredentialsPage()
{
    delete ui;

    TG_DEBUG("Destructing credentials page");
}

void UiCredentialsPage::on_backButton_clicked()
{
    gotoPage(spGeneralSettingsPage);
}

void UiCredentialsPage::on_saveChangesButton_clicked()
{
    const GatewayId_t& id = gwmodel->getId();
    credentials = id;

    credentials.name = ui->nameLineedit->text();
    credentials.description = ui->descriptionLineedit->text();

    msgDlg = new MessageDialog(this,"Updating id ...",true);
    msgDlg->show();

    QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_gatewayCredentialsUpdated(bool,QString)));
    gwmodel->requestUpdateCredentials(&credentials);
}

void UiCredentialsPage::on_gatewayCredentialsUpdated(bool result, QString repr)
{
    Q_UNUSED(repr);

    msgDlg->close();

    if (result){
        msgDlg = new MessageDialog(this,"",false);
        msgDlg->setResult("Success",true);
        msgDlg->show();

    } else {
        msgDlg = new MessageDialog(this,"",false);
        msgDlg->setResult("Failed",false);
        msgDlg->show();
    }
}

void UiCredentialsPage::on_nameLineedit_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1);
    Q_UNUSED(arg2);
#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif
}

void UiCredentialsPage::on_descriptionLineedit_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1);
    Q_UNUSED(arg2);
#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif
}
