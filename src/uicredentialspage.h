#ifndef UICREDENTIALSPAGE_H
#define UICREDENTIALSPAGE_H

#include <QWidget>
#include "uipagewidget.h"
#include "virtualkeypaddialog.h"
#include "messagedialog.h"

namespace Ui {
class UiCredentialsPage;
}

class UiCredentialsPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiCredentialsPage(QWidget *parent = 0, WidgetPage pageid = spNotSet, TgGateway * gateway = 0, VirtualKeypadDialog * keypad = 0);
    ~UiCredentialsPage();

private slots:
    void on_backButton_clicked();
    void on_saveChangesButton_clicked();

    void on_gatewayCredentialsUpdated(bool result, QString repr);

    void on_nameLineedit_cursorPositionChanged(int arg1, int arg2);

    void on_descriptionLineedit_cursorPositionChanged(int arg1, int arg2);

private:
    Ui::UiCredentialsPage *ui;
    GatewayId_t credentials;
    MessageDialog * msgDlg;
    VirtualKeypadDialog * vKeypad;
};

#endif // UICREDENTIALSPAGE_H
