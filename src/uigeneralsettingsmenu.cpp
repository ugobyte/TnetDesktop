#include "uigeneralsettingsmenu.h"
#include "ui_uigeneralsettingsmenu.h"

UiGeneralSettingsMenu::UiGeneralSettingsMenu(QWidget *parent, WidgetPage pageid, TgGateway *gateway) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiGeneralSettingsMenu)
{
    ui->setupUi(this);

#if defined(TNET_DESKTOP)
    ui->imagesButton->setEnabled(false);
    ui->imagesButton->setVisible(false);
#endif

    TG_DEBUG("Constructing general settings menu page");
}

UiGeneralSettingsMenu::~UiGeneralSettingsMenu()
{
    delete ui;

    TG_DEBUG("Destructing general settings menu page");
}

void UiGeneralSettingsMenu::on_credentialsButton_clicked()
{
    gotoPage(spCredentialPage);
}

void UiGeneralSettingsMenu::on_passwordButton_clicked()
{
    emit gotoPage(spPasswordPage);
}

void UiGeneralSettingsMenu::on_notificationsButton_clicked()
{
    emit gotoPage(spNotificationPage);
}

void UiGeneralSettingsMenu::on_audioVisualAlarmsButton_clicked()
{
    emit gotoPage(spAudioVisualPage);
}

void UiGeneralSettingsMenu::on_imagesButton_clicked()
{
    emit gotoPage(spImagesPage);
}

void UiGeneralSettingsMenu::on_backButton_clicked()
{
    emit gotoPage(spMenuPage);
}
