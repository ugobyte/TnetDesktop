#ifndef UIGENERALSETTINGSMENU_H
#define UIGENERALSETTINGSMENU_H

#include <QWidget>
#include "uipagewidget.h"

namespace Ui {
class UiGeneralSettingsMenu;
}

class UiGeneralSettingsMenu : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiGeneralSettingsMenu(QWidget *parent = 0, WidgetPage pageid = spNotSet, TgGateway * gateway = 0);
    ~UiGeneralSettingsMenu();

private slots:
    void on_credentialsButton_clicked();
    void on_passwordButton_clicked();
    void on_notificationsButton_clicked();
    void on_audioVisualAlarmsButton_clicked();
    void on_imagesButton_clicked();

    void on_backButton_clicked();

private:
    Ui::UiGeneralSettingsMenu *ui;
};

#endif // UIGENERALSETTINGSMENU_H
