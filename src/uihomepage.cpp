#include "uihomepage.h"
#include "ui_uihomepage.h"


UiHomePage::UiHomePage(QWidget *parent, WidgetPage pageid, TgGateway * gateway, VirtualKeypadDialog *vKpad, FilterContext *ctx, bool celcius) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiHomePage),
    passDlg(0),
    vKeypad(vKpad),
    filterCtx(ctx),
    degreesCelcius(celcius)
{
    ui->setupUi(this);

    // signal/slot to receive live event streams from gateway
    QObject::connect(gwmodel, SIGNAL(streamReceived(TgEvStreamClass)), this, SLOT(on_gatewayStreamReceived(TgEvStreamClass)));

    QObject::connect(&rotateTimer, SIGNAL(timeout()), this, SLOT(on_rotateTimerTimeout()));
    rotateTimer.setInterval(3000);

    createSensorWidgetItems();
    updateTable();

    // set the state of playButton based on the alarm checkbox states
    ui->a1Checkbox->setChecked(filterCtx->a1Checked);
    ui->a2Checkbox->setChecked(filterCtx->a2Checked);
    if (filterCtx->a1Checked || filterCtx->a2Checked){
        playButtonPaused = true;
        rotateTimer.stop();
    } else {
        playButtonPaused = false;
        rotateTimer.start();
    }
    ui->playButton->setChecked(playButtonPaused);

#if defined(TNET_OLIMEX)
    const InputOutput_t& inout = gwmodel->getInout();
    const MuteStatus_t& muteStatus = gwmodel->getMuteStatus();

    // ignore if audio alerts not enabled
    if (!inout.audioalert || muteStatus.muteActionsTaken == inout.mutecount)
        ui->muteButton->setEnabled(false);
# elif defined(TNET_DESKTOP)
    ui->muteButton->setEnabled(false);
    ui->muteButton->setVisible(false);
#endif

    TG_DEBUG("Constructing home page");
}

UiHomePage::~UiHomePage()
{
    rotateTimer.stop();
    items.clear();

    delete ui;

    TG_DEBUG("Destructing home page");
}

void UiHomePage::syncAlarmStateTimeout(bool mark)
{
    qint16 thisRowIndex;
    qint16 sensorIndex;
    quint16 rowCount;
    const QVector<Sensor_t>& sensors = gwmodel->getSensors();
    const Session_t& session = gwmodel->getSession();
    int filterListSize = filterCtx->sensorList.count();

#if defined(TNET_OLIMEX)
    rowCount = (session.totalSensors > 5)? 6:session.totalSensors;
#elif defined(TNET_DESKTOP)
    rowCount = session.totalSensors;
#endif

    for (int i=0; i<rowCount; i++) {

        thisRowIndex = sensorListTopItem + i;

        // wrap
        if (thisRowIndex >= session.totalSensors)
            thisRowIndex = thisRowIndex - session.totalSensors;

        // break if done
        if (thisRowIndex >= filterListSize)
            break;

        sensorIndex = filterCtx->sensorList.at(thisRowIndex).pos - 1;
        const Sensor_t& sensor = sensors.at(sensorIndex);
        items[i]->syncAlarmStateTimeout(mark, sensor.alarmStatus);
    }
}

void UiHomePage::on_rotateTimerTimeout()
{
    rotateListDown();
    updateTable();
}

void UiHomePage::createSensorWidgetItems()
{
    quint16 rowCount;
    const Session_t& session = gwmodel->getSession();

#if defined(TNET_OLIMEX)
    rowCount = (session.totalSensors > 5)? 6:session.totalSensors;
#elif defined(TNET_DESKTOP)
    rowCount = session.totalSensors;
#endif

    ui->tableWidget->setColumnCount(1);
    ui->tableWidget->setRowCount(rowCount);

    for (int i=0; i<rowCount; i++) {
        ui->tableWidget->setRowHeight(i,60);
        DynamicSensorWidgetItem * w = new DynamicSensorWidgetItem;
        ui->tableWidget->setCellWidget(i,0,w);
        items.append(w);
    }

    sensorListTopItem = 0;
}

void UiHomePage::updateTable()
{
    quint16 rowCount;
    qint16 thisRowIndex;
    qint16 sensorIndex;
    int filterListSize = filterCtx->sensorList.count();

    const QVector<Sensor_t>& sensors = gwmodel->getSensors();
    const Session_t& session = gwmodel->getSession();

#if defined(TNET_OLIMEX)
    rowCount = (session.totalSensors > 5)? 6:session.totalSensors;
#elif defined(TNET_DESKTOP)
    rowCount = session.totalSensors;
#endif

    // clear item contents
    for (int i=0; i<rowCount; i++)
        items[i]->clear();

    // update item with new contents
    for (int i=0; i<rowCount; i++) {

        thisRowIndex = sensorListTopItem + i;

        // wrap
        if (thisRowIndex >= session.totalSensors)
            thisRowIndex = thisRowIndex - session.totalSensors;

        // break if done
        if (thisRowIndex >= filterListSize)
            break;

        sensorIndex = filterCtx->sensorList.at(thisRowIndex).pos - 1;
        const Sensor_t& sensor = sensors.at(sensorIndex);

        items[i]->updateWidget(sensor,degreesCelcius);
    }
}


void UiHomePage::rotateListUp()
{
    sensorListTopItem++;

    if (sensorListTopItem >= filterCtx->sensorList.count())
        sensorListTopItem = 0;
}

void UiHomePage::rotateListDown()
{
    sensorListTopItem--;

    if (sensorListTopItem < 0)
        sensorListTopItem = filterCtx->sensorList.count() -1;
}

void UiHomePage::updateFilterList()
{
    filterCtx->sensorList.clear();

    const Session_t& session = gwmodel->getSession();
    const QVector<Sensor_t>& sensors = gwmodel->getSensors();

    for (int i=0; i<session.totalSensors; i++){
        const Sensor_t& sensor = sensors.at(i);

        if (ui->a1Checkbox->isChecked() && ui->a2Checkbox->isChecked()){
            if (sensor.alarmStatus != daNone){
                struct SensorContext sctx;
                sctx.name = sensors.at(i).alias;
                sctx.pos = sensors.at(i).pos;
                quint8 refPos = sensors.at(i).diffmode;
                if (refPos != 0 && refPos != sctx.pos)
                    sctx.refName = sensors.at(refPos-1).alias;
                else
                    sctx.refName = "";
                sctx.refPos = refPos;
                filterCtx->sensorList.append(sctx);
            }
        }

        else if (ui->a2Checkbox->isChecked()){
             if (sensor.alarmStatus == daPastA2 || sensor.alarmStatus == daCurrentA2){
                 struct SensorContext sctx;
                 sctx.name = sensors.at(i).alias;
                 sctx.pos = sensors.at(i).pos;
                 quint8 refPos = sensors.at(i).diffmode;
                 if (refPos != 0 && refPos != sctx.pos)
                     sctx.refName = sensors.at(refPos-1).alias;
                 else
                     sctx.refName = "";
                 sctx.refPos = refPos;
                 filterCtx->sensorList.append(sctx);
             }
        }

        else if (ui->a1Checkbox->isChecked()){
            if (sensor.alarmStatus != daNone){
                struct SensorContext sctx;
                sctx.name = sensors.at(i).alias;
                sctx.pos = sensors.at(i).pos;
                quint8 refPos = sensors.at(i).diffmode;
                if (refPos != 0 && refPos != sctx.pos)
                    sctx.refName = sensors.at(refPos-1).alias;
                else
                    sctx.refName = "";
                sctx.refPos = refPos;
                filterCtx->sensorList.append(sctx);
            }
        }

    }

    if (!ui->a1Checkbox->isChecked() && !ui->a2Checkbox->isChecked()) {
        for (int i=0; i<session.totalSensors; i++){
            struct SensorContext sctx;
            sctx.name = sensors.at(i).alias;
            sctx.pos = sensors.at(i).pos;
            quint8 refPos = sensors.at(i).diffmode;
            if (refPos != 0 && refPos != sctx.pos)
                sctx.refName = sensors.at(refPos-1).alias;
            else
                sctx.refName = "";
            sctx.refPos = refPos;
            filterCtx->sensorList.append(sctx);
        }
    }

    sensorListTopItem = 0;

    TG_DEBUG("Filtered sensor list: ");
    for (int i=0; i<filterCtx->sensorList.count(); i++)
        TG_DEBUG(filterCtx->sensorList.at(i).pos);
}

void UiHomePage::on_settingsButton_clicked()
{  
    const QByteArray hashKey = gwmodel->getKey();

    // no key
    if (hashKey.count() == 0){
        emit gotoPage(spMenuPage);
        return;
    }

    // create password dialog
    passDlg = new PasswordDialog(this);
    QObject::connect(passDlg, SIGNAL(passwordEntered(QString)), this, SLOT(on_passDlgPasswordEntered(QString)));
    QObject::connect(passDlg, SIGNAL(onCancel()), this, SLOT(on_passDlgCancel()));
    passDlg->show();
#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif

}

void UiHomePage::on_upButton_clicked()
{
    rotateListUp();
    updateTable();
}

void UiHomePage::on_downButton_clicked()
{
    rotateListDown();
    updateTable();
}

void UiHomePage::on_tableWidget_clicked(const QModelIndex &index)
{
    qint16 sensorListSize;
    qint16 thisRowIndex;
    const Session_t& session = gwmodel->getSession();

    sensorListSize = filterCtx->sensorList.count();
    if (sensorListSize == 0)
        return;

    thisRowIndex = sensorListTopItem + index.row();

    if (thisRowIndex >= sensorListSize)
        thisRowIndex = thisRowIndex - sensorListSize;

    filterCtx->selectedSensorIndex = thisRowIndex;
    if (filterCtx->selectedSensorIndex >= session.totalSensors) {
        TG_DEBUG("Index out of range << " << filterCtx->selectedSensorIndex);
        return;
    }

    TG_DEBUG("Selected sensor " << filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos);

    // set these before going to realtime chart page
    filterCtx->isCurrentSession = true;
    filterCtx->sessionNo = session.sessionnumber;
    filterCtx->totalSensors = session.totalSensors;

    // if offline go straight to logchart
    if (gwmodel->isOnline())
        emit gotoPage(spRealtimeChartPage);
    else
        emit gotoPage(spLogChartPage);
}

void UiHomePage::on_passDlgPasswordEntered(QString password)
{
    TG_DEBUG("Password = " << password);
    const QByteArray hashKey = gwmodel->getKey();
    QByteArray enteredHash = hashPassword(password);

#if defined(TNET_OLIMEX)
        vKeypad->hide();
#endif

    TG_DEBUG("Entered key = " << QString(enteredHash));
    TG_DEBUG("Saved key = " << QString(hashKey));

    if (hashKey == enteredHash){
        TG_DEBUG("Password match");
        passDlg->setResult(true);
        emit gotoPage(spMenuPage);
    } else {
        TG_DEBUG("Password failed");
        passDlg->setResult(false);
    }
}

void UiHomePage::on_passDlgCancel()
{
#if defined(TNET_OLIMEX)
    vKeypad->hide();
#endif
}

void UiHomePage::on_a1Checkbox_toggled(bool checked)
{
    filterCtx->a1Checked = checked;

    // if a1 unchecked and a2 already unchecked, then restore state of playbutton
    if (!filterCtx->a1Checked && !filterCtx->a2Checked){
        ui->playButton->setChecked(playButtonPaused);
        // previous state was active so start the rotation timer
        if (!playButtonPaused)
            rotateTimer.start();

    // a1 checked and a2 unchecked and playbutton not paused, set playbutton as paused
    } else if (filterCtx->a1Checked && !filterCtx->a2Checked && !playButtonPaused) {
        playButtonPaused = ui->playButton->isChecked();
        ui->playButton->setChecked(true);
        rotateTimer.stop();
    }

    updateFilterList();
    updateTable();
}

void UiHomePage::on_a2Checkbox_toggled(bool checked)
{
    filterCtx->a2Checked = checked;

    // if a1 unchecked and a2 already unchecked, then restore state of playbutton
    if (!filterCtx->a2Checked && !filterCtx->a1Checked){
        ui->playButton->setChecked(playButtonPaused);
        // previous state was active so start the rotation timer
        if (!playButtonPaused)
            rotateTimer.start();

    // a1 checked and a2 unchecked and playbutton not paused, set playbutton as paused
    } else if (filterCtx->a2Checked && !filterCtx->a1Checked && !playButtonPaused) {
        playButtonPaused = ui->playButton->isChecked();
        ui->playButton->setChecked(true);
        rotateTimer.stop();
    }

    updateFilterList();
    updateTable();
}

void UiHomePage::on_playButton_toggled(bool checked)
{
    // if one or both are active, return
    if (filterCtx->a1Checked || filterCtx->a2Checked) {
        ui->playButton->setChecked(true);
        rotateTimer.stop();
    } else {
        if (checked)
            rotateTimer.stop();
        else
            rotateTimer.start();
    }
}

void UiHomePage::on_gatewayStreamReceived(TgEvStreamClass evtype)
{
/*#if defined(TNET_OLIMEX)
    if (evtype == ecLive){
        const MuteStatus_t& muteStatus = gwmodel->getMuteStatus();
        ui->muteButton->setChecked(muteStatus.muted);
    }
#endif*/

    // if live temperature stream event
    if (evtype == ecTemp){
        // only if in static mode, update eitherwise it is handled
        // by the rotation timer event in dynamic view
        if (!rotateTimer.isActive())
            updateTable();
    }

}

void UiHomePage::on_muteButton_clicked(bool checked)
{
    Q_UNUSED(checked);

    const InputOutput_t& inout = gwmodel->getInout();
    const MuteStatus_t& muteStatus = gwmodel->getMuteStatus();

    // ignore if audio alerts not enabled
    if (!inout.audioalert){
        ui->muteButton->setEnabled(false);
        return;
    }

    // ignore if mute actions taken has expired
    if (muteStatus.muteActionsTaken == inout.mutecount){
        ui->muteButton->setEnabled(false);
        return;
    }

    // request mute
    gwmodel->requestMuteAlarm();
}
