#ifndef UIHOMEPAGE_H
#define UIHOMEPAGE_H

#include <QWidget>
#include <QTimer>
#include <QModelIndex>

#include "uipagewidget.h"
#include "common.h"
#include "tnetportal.h"
#include "dynamicsensorwidgetitem.h"
#include "passworddialog.h"
#include "virtualkeypaddialog.h"




namespace Ui {
class UiHomePage;
}

class UiHomePage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiHomePage(QWidget *parent = 0,
                        enum WidgetPage pageid = spNotSet,
                        TgGateway * gateway = 0,
                        VirtualKeypadDialog * vKpad = 0,
                        struct FilterContext * ctx = 0,
                        bool celcius = true);
    ~UiHomePage();

    void syncAlarmStateTimeout(bool mark);

private slots:
    void on_rotateTimerTimeout();
    void on_settingsButton_clicked();
    void on_upButton_clicked();
    void on_downButton_clicked();
    void on_a1Checkbox_toggled(bool checked);
    void on_a2Checkbox_toggled(bool checked);
    void on_tableWidget_clicked(const QModelIndex &index);

    void on_passDlgPasswordEntered(QString password);
    void on_passDlgCancel();

    void on_playButton_toggled(bool checked);

    void on_gatewayStreamReceived(TgEvStreamClass evtype);
    void on_muteButton_clicked(bool checked);

private:
    Ui::UiHomePage *ui;
    enum AuthAction authAction;
    PasswordDialog * passDlg;    
    VirtualKeypadDialog * vKeypad;
    struct FilterContext * filterCtx;
    bool degreesCelcius;
    QTimer rotateTimer;
    bool playButtonPaused;
    QList<DynamicSensorWidgetItem *> items;
    qint16 sensorListTopItem;

    void createSensorWidgetItems();
    void updateTable();
    void rotateListUp();
    void rotateListDown();
    void updateFilterList();
};

#endif // UIHOMEPAGE_H
