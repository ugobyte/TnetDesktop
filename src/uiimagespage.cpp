#include "uiimagespage.h"
#include "ui_uiimagespage.h"
#include "emailtablewidgetitem.h"

UiImagesPage::UiImagesPage(QWidget *parent, WidgetPage pageid, TgGateway *gateway) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiImagesPage),
    msgDialog(0)
{
    ui->setupUi(this);


    QString imagesLocation;
#if defined(TNET_DESKTOP)
    imagesLocation = QStandardPaths::locate(QStandardPaths::DesktopLocation,"", QStandardPaths::LocateDirectory);
    TG_DEBUG("Desktop location = " << imagesLocation);
#elif defined(TNET_OLIMEX)
    imagesLocation = QString(TNET_DEVICE_IMAGES_DIR).arg(gwmodel->getDevicePath());
#endif

    // get list of users to select from in the dialog
    const QVector<UserInfo_t>& users = gwmodel->getUsers();
    for (int i=0; i<users.count(); i++){
        EmailRecipient_t s;
        s.name = users.at(i).name;
        s.email = users.at(i).email;
        s.selected = false;
        recipients.append(s);
    }

    // get a list of images to select from in the dialog
    QDir imagesDir(imagesLocation);

    QStringList filters;
    filters << "tnetz_plot_*.png";
    imagesDir.setNameFilters(filters);
    QStringList imageList = imagesDir.entryList();

    for (int i=0; i<imageList.count(); i++){
        TG_DEBUG("Found image at " << imagesLocation << imageList.at(i));
        EmailImage_t s;
        s.name = imageList.at(i);
        s.path = imagesLocation;
        s.selected = false;
        images.append(s);
    }

    ui->recipientsTableWidget->setColumnCount(1);
    ui->recipientsTableWidget->setRowCount(recipients.count());
    for (int i=0; i<recipients.count(); i++){
        EmailTableWidgetItem * w = new EmailTableWidgetItem(0, i, QString("%1 (%2)").arg(recipients.at(i).name,recipients.at(i).email));
        QObject::connect(w,SIGNAL(on_itemToggled(int,bool)),this, SLOT(on_recipientToggled(int,bool)));
        ui->recipientsTableWidget->setCellWidget(i,0,w);
        ui->recipientsTableWidget->setRowHeight(i,40);
    }

    ui->imagesTableWidget->setColumnCount(1);
    ui->imagesTableWidget->setRowCount(images.count());
    for (int i=0; i<images.count(); i++){
        EmailTableWidgetItem * w = new EmailTableWidgetItem(0, i, images.at(i).name);
        QObject::connect(w,SIGNAL(on_itemToggled(int,bool)),this, SLOT(on_imageToggled(int,bool)));
        ui->imagesTableWidget->setCellWidget(i,0,w);
        ui->imagesTableWidget->setRowHeight(i,40);
    }

    TG_DEBUG("Constructing images page");
}

UiImagesPage::~UiImagesPage()
{
    delete ui;

    TG_DEBUG("Destructing images page");
}

void UiImagesPage::on_backButton_clicked()
{
    emit gotoPage(spGeneralSettingsPage);
}

void UiImagesPage::on_recipientToggled(int item, bool state)
{
    EmailRecipient_t& s = recipients.operator [](item);
    s.selected = state;
}

void UiImagesPage::on_imageToggled(int item, bool state)
{
    EmailImage_t& s = images.operator [](item);
    s.selected = state;
}

void UiImagesPage::on_emailImagesRequestResult(bool result, QString repr)
{
    msgDialog->setResult(repr, result);
}

void UiImagesPage::on_sendImagesButton_clicked()
{

    msgDialog = new MessageDialog(this, "Sending ...", true);
    msgDialog->show();

    QStringList imageList;
    QStringList recipientList;
    for (QVector<EmailImage_t>::const_iterator it = images.begin(); it != images.end(); ++it) {
        if (it->selected){
            QString imagesLocation(QString(TNET_DEVICE_IMAGES_DIR).arg(gwmodel->getDevicePath()));
            imageList.append(imagesLocation + "/" + it->name);
        }
    }

    for (QVector<EmailRecipient_t>::const_iterator it = recipients.begin(); it != recipients.end(); ++it) {
        if (it->selected)
            recipientList.append(it->email);
    }

    QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_emailImagesRequestResult(bool,QString)));
    gwmodel->requestEmailImages(imageList, recipientList);
    TG_DEBUG("Requesting images to email");

}
