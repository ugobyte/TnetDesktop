#ifndef UIIMAGESPAGE_H
#define UIIMAGESPAGE_H

#include <QWidget>
#include "uipagewidget.h"
#include "tnetportal.h"
#include "messagedialog.h"

namespace Ui {
class UiImagesPage;
}

typedef struct {
    QString name;
    QString email;
    bool selected;
} EmailRecipient_t;

typedef struct {
    QString path;
    QString name;
    bool selected;
} EmailImage_t;

class UiImagesPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiImagesPage(QWidget *parent = 0,
                          WidgetPage pageid = spNotSet,
                          TgGateway * gateway = 0);
    ~UiImagesPage();

private slots:
    void on_backButton_clicked();
    void on_sendImagesButton_clicked();
    void on_recipientToggled(int item, bool state);
    void on_imageToggled(int item, bool state);
    void on_emailImagesRequestResult(bool result, QString repr);

private:
    Ui::UiImagesPage *ui;
    MessageDialog * msgDialog;
    QVector<EmailRecipient_t> recipients;
    QVector<EmailImage_t> images;
};

#endif // UIIMAGESPAGE_H
