#include "uilogchartpage.h"
#include "ui_uilogchartpage.h"


UiLogChartPage::UiLogChartPage(QWidget *parent,
                               WidgetPage pageid,
                               TgGateway *gateway,
                               FilterContext *ctx,
                               SettingsContext_t * settingsContext) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiLogChartPage),
    filterCtx(ctx),
    msgDialog(0),
    settingsCtx(settingsContext),
    alarmSeachActive(false),
    chartInColor(true)
{
    ui->setupUi(this);

    QSet<ChartItemVisibility> visibleItems;
    if (settingsCtx->chartSettings.a1PlotVisible)
        visibleItems.insert(cvA1);
    if (settingsCtx->chartSettings.a2PlotVisible)
        visibleItems.insert(cvA2);
    if (settingsCtx->chartSettings.diffPlotVisible)
        visibleItems.insert(cvDiff);
    if (settingsCtx->chartSettings.refPlotVisible)
        visibleItems.insert(cvRef);
    if (settingsCtx->chartSettings.hGridVisible)
        visibleItems.insert(cvHGrid);
    if (settingsCtx->chartSettings.vGridVisible)
        visibleItems.insert(cvVGrid);
    if (settingsCtx->chartSettings.hGridVisible)
        visibleItems.insert(cvHGrid);

    logChart = new TgLogChart(ui->chartFrame,settingsContext->degreeCelcius, &visibleItems);
    ui->chartFrame->layout()->addWidget(logChart);

    // allow chart to receive touch events
    logChart->setGateway(gwmodel);
    logChart->setChartActive(false);
    QObject::connect(logChart, SIGNAL(chartReady(bool,bool)), this, SLOT(on_chartQueryReady(bool,bool)));

    // hide menu
    alarmSeachActive = false;
    ui->menuFrame->hide();

    if (filterCtx->isCurrentSession)
        TG_DEBUG("Selected session = " << filterCtx->sessionNo << " (current session)");
    else
        TG_DEBUG("Selected session = " << filterCtx->sessionNo << " (previous session)");
    TG_DEBUG("Selected filter index = " << filterCtx->selectedSensorIndex);
    TG_DEBUG("Selected sensor " << filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name << " (" << filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos << ")");
    if (filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos != 0 &&
            filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos != filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos)
        TG_DEBUG("Selected sensor reference = " << filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refName << " (" << filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos << ")");
    TG_DEBUG("Total sensors in filter list = " << filterCtx->sensorList.count());
    TG_DEBUG("Current session data file = " << filterCtx->currentDataFile);

    // check if need to load from latest if coming from real-time chart
    // else load file from tableview
    if (filterCtx->currentDataFile == "")
        logChart->loadSensorData(filterCtx->sessionNo,
                             filterCtx->totalSensors,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refName,
                             true,
                             "");
    else
        logChart->loadSensorData(filterCtx->sessionNo,
                             filterCtx->totalSensors,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refName,
                             false,
                             filterCtx->currentDataFile);

    logChart->show();

    ui->contrastButton->setChecked(false);

#if defined(TNET_DESKTOP)
    // if not current session, hide and disable
    if (!filterCtx->isCurrentSession){
        ui->syncDataButton->setVisible(false);
        ui->syncDataButton->setEnabled(false);
        ui->syncDataButton->setMinimumWidth(0);
    } else {
        ui->syncDataButton->setVisible(true);
        ui->syncDataButton->setEnabled(true);
        ui->syncDataButton->setMinimumWidth(0);
    }
    QObject::connect(&syncDisableTimer, SIGNAL(timeout()), this, SLOT(syncDisableTimerTimeout()));
#else
    ui->syncDataButton->setVisible(false);
    ui->syncDataButton->setEnabled(false);
    ui->syncDataButton->setMinimumWidth(0);
#endif
}

UiLogChartPage::~UiLogChartPage()
{
    ui->chartFrame->layout()->removeWidget(logChart);
    logChart->deleteLater();
    delete ui;
}

void UiLogChartPage::on_menuButton_clicked()
{
    if (ui->menuFrame->isVisible())
        ui->menuFrame->hide();
    else {
        initialiseMenu();
        ui->menuFrame->show();
    }
}

void UiLogChartPage::on_oldestDataButton_clicked()
{
    logChart->loadOldest();
}

void UiLogChartPage::on_prevDataButton_clicked()
{
    logChart->panLeft();
}

void UiLogChartPage::on_prevSensorButton_clicked()
{
    quint8 preserveCurrentIndex = filterCtx->selectedSensorIndex;

    if (filterCtx->selectedSensorIndex > 0)
        filterCtx->selectedSensorIndex--;
    else
        filterCtx->selectedSensorIndex = filterCtx->sensorList.count() -1;

    // if no change, don't reload
    if (preserveCurrentIndex == filterCtx->selectedSensorIndex)
        return;

    TG_DEBUG("Selected sensor = " << filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos);

    logChart->loadSensorData(filterCtx->sessionNo,
                             filterCtx->totalSensors,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refName,
                             false,
                             logChart->getCurrentFile());
}

void UiLogChartPage::on_nextSensorButton_clicked()
{
    quint8 preserveCurrentIndex = filterCtx->selectedSensorIndex;

    if (filterCtx->selectedSensorIndex < filterCtx->sensorList.count() - 1)
        filterCtx->selectedSensorIndex++;
    else
        filterCtx->selectedSensorIndex = 0;

    // if no change, don't reload
    if (preserveCurrentIndex == filterCtx->selectedSensorIndex)
        return;

    TG_DEBUG("Selected sensor = " << filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos);
    logChart->loadSensorData(filterCtx->sessionNo,
                             filterCtx->totalSensors,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refName,
                             false,
                             logChart->getCurrentFile());
}

void UiLogChartPage::on_nextDataButton_clicked()
{
    logChart->panRight();
}

void UiLogChartPage::on_latestDataButton_clicked()
{
    logChart->loadLatest();
}

void UiLogChartPage::on_backButton_clicked()
{
    if (filterCtx->isCurrentSession){
        if (gwmodel->isOnline())
            emit gotoPage(spRealtimeChartPage);
        else
            emit gotoPage(spHomePage);
    } else
        emit gotoPage(spSessionPreviewPage);
}

void UiLogChartPage::on_imageButton_clicked()
{

    QString imagesLocation;
#if defined(TNET_DESKTOP)
    imagesLocation = QStandardPaths::locate(QStandardPaths::DesktopLocation,"", QStandardPaths::LocateDirectory);
    TG_DEBUG("Desktop location = " << imagesLocation);
#elif defined(TNET_OLIMEX)
    imagesLocation = QString(TNET_DEVICE_IMAGES_DIR).arg(gwmodel->getDevicePath());
#endif

    QDateTime dateTime = dateTime.currentDateTime();
    QString filename = imagesLocation + QString("/tnetz_plot_%1_%2_%3.png").arg(
                filterCtx->sessionNo).arg(
                filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name).arg(
                dateTime.toString("ddMMyyhhmmss"));

    msgDialog = new MessageDialog(this, "", false);
    if (!logChart->saveAsImage("PNG",filename,logChart->width(),logChart->height(),2.0,-1)){
        TG_ERROR("Unable to save to image");
        msgDialog->setResult("Failed",false);
    } else
        msgDialog->setResult("Success",true);
    msgDialog->show();
}

void UiLogChartPage::on_alarmSearchButton_clicked()
{
    msgDialog = new MessageDialog(this, "Searching alarms ...", true);
    msgDialog->show();
    alarmSeachActive = true;
    logChart->searchAlarms();
}

void UiLogChartPage::on_chartQueryReady(bool result, bool alarms)
{
    TG_DEBUG("Load result = " << result << " alarms found = " << alarms);
    // search active
    if (alarmSeachActive){
        alarmSeachActive = false;

        // alarms found, just hide
        if (alarms)
            msgDialog->close();
        else
            msgDialog->setResult("No alarms", false);
    }

    if (result)
        logChart->setChartActive(true);
}

void UiLogChartPage::on_goTableButton_clicked()
{
    // preserve the datafile
    filterCtx->currentDataFile = logChart->getCurrentFile();
    gotoPage(spTableDataPage);
}



void UiLogChartPage::initialiseMenu()
{
    ui->diffPlotCheckbox->setChecked(settingsCtx->chartSettings.diffPlotVisible);
    ui->a1PlotCheckbox->setChecked(settingsCtx->chartSettings.a1PlotVisible);
    ui->a2PlotCheckbox->setChecked(settingsCtx->chartSettings.a2PlotVisible);
    ui->refPlotCheckbox->setChecked(settingsCtx->chartSettings.refPlotVisible);
    ui->legendCheckbox->setChecked(settingsCtx->chartSettings.legendVisible);
    ui->hGridCheckbox->setChecked(settingsCtx->chartSettings.hGridVisible);
    ui->vGridCheckbox->setChecked(settingsCtx->chartSettings.vGridVisible);
}

void UiLogChartPage::on_diffPlotCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.diffPlotVisible = checked;
    logChart->setDiffPlotVisibility(checked);
}

void UiLogChartPage::on_refPlotCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.refPlotVisible = checked;
    logChart->setRefPlotVisibility(checked);
}

void UiLogChartPage::on_a1PlotCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.a1PlotVisible = checked;
    logChart->setAlarm1PlotVisibility(checked);
}

void UiLogChartPage::on_a2PlotCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.a2PlotVisible = checked;
    logChart->setAlarm2PlotVisibility(checked);
}

void UiLogChartPage::on_hGridCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.hGridVisible = checked;
    logChart->setHGridVisibility(checked);
}

void UiLogChartPage::on_vGridCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.vGridVisible = checked;
    logChart->setLegendVisibility(checked);
}

void UiLogChartPage::on_legendCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.legendVisible = checked;
    logChart->setLegendVisibility(checked);
}

void UiLogChartPage::on_contrastButton_clicked(bool checked)
{
    TG_DEBUG("Set print colours = " << checked);
    logChart->setPrintColors(checked);
}

void UiLogChartPage::on_syncDataButton_clicked()
{
    ui->syncDataButton->setDisabled(true);
    // disable sync timer for about 10mins
    syncDisableTimer.start(600000);
    msgDialog = new MessageDialog(this, "Syncing ...", true);
    msgDialog->show();
    QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_recordsSynced(bool,QString)));
    gwmodel->syncRecords();
}

void UiLogChartPage::on_recordsSynced(bool result, QString repr)
{
    Q_UNUSED(repr);
    if (result){
        msgDialog->setResult("Success", true);
        logChart->loadLatest();
    } else {
        msgDialog->setResult("Failed", false);
    }
}

void UiLogChartPage::syncDisableTimerTimeout()
{
    syncDisableTimer.stop();
    ui->syncDataButton->setEnabled(true);
}
