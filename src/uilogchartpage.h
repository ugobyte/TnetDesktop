#ifndef UILOGCHARTPAGE_H
#define UILOGCHARTPAGE_H

#include <QWidget>
#include "uipagewidget.h"
#include "uihomepage.h"
#include "tnetportal.h"
#include "messagedialog.h"



namespace Ui {
class UiLogChartPage;
}



class UiLogChartPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiLogChartPage(QWidget *parent = 0,
                            enum WidgetPage pageid = spNotSet,
                            TgGateway * gateway=0,
                            struct FilterContext * ctx = 0,
                            SettingsContext_t * settingsContext = 0 );
    ~UiLogChartPage();

private slots:
    void on_menuButton_clicked();
    void on_oldestDataButton_clicked();
    void on_prevDataButton_clicked();
    void on_prevSensorButton_clicked();
    void on_nextSensorButton_clicked();
    void on_nextDataButton_clicked();
    void on_latestDataButton_clicked();
    void on_backButton_clicked();
    void on_imageButton_clicked();
    void on_alarmSearchButton_clicked();

    void on_chartQueryReady(bool result, bool alarms);
    void on_goTableButton_clicked();

    void on_diffPlotCheckbox_clicked(bool checked);
    void on_refPlotCheckbox_clicked(bool checked);
    void on_a1PlotCheckbox_clicked(bool checked);
    void on_a2PlotCheckbox_clicked(bool checked);
    void on_hGridCheckbox_clicked(bool checked);
    void on_vGridCheckbox_clicked(bool checked);
    void on_legendCheckbox_clicked(bool checked);

    void on_contrastButton_clicked(bool checked);
    void on_syncDataButton_clicked();

    void on_recordsSynced(bool result, QString repr);
    void syncDisableTimerTimeout();
private:
    Ui::UiLogChartPage *ui;
    struct FilterContext * filterCtx;
    TgLogChart * logChart;
    MessageDialog * msgDialog;
    SettingsContext_t * settingsCtx;
    bool alarmSeachActive;
    bool chartInColor;
    QTimer syncDisableTimer;
    void initialiseMenu();
};

#endif // UILOGCHARTPAGE_H
