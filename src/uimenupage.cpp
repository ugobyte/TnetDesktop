#include "uimenupage.h"
#include "ui_uimenupage.h"

uiMenuPage::uiMenuPage(QWidget *parent, WidgetPage pageid) :
    UiPageWidget(parent,pageid,0),
    ui(new Ui::uiMenuPage)
{
    ui->setupUi(this);
    TG_DEBUG("Constructing menu page");
}

uiMenuPage::~uiMenuPage()
{
    delete ui;

    TG_DEBUG("Destructing menu page");
}



void uiMenuPage::on_backButton_clicked()
{
    gotoPage(spHomePage);
}

void uiMenuPage::on_aboutButton_clicked()
{
    gotoPage(spAboutPage);
}

void uiMenuPage::on_connectivityButton_clicked()
{
    gotoPage(spConnectPage);
}

void uiMenuPage::on_sensorsButton_clicked()
{
    gotoPage(spSessionConfigPage);
}

void uiMenuPage::on_poweroffButton_clicked()
{
    gotoPage(spShutdownPage);
}

void uiMenuPage::on_generalButton_clicked()
{
    gotoPage(spGeneralSettingsPage);
}
