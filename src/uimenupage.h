#ifndef UIMENUPAGE_H
#define UIMENUPAGE_H

#include <QWidget>
#include "uipagewidget.h"

namespace Ui {
class uiMenuPage;
}

class uiMenuPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit uiMenuPage(QWidget *parent = 0, WidgetPage pageid = spNotSet);
    ~uiMenuPage();

private slots:

    void on_backButton_clicked();
    void on_aboutButton_clicked();
    void on_connectivityButton_clicked();
    void on_sensorsButton_clicked();
    void on_poweroffButton_clicked();
    void on_generalButton_clicked();

private:
    Ui::uiMenuPage *ui;
};

#endif // UIMENUPAGE_H
