#include "uipagewidget.h"

UiPageWidget::UiPageWidget(QWidget *parent, enum WidgetPage pageid, TgGateway *gateway) :
    QWidget(parent),
    gwmodel(gateway),
    id(pageid)
{

}

WidgetPage UiPageWidget::getPageId() const
{
    return id;
}
