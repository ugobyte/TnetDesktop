#ifndef UIPAGEWIDGET_H
#define UIPAGEWIDGET_H

#include <QWidget>
#include "tnetportal.h"

enum WidgetPage {spNotSet=0,
                  spHomePage,
                  spMenuPage,
                  spAboutPage,
                  spSettingsPage,
                  spConnectPage,
                  spEthPage,
                  spWifiPage,
                  spCellPage,
                  spEventPage,
                  spShutdownPage,
                  spLogChartPage,
                  spRealtimeChartPage,
                  spSessionConfigPage,
                  spSensorListPage,
                  spSensorConfigPage,
                  spSettingsOptionsPage,
                  spPasswordPage,
                  spTableDataPage,
                  spCredentialPage,
                  spNotificationPage,
                  spUserAddEditPage,
                  spAudioVisualPage,
                  spGeneralSettingsPage,
                  spSessionFolderPage,
                  spSessionPreviewPage,
                  spImagesPage};

class UiPageWidget : public QWidget
{
    Q_OBJECT
public:
    explicit UiPageWidget(QWidget *parent = 0, enum WidgetPage pageId = spNotSet, TgGateway * gateway = 0);
    enum WidgetPage getPageId() const;

protected:
    TgGateway * gwmodel;

signals:
    void gotoPage(enum WidgetPage page);

private:
    enum WidgetPage id;
};

#endif // UIPAGEWIDGET_H
