#include "uipasswordeditpage.h"
#include "ui_uipasswordeditpage.h"

UiPasswordEditPage::UiPasswordEditPage(QWidget *parent, WidgetPage pageid, TgGateway * gateway, VirtualKeypadDialog * keypad) :
    UiPageWidget(parent, pageid, gateway),
    ui(new Ui::UiPasswordEditPage),
    vKeypad(keypad)
{
    ui->setupUi(this);

    ui->newPasswordLineedit->setFocus();

#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif

    TG_DEBUG("Constructing password edit page");
}

UiPasswordEditPage::~UiPasswordEditPage()
{
    delete ui;

    TG_DEBUG("Destructing password edit page");
}

void UiPasswordEditPage::on_backButton_clicked()
{
    gotoPage(spGeneralSettingsPage);
}

void UiPasswordEditPage::on_saveChangesButton_clicked()
{
    if (ui->newPasswordLineedit->text().count() < 4){
        msgDlg = new MessageDialog(this, "Need min 4 characters",false);
        msgDlg->show();
        return;
    }

    // passwords don't match
    if (ui->newPasswordLineedit->text() != ui->confirmPasswordLineedit->text()){
        msgDlg = new MessageDialog(this, "No match",false);
        msgDlg->show();

    } else {
        QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_keyUpdateResponse(bool,QString)));
        gwmodel->requestUpdateKey(ui->newPasswordLineedit->text());
    }

}

void UiPasswordEditPage::on_keyUpdateResponse(bool result, QString repr)
{
    TG_DEBUG("Result = " << repr.toLatin1().data());
    if (result) {
        msgDlg = new MessageDialog(this, "", false);
        msgDlg->setResult("Success",true);
        msgDlg->show();
    } else {
        msgDlg = new MessageDialog(this, "", false);
        msgDlg->setResult("Failed",true);
        msgDlg->show();
    }
}

void UiPasswordEditPage::on_confirmPasswordLineedit_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1);
    Q_UNUSED(arg2);

#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif
}

void UiPasswordEditPage::on_newPasswordLineedit_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1);
    Q_UNUSED(arg2);

#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif
}
