#ifndef UIPASSWORDEDITPAGE_H
#define UIPASSWORDEDITPAGE_H

#include <QWidget>
#include "uipagewidget.h"
#include "virtualkeypaddialog.h"
#include "messagedialog.h"

namespace Ui {
class UiPasswordEditPage;
}

class UiPasswordEditPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiPasswordEditPage(QWidget *parent = 0, WidgetPage pageid = spNotSet, TgGateway * gateway = 0, VirtualKeypadDialog * keypad = 0);
    ~UiPasswordEditPage();

private slots:
    void on_backButton_clicked();
    void on_saveChangesButton_clicked();

    void on_keyUpdateResponse(bool result, QString repr);
    void on_confirmPasswordLineedit_cursorPositionChanged(int arg1, int arg2);

    void on_newPasswordLineedit_cursorPositionChanged(int arg1, int arg2);

private:
    Ui::UiPasswordEditPage *ui;
    VirtualKeypadDialog * vKeypad;
    MessageDialog * msgDlg;
};

#endif // UIPASSWORDEDITPAGE_H
