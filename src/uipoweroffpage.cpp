#include "uipoweroffpage.h"
#include "ui_uipoweroffpage.h"

UiPowerOffPage::UiPowerOffPage(QWidget *parent, WidgetPage pageid, TgGateway *gateway) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiPowerOffPage),
    cfmDlg(0),
    msgDlg(0)
{
    ui->setupUi(this);

    TG_DEBUG("Constructing power off page");
}

UiPowerOffPage::~UiPowerOffPage()
{
    delete ui;

    TG_DEBUG("Destructing power off page");
}

void UiPowerOffPage::on_backButton_clicked()
{
    gotoPage(spMenuPage);
}

void UiPowerOffPage::on_rebootButton_clicked()
{
    poCtx = poReboot;
    cfmDlg = new ConfirmDialog(this, "Reboot ?");
    QObject::connect(cfmDlg, SIGNAL(confirmResult(bool)), this, SLOT(on_confirmDialogResult(bool)));
    cfmDlg->show();
}

void UiPowerOffPage::on_shutdownButton_clicked()
{
    poCtx = poShutdown;
    cfmDlg = new ConfirmDialog(this, "Shutdown ?");
    QObject::connect(cfmDlg, SIGNAL(confirmResult(bool)), this, SLOT(on_confirmDialogResult(bool)));
    cfmDlg->show();
}

void UiPowerOffPage::on_confirmDialogResult(bool result)
{
    cfmDlg->close();

    // reboot
    if (result){
        if (poCtx == poReboot)
            gwmodel->requestPowerOff(true);
        else
            gwmodel->requestPowerOff(false);
    }
}

void UiPowerOffPage::on_powerOffResult(bool result, QString repr)
{
    Q_UNUSED(repr);
    cfmDlg->close();

    if (result){
        msgDlg = new MessageDialog(this, "",false);
        msgDlg->setResult("Success", true);
        msgDlg->show();
    } else {
        msgDlg = new MessageDialog(this, "",false);
        msgDlg->setResult("Failed", false);
        msgDlg->show();
    }
}
