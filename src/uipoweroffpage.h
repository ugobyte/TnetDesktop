#ifndef UIPOWEROFFPAGE_H
#define UIPOWEROFFPAGE_H

#include <QWidget>
#include "uipagewidget.h"
#include "confirmdialog.h"
#include "messagedialog.h"


enum PowerOffContext {poReboot, poShutdown};

namespace Ui {
class UiPowerOffPage;
}

class UiPowerOffPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiPowerOffPage(QWidget *parent = 0, WidgetPage pageid = spNotSet, TgGateway * gateway = 0);
    ~UiPowerOffPage();

private slots:
    void on_backButton_clicked();
    void on_rebootButton_clicked();
    void on_shutdownButton_clicked();

    void on_confirmDialogResult(bool result);
    void on_powerOffResult(bool result, QString repr);
private:
    Ui::UiPowerOffPage *ui;
    ConfirmDialog * cfmDlg;
    MessageDialog * msgDlg;
    enum PowerOffContext poCtx;
};

#endif // UIPOWEROFFPAGE_H
