#include "uirealtimechartpage.h"
#include "ui_uirealtimechartpage.h"

UiRealtimeChartPage::UiRealtimeChartPage(QWidget *parent, WidgetPage pageid, TgGateway *gateway, FilterContext *ctx, SettingsContext_t *settingsContext) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiRealtimeChartPage),
    filterCtx(ctx),
    settingsCtx(settingsContext),
    msgDlg(0),
    chartInColor(true)
{
    ui->setupUi(this);


    QSet<ChartItemVisibility> visibleItems;
    if (settingsCtx->chartSettings.a1PlotVisible)
        visibleItems.insert(cvA1);
    if (settingsCtx->chartSettings.a2PlotVisible)
        visibleItems.insert(cvA2);
    if (settingsCtx->chartSettings.diffPlotVisible)
        visibleItems.insert(cvDiff);
    if (settingsCtx->chartSettings.refPlotVisible)
        visibleItems.insert(cvRef);
    if (settingsCtx->chartSettings.hGridVisible)
        visibleItems.insert(cvHGrid);
    if (settingsCtx->chartSettings.vGridVisible)
        visibleItems.insert(cvVGrid);
    if (settingsCtx->chartSettings.hGridVisible)
        visibleItems.insert(cvHGrid);
    streamChart = new TgStreamChart(ui->chartFrame,settingsCtx->degreeCelcius, &visibleItems);
    ui->chartFrame->layout()->addWidget(streamChart);

    streamChart->setGateway(gwmodel);
    streamChart->setChartActive(false);

    // hide menu
    ui->menuFrame->hide();

    // new plot
    SensorGraphData_t graphOpts;
    graphOpts.sensorPos = filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos;
    graphOpts.sensorAlias = filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name;
    graphOpts.sensorRefPos = filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos;
    graphOpts.sensorRefAlias = filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refName;


    streamChart->newSensorGraph(graphOpts);
    streamChart->setChartActive(true);
    streamChart->refresh();
    streamChart->show();

    ui->contrastButton->setChecked(false);
}

UiRealtimeChartPage::~UiRealtimeChartPage()
{
    ui->chartFrame->layout()->removeWidget(streamChart);
    streamChart->deleteLater();
    delete ui;
}

void UiRealtimeChartPage::on_menuButton_clicked()
{
    if (ui->menuFrame->isVisible())
        ui->menuFrame->hide();
    else {
        initialiseMenu();
        ui->menuFrame->show();
    }
}

void UiRealtimeChartPage::on_prevSensorButton_clicked()
{
    quint8 preserveCurrentIndex = filterCtx->selectedSensorIndex;

    if (filterCtx->selectedSensorIndex > 0)
        filterCtx->selectedSensorIndex--;
    else
        filterCtx->selectedSensorIndex = filterCtx->sensorList.count() -1;

    // if no change, don't reload
    if (preserveCurrentIndex == filterCtx->selectedSensorIndex)
        return;

    TG_DEBUG("Selected sensor index = " << filterCtx->selectedSensorIndex);

    SensorGraphData_t graphOpts;
    graphOpts.sensorPos = filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos;
    graphOpts.sensorAlias = filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name;
    graphOpts.sensorRefPos = filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos;
    graphOpts.sensorRefAlias = filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refName;

    streamChart->newSensorGraph(graphOpts);
    streamChart->setChartActive(true);
    streamChart->refresh();
}

void UiRealtimeChartPage::on_nextSensorButton_clicked()
{   
    quint8 preserveCurrentIndex = filterCtx->selectedSensorIndex;

    if (filterCtx->selectedSensorIndex < filterCtx->sensorList.count() - 1)
        filterCtx->selectedSensorIndex++;
    else
        filterCtx->selectedSensorIndex = 0;

    // if no change, don't reload
    if (preserveCurrentIndex == filterCtx->selectedSensorIndex)
        return;

    TG_DEBUG("Selected sensor index = " << filterCtx->selectedSensorIndex);

    SensorGraphData_t graphOpts;
    graphOpts.sensorPos = filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos;
    graphOpts.sensorAlias = filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name;
    graphOpts.sensorRefPos = filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos;
    graphOpts.sensorRefAlias = filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refName;

    streamChart->newSensorGraph(graphOpts);
    streamChart->setChartActive(true);
    streamChart->refresh();
}

void UiRealtimeChartPage::on_backButton_clicked()
{
    emit gotoPage(spHomePage);
}

void UiRealtimeChartPage::on_imageButton_clicked()
{
    QString imagesLocation;
#if defined(TNET_DESKTOP)
    imagesLocation = QStandardPaths::locate(QStandardPaths::DesktopLocation,"", QStandardPaths::LocateDirectory);
    TG_DEBUG("Desktop location = " << imagesLocation);
#elif defined(TNET_OLIMEX)
    imagesLocation = QString(TNET_DEVICE_IMAGES_DIR).arg(gwmodel->getDevicePath());
#endif

    QDateTime dateTime = dateTime.currentDateTime();
    QString filename = imagesLocation + QString("/tnetz_plot_%1_%2_%3.png").arg(
                filterCtx->sessionNo).arg(
                filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name).arg(
                dateTime.toString("ddMMyyhhmmss"));

    msgDlg = new MessageDialog(this, "", false);

    if (!streamChart->saveAsImage("PNG",filename,streamChart->width(),streamChart->height(),2.0,-1)){
        TG_ERROR("Unable to save to image");
        msgDlg->setResult("Failed",false);
    } else
        msgDlg->setResult("Success",true);

    msgDlg->show();
}

void UiRealtimeChartPage::on_gotoLogChartPageButton_clicked()
{
    const Session_t& session = gwmodel->getSession();
    // first check if there is data
    if (!gwmodel->sessionHasData(session.sessionnumber, false)){
        msgDlg = new MessageDialog(this, "No data",false);
        msgDlg->show();
    } else
        emit gotoPage(spLogChartPage);
}

void UiRealtimeChartPage::initialiseMenu()
{  
    ui->diffPlotCheckbox->setChecked(settingsCtx->chartSettings.diffPlotVisible);
    ui->a1PlotCheckbox->setChecked(settingsCtx->chartSettings.a1PlotVisible);
    ui->a2PlotCheckbox->setChecked(settingsCtx->chartSettings.a2PlotVisible);
    ui->refPlotCheckbox->setChecked(settingsCtx->chartSettings.refPlotVisible);
    ui->legendCheckbox->setChecked(settingsCtx->chartSettings.legendVisible);
    ui->hGridCheckbox->setChecked(settingsCtx->chartSettings.hGridVisible);
    ui->vGridCheckbox->setChecked(settingsCtx->chartSettings.vGridVisible);
}

void UiRealtimeChartPage::on_diffPlotCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.diffPlotVisible = checked;
    streamChart->setDiffPlotVisibility(checked);
}

void UiRealtimeChartPage::on_refPlotCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.refPlotVisible = checked;
    streamChart->setRefPlotVisibility(checked);
}

void UiRealtimeChartPage::on_a1PlotCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.a1PlotVisible = checked;
    streamChart->setAlarm1PlotVisibility(checked);
}

void UiRealtimeChartPage::on_a2PlotCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.a2PlotVisible = checked;
    streamChart->setAlarm2PlotVisibility(checked);
}

void UiRealtimeChartPage::on_hGridCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.hGridVisible = checked;
    streamChart->setHGridVisibility(checked);
}

void UiRealtimeChartPage::on_vGridCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.vGridVisible = checked;
    streamChart->setVGridVisibility(checked);
}

void UiRealtimeChartPage::on_legendCheckbox_clicked(bool checked)
{
    settingsCtx->chartSettings.legendVisible = checked;
    streamChart->setLegendVisibility(checked);
}

void UiRealtimeChartPage::on_contrastButton_clicked(bool checked)
{
    streamChart->setPrintColors(checked);
}
