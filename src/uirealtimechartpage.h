#ifndef UIREALTIMECHARTPAGE_H
#define UIREALTIMECHARTPAGE_H

#include <QWidget>
#include "messagedialog.h"
#include "uipagewidget.h"
#include "uihomepage.h"

namespace Ui {
class UiRealtimeChartPage;
}

class UiRealtimeChartPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiRealtimeChartPage(QWidget *parent = 0,
                                 enum WidgetPage pageid = spNotSet,
                                 TgGateway * gateway = 0,
                                 struct FilterContext * ctx = 0,
                                 SettingsContext_t * settingsContext = 0);
    ~UiRealtimeChartPage();

private slots:

    void on_menuButton_clicked();
    void on_prevSensorButton_clicked();
    void on_nextSensorButton_clicked();
    void on_backButton_clicked();
    void on_imageButton_clicked();
    void on_gotoLogChartPageButton_clicked();

    void on_diffPlotCheckbox_clicked(bool checked);
    void on_refPlotCheckbox_clicked(bool checked);
    void on_a1PlotCheckbox_clicked(bool checked);
    void on_a2PlotCheckbox_clicked(bool checked);
    void on_hGridCheckbox_clicked(bool checked);
    void on_vGridCheckbox_clicked(bool checked);
    void on_legendCheckbox_clicked(bool checked);

    void on_contrastButton_clicked(bool checked);

private:
    Ui::UiRealtimeChartPage *ui;
    TgStreamChart * streamChart;
    struct FilterContext * filterCtx;
    SettingsContext_t * settingsCtx;
    MessageDialog * msgDlg;
    bool chartInColor;
    bool printColorsEnabled;
    void initialiseMenu();
};

#endif // UIREALTIMECHARTPAGE_H
