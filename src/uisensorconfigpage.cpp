#include "uisensorconfigpage.h"
#include "ui_uisensorconfigpage.h"

UiSensorConfigPage::UiSensorConfigPage(QWidget *parent,
                                       WidgetPage pageid,
                                       TgGateway * gateway,
                                       Session_t *session,
                                       QVector<Sensor_t> *sensors,
                                       int sensorItem,
                                       enum SessionAction action,
                                       VirtualKeypadDialog * keypad) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiSensorConfigPage),
    sessionCopy(session),
    sensorsCopy(sensors),
    sensorItemSelected(sensorItem),
    sessionAction(action),
    vkeypad(keypad)
{
    ui->setupUi(this);

    // adding a new sensor
    if (sensorItemSelected == -1) {

        sessionCopy->totalSensors++;

        Sensor_t addSensor;
        addSensor.a1 = 40;
        addSensor.a2 = 50;
        addSensor.alias = "";
        addSensor.serial = "";
        addSensor.pos = sessionCopy->totalSensors;
        addSensor.diffmode = 0;

        sensorsCopy->append(addSensor);
        sensorItemSelected = sessionCopy->totalSensors - 1;

        ui->posSpinbox->setEnabled(true);
        ui->refSpinbox->setEnabled(true);
        ui->serialLineEdit->setEnabled(true);
    }

    // name has focus
    ui->nameLineedit->setFocus();

    // update the ui
    ui->posSpinbox->setValue(sensorsCopy->at(sensorItemSelected).pos);
    ui->serialLineEdit->setText(sensorsCopy->at(sensorItemSelected).serial);
    ui->nameLineedit->setText(sensorsCopy->at(sensorItemSelected).alias);
    ui->a1Spinbox->setValue(sensorsCopy->at(sensorItemSelected).a1);
    ui->a2Spinbox->setValue(sensorsCopy->at(sensorItemSelected).a2);
    ui->refSpinbox->setValue(sensorsCopy->at(sensorItemSelected).diffmode);

    TG_DEBUG("Constructing sensor config page");
}



UiSensorConfigPage::~UiSensorConfigPage()
{
    delete ui;

    TG_DEBUG("Destructing sensor config page");
}




void UiSensorConfigPage::on_posSpinbox_valueChanged(int arg1)
{
    Sensor_t& sensor = sensorsCopy->operator [](sensorItemSelected);
    sensor.pos = arg1;
}

void UiSensorConfigPage::on_a1Spinbox_valueChanged(int arg1)
{
    Sensor_t& sensor = sensorsCopy->operator [](sensorItemSelected);
    sensor.a1 = arg1;
}


void UiSensorConfigPage::on_a2Spinbox_valueChanged(int arg1)
{
    Sensor_t& sensor = sensorsCopy->operator [](sensorItemSelected);
    sensor.a2 = arg1;
}

void UiSensorConfigPage::on_refSpinbox_valueChanged(int arg1)
{
    Sensor_t& sensor = sensorsCopy->operator [](sensorItemSelected);

    if (arg1 == sensor.pos){
        ui->refSpinbox->setValue(sensor.diffmode);
    } else
        sensor.diffmode = arg1;
}

void UiSensorConfigPage::on_nameLineedit_textChanged(const QString &arg1)
{
    Sensor_t& sensor = sensorsCopy->operator [](sensorItemSelected);
    sensor.alias = arg1;
}

void UiSensorConfigPage::on_serialLineEdit_textChanged(const QString &arg1)
{
    Sensor_t& sensor = sensorsCopy->operator [](sensorItemSelected);
    sensor.serial = arg1;
}

void UiSensorConfigPage::on_backButton_clicked()
{
    gotoPage(spSensorListPage);
}


void UiSensorConfigPage::on_nameLineedit_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1);
    Q_UNUSED(arg2);

#if defined(TNET_OLIMEX)
    vkeypad->show();
#endif

}

void UiSensorConfigPage::on_serialLineEdit_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1);
    Q_UNUSED(arg2);

#if defined(TNET_OLIMEX)
    vkeypad->show();
#endif
}
