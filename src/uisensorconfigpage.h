#ifndef UISENSORCONFIGPAGE_H
#define UISENSORCONFIGPAGE_H

#include <QWidget>
#include "uipagewidget.h"
#include "common.h"
#include "virtualkeypaddialog.h"


namespace Ui {
class UiSensorConfigPage;
}

class UiSensorConfigPage : public UiPageWidget
{
    Q_OBJECT

public:

    explicit UiSensorConfigPage(QWidget *parent = 0,
                                WidgetPage pageid = spNotSet,
                                TgGateway * gateway = 0,
                                Session_t *session = 0,
                                QVector<Sensor_t> *sensors = 0,
                                int sensorItem = -1,
                                enum SessionAction action = saResume,
                                VirtualKeypadDialog * keypad = 0);


    ~UiSensorConfigPage();

private slots:
    void on_posSpinbox_valueChanged(int arg1);
    void on_a1Spinbox_valueChanged(int arg1);
    void on_a2Spinbox_valueChanged(int arg1);
    void on_refSpinbox_valueChanged(int arg1);
    void on_nameLineedit_textChanged(const QString &arg1);
    void on_serialLineEdit_textChanged(const QString &arg1);
    void on_backButton_clicked();

    void on_nameLineedit_cursorPositionChanged(int arg1, int arg2);

    void on_serialLineEdit_cursorPositionChanged(int arg1, int arg2);

private:
    Ui::UiSensorConfigPage *ui;
    Session_t * sessionCopy;
    QVector<Sensor_t> * sensorsCopy;
    int sensorItemSelected;
    enum SessionAction sessionAction;
    VirtualKeypadDialog * vkeypad;
};

#endif // UISENSORCONFIGPAGE_H
