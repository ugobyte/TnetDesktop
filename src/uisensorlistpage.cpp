#include "uisensorlistpage.h"
#include "ui_uisensorlistpage.h"

UiSensorListPage::UiSensorListPage(QWidget *parent,
                                   WidgetPage pageid,
                                   TgGateway *gateway,
                                   Session_t *session,
                                   QVector<Sensor_t> * sensors,
                                   enum SessionAction action) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiSensorListPage),
    sessionCopy(session),
    sensorsCopy(sensors),
    sensorItemSelected(-1),
    sessionAction(action)
{
    ui->setupUi(this);

    // clear all list button
    if (sessionAction == saCreateNew){
        ui->clearListButton->setEnabled(true);
        ui->addSensorButton->setEnabled(true);
        ui->highhighButton->setEnabled(true);
        ui->highlowButton->setEnabled(true);
        ui->lowlowButton->setEnabled(true);

    }else if (sessionAction == saResume){
        ui->clearListButton->setEnabled(false);
        ui->addSensorButton->setEnabled(false);

        ui->highhighButton->setEnabled(false);
        ui->highlowButton->setEnabled(false);
        ui->lowlowButton->setEnabled(false);
    }

    ui->sessionNameLineedit->setText(sessionCopy->sessionname);

    switch(sessionCopy->alarmtype){
        case aiNone:
            ui->highhighButton->setChecked(true);
            ui->highlowButton->setChecked(false);
            ui->lowlowButton->setChecked(false);
            break;
        case aiHH:
            ui->highhighButton->setChecked(true);
            ui->highlowButton->setChecked(false);
            ui->lowlowButton->setChecked(false);
            break;
        case aiHL:
            ui->highhighButton->setChecked(false);
            ui->highlowButton->setChecked(true);
            ui->lowlowButton->setChecked(false);
            break;
        case aiLL:
            ui->highhighButton->setChecked(false);
            ui->highlowButton->setChecked(false);
            ui->lowlowButton->setChecked(true);
            break;
        default:
            ui->highhighButton->setChecked(true);
            ui->highlowButton->setChecked(false);
            ui->lowlowButton->setChecked(false);
            break;
    }

    if (sessionCopy->triggerrate == 1)
        ui->alarmFilterButton->setChecked(false);
    else
        ui->alarmFilterButton->setChecked(true);

    // populate the table
    updateTable();

    TG_DEBUG("Constructing sensor list page");
}

UiSensorListPage::~UiSensorListPage()
{
    delete ui;
    TG_DEBUG("Destructing sensor list page");
}

int UiSensorListPage::getSelectedSensor() const
{
    return sensorItemSelected;
}

void UiSensorListPage::on_backButton_clicked()
{
    gotoPage(spSessionConfigPage);
}

void UiSensorListPage::on_tableWidget_clicked(const QModelIndex &index)
{
    sensorItemSelected = index.row();
    gotoPage(spSensorConfigPage);
}

void UiSensorListPage::on_sensorListItemRemoved(int itemPosition)
{
    // reduce total sensors
    if (sessionCopy->totalSensors > 0)
        sessionCopy->totalSensors--;

    // remove from sensorsCopy
    QVector<Sensor_t>::iterator it = sensorsCopy->begin();
    while (it != sensorsCopy->end()){
        if (it->pos == itemPosition){
            TG_DEBUG("Removing sensor at " << it->pos);
            it = sensorsCopy->erase(it);
        } else
            it++;
    }

    sessionCopy->totalSensors = sensorsCopy->count();
    TG_DEBUG("Total sensors = " << sessionCopy->totalSensors);

    // update the table
    updateTable();
}

void UiSensorListPage::updateTable()
{
    sensorListTop = 0;
    ui->tableWidget->clearContents();

    // total rows and columns
    ui->tableWidget->setRowCount(sessionCopy->totalSensors);
    ui->tableWidget->setColumnCount(1);

    // create widgets
    for (int i=0; i<sessionCopy->totalSensors; i++){
        bool createnew = (sessionAction == saCreateNew)? true: false;
        SensorListItem *sensorListItem = new SensorListItem(0, sensorsCopy->at(i), createnew);
        ui->tableWidget->setCellWidget(i,0,sensorListItem);
        ui->tableWidget->setRowHeight(i,60);
        QObject::connect(sensorListItem, SIGNAL(sensorRemoved(int)), this, SLOT(on_sensorListItemRemoved(int)));
    }
}

void UiSensorListPage::on_clearListButton_clicked()
{
    /* TODO: Over here maybe have a way of preserving the copy but marking it as deleted so that
             user can undo the operation without loss of data */

    // clear all
    sessionCopy->totalSensors = 0;

    // remove from sensorsCopy
    sensorsCopy->clear();

    // update table
    updateTable();
}

void UiSensorListPage::on_undoDeleteButton_clicked()
{
    // undo last delete operation
}

void UiSensorListPage::on_saveChangesButton_clicked()
{
    // bring confirm dialog

    if (sessionAction == saResume) {
        msgDlg = new MessageDialog(this, "Uploading ...", true);
        msgDlg->show();

        QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_sessionUploadResult(bool,QString)));
        gwmodel->requestSessionResume(sessionCopy, sensorsCopy);
    } else if (sessionAction == saCreateNew){

        // first sync data for desktop

#if defined(TNET_DESKTOP)
        QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_syncDataResult(bool,QString)));
        gwmodel->syncRecords();

        msgDlg = new MessageDialog(this, "Syncing first ...", true);
        msgDlg->show();
#else
        QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_sessionUploadResult(bool,QString)));
        gwmodel->requestSessionNew(sessionCopy, sensorsCopy);

        msgDlg = new MessageDialog(this, "Uploading session ...", true);
        msgDlg->show();
#endif
    }
}

void UiSensorListPage::on_addSensorButton_clicked()
{
    // will indicate that we are adding sensor not editing
    sensorItemSelected = -1;
    gotoPage(spSensorConfigPage);
}

void UiSensorListPage::on_sessionUploadResult(bool result, QString repr)
{
    msgDlg->close();

    TG_DEBUG("Result = " << result << " repr = " << repr);

    if (result) {
        msgDlg = new MessageDialog(this, "", false);
        msgDlg->setResult("Session upload success",true);
        msgDlg->show();
    } else {
        msgDlg = new MessageDialog(this, "", false);
        msgDlg->setResult("Session upload failed",false);
        msgDlg->show();
    }
}

void UiSensorListPage::on_syncDataResult(bool result, QString repr)
{
    Q_UNUSED(result);
    TG_WARNING("Sync data result = " << repr.toLatin1().data());
    msgDlg->close();

    // need to remove sync result slot from signal mapping before connecting next signal
    QObject::disconnect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_syncDataResult(bool,QString)));

    // now start new
    QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_sessionUploadResult(bool,QString)));
    gwmodel->requestSessionNew(sessionCopy, sensorsCopy);

    msgDlg = new MessageDialog(this, "Uploading session ...", true);
    msgDlg->show();
}

void UiSensorListPage::on_alarmFilterButton_clicked(bool checked)
{
    if (checked){
        TG_DEBUG("Enable alarm trigger filter");
        sessionCopy->triggerrate = 3;
    } else {
        TG_DEBUG("Disable alarm trigger filter");
        sessionCopy->triggerrate = 1;
    }
}

void UiSensorListPage::on_alarmInterpretationCombobox_currentIndexChanged(int index)
{
    TG_DEBUG("Alarm interpretation changed to " << index);
    sessionCopy->alarmtype = (TgAlarmInterpretation)(index + 1);
}

void UiSensorListPage::on_sessionNameLineedit_textChanged(const QString &arg1)
{
    sessionCopy->sessionname = arg1;
    TG_DEBUG("Session name changed to " << arg1);
}

void UiSensorListPage::on_highhighButton_clicked(bool checked)
{
    if (checked)
        ui->highlowButton->setChecked(false);
    else
        ui->highlowButton->setChecked(true);

    ui->lowlowButton->setChecked(false);
}


void UiSensorListPage::on_highlowButton_clicked(bool checked)
{
    if (checked)
        ui->highhighButton->setChecked(false);
    else
        ui->highhighButton->setChecked(true);

    ui->lowlowButton->setChecked(false);
}

void UiSensorListPage::on_lowlowButton_clicked(bool checked)
{
    if (checked)
        ui->highhighButton->setChecked(false);
    else
        ui->highhighButton->setChecked(true);

    ui->highlowButton->setChecked(false);
}
