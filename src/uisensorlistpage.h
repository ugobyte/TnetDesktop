#ifndef UISENSORLISTPAGE_H
#define UISENSORLISTPAGE_H

#include <QWidget>
#include <QModelIndex>
#include "uipagewidget.h"
#include "sensorlistitem.h"
#include "common.h"
#include "messagedialog.h"

namespace Ui {
class UiSensorListPage;
}

class UiSensorListPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiSensorListPage(QWidget *parent = 0,
                              enum WidgetPage pageid = spNotSet,
                              TgGateway * gateway = 0,
                              Session_t * session = 0,
                              QVector<Sensor_t> *sensors = 0,
                              enum SessionAction action = saIgnored);
    ~UiSensorListPage();

    int getSelectedSensor() const;

private slots:
    void on_backButton_clicked();
    void on_tableWidget_clicked(const QModelIndex &index);

    void on_sensorListItemRemoved(int itemPosition);
    void on_clearListButton_clicked();
    void on_undoDeleteButton_clicked();
    void on_saveChangesButton_clicked();
    void on_addSensorButton_clicked();

    void on_sessionUploadResult(bool result, QString repr);
    void on_syncDataResult(bool result, QString repr);

    void on_alarmFilterButton_clicked(bool checked);
    void on_alarmInterpretationCombobox_currentIndexChanged(int index);

    void on_sessionNameLineedit_textChanged(const QString &arg1);

    void on_highhighButton_clicked(bool checked);
    void on_highlowButton_clicked(bool checked);
    void on_lowlowButton_clicked(bool checked);

private:
    Ui::UiSensorListPage *ui;
    Session_t * sessionCopy;
    QVector<Sensor_t> * sensorsCopy;
    MessageDialog * msgDlg;
    int sensorListTop;
    int sensorItemSelected;
    enum SessionAction sessionAction;
    void updateTable();
};

#endif // UISENSORLISTPAGE_H
