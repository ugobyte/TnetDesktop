#include "uisessionfolderviewpage.h"
#include "ui_uisessionfolderviewpage.h"
#include "sessionfolderwidget.h"
#include "tnetportal.h"

UiSessionFolderViewPage::UiSessionFolderViewPage(QWidget *parent, WidgetPage pageid, TgGateway *gateway, FilterContext *ctx) :
    UiPageWidget(parent, pageid, gateway),
    ui(new Ui::UiSessionFolderViewPage),
    filterCtx(ctx)
{
    ui->setupUi(this);

    discoverSessions();
    updateTable();

    TG_DEBUG("Constructing Session folder view page");
}

UiSessionFolderViewPage::~UiSessionFolderViewPage()
{
    delete ui;

    TG_DEBUG("Destructing Session folder view page");
}

void UiSessionFolderViewPage::on_tableWidget_clicked(const QModelIndex &index)
{
    TG_DEBUG("Session folder " << sessions.at(index.row()) << " selected");   
    filterCtx->isCurrentSession = false;
    filterCtx->sessionNo = sessions.at(index.row());
    // delete filter list
    filterCtx->sensorList.clear();
    emit gotoPage(spSessionPreviewPage);
}

void UiSessionFolderViewPage::discoverSessions()
{
    const QString devicePath = gwmodel->getDevicePath();
    const Session_t& session = gwmodel->getSession();

    QDir sessionDir(QString(TNET_DEVICE_ROOT_SESSION_DIR).arg(devicePath));
    QStringList discovery = sessionDir.entryList(QDir::NoDotAndDotDot | QDir::Dirs);

    // remove any sessions that don't have data and/or a temperature.json file
    for (QStringList::const_iterator it = discovery.constBegin(); it != discovery.constEnd(); ++it){
        // check for temperature.json and data  files
        TG_DEBUG("Checking " << *it << " session folder");

        if (*it == session.sessionnumber) {
            TG_DEBUG("Ignoring current session folder");
            continue;
        }

        if (gwmodel->sessionHasData(*it, true))
            sessions.append(*it);

    }

    TG_DEBUG("Discovered session folders:");
    for (int i=0; i<sessions.count(); i++){
        TG_DEBUG(sessions.at(i));
    }  
}

void UiSessionFolderViewPage::updateTable()
{
    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(sessions.count());
    ui->tableWidget->setColumnCount(1);

    for (int i=0; i<sessions.count(); i++){
        SessionFolderWidget * w = new SessionFolderWidget;
        QString sessionname = gwmodel->getSessionName(sessions.at(i));
        w->updateData(sessionname, sessions.at(i), "", 0);
        ui->tableWidget->setCellWidget(i,0,w);
        ui->tableWidget->setRowHeight(i,60);
    }
}

void UiSessionFolderViewPage::on_backButton_clicked()
{
    emit gotoPage(spSessionConfigPage);
}
