#ifndef UISESSIONFOLDERVIEWPAGE_H
#define UISESSIONFOLDERVIEWPAGE_H

#include <QWidget>
#include <QModelIndex>
#include "uipagewidget.h"
#include "common.h"

namespace Ui {
class UiSessionFolderViewPage;
}

class UiSessionFolderViewPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiSessionFolderViewPage(QWidget *parent = 0,
                                     WidgetPage pageid = spNotSet,
                                     TgGateway * gateway = 0,
                                     struct FilterContext * ctx = 0);
    ~UiSessionFolderViewPage();

private slots:
    void on_tableWidget_clicked(const QModelIndex &index);
    void on_backButton_clicked();

private:
    Ui::UiSessionFolderViewPage *ui;
    QStringList sessions;
    struct FilterContext * filterCtx;
    void discoverSessions();
    void updateTable();

};

#endif // UISESSIONFOLDERVIEWPAGE_H
