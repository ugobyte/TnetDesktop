#include "uisessionmenupage.h"
#include "ui_uisessionmenupage.h"

UiSessionMenuPage::UiSessionMenuPage(QWidget *parent, WidgetPage pageid, TgGateway *gateway, SettingsContext_t * generalSettings) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiSessionMenuPage),
    sessionAction(saIgnored),
    settingsCtx(generalSettings)
{
    ui->setupUi(this);

    ui->degreesButton->setChecked(settingsCtx->degreeCelcius);
    if (settingsCtx->degreeCelcius)
        ui->degreesLabel->setText("Temperature in degrees Celsius");
    else
        ui->degreesLabel->setText("Temperature in degrees Fahrenheit");

    TG_DEBUG("Constructing session menu page");
}

UiSessionMenuPage::~UiSessionMenuPage()
{
    delete ui;
    TG_DEBUG("Destructing session menu page");
}

SessionAction UiSessionMenuPage::getSessionAction() const
{
    return sessionAction;
}

void UiSessionMenuPage::on_restartButton_clicked()
{
    // create confirm dialog
    cfmDlg = new ConfirmDialog(this,"Confirm restart ?");
    QObject::connect(cfmDlg,SIGNAL(confirmResult(bool)), this, SLOT(on_confirmDlgResult(bool)));
    cfmDlg->show();
}

void UiSessionMenuPage::on_changeResumeButton_clicked()
{
    sessionAction = saResume;
    gotoPage(spSensorListPage);
}

void UiSessionMenuPage::on_createStartNewButton_clicked()
{
    sessionAction = saCreateNew;
    gotoPage(spSensorListPage);
}

void UiSessionMenuPage::on_sessionRestartResult(bool result, QString repr)
{
    // close loader
    msgDlg->close();

    // create result dlg
    msgDlg = new MessageDialog(this, "", false);
    msgDlg->setResult(repr,result);
    msgDlg->show();
}

void UiSessionMenuPage::on_syncDataResult(bool result, QString repr)
{
    Q_UNUSED(result);
    TG_WARNING("Sync data result = " << repr.toLatin1().data());
    msgDlg->close();

    // need to remove sync result slot from signal mapping before connecting next signal
    QObject::disconnect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_syncDataResult(bool,QString)));

    QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_sessionRestartResult(bool,QString)));
    gwmodel->requestSessionRestart();
    // show loader
    msgDlg = new MessageDialog(this, "Restarting session ...", true);
    msgDlg->show();
}

void UiSessionMenuPage::on_confirmDlgResult(bool result)
{
    cfmDlg->close();
    if (result){

        // if desktop, first sync data
#if defined(TNET_DESKTOP)
        // first sync data
        QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_syncDataResult(bool,QString)));
        gwmodel->syncRecords();

        msgDlg = new MessageDialog(this, "Syncing first ...", true);
#else
        QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_sessionRestartResult(bool,QString)));
        gwmodel->requestSessionRestart();

        msgDlg = new MessageDialog(this, "Restarting session ...", true);
#endif
        // show loader
        msgDlg->show();

    }
}

void UiSessionMenuPage::on_backButton_clicked()
{
    sessionAction = saIgnored;
    gotoPage(spMenuPage);
}

void UiSessionMenuPage::on_degreesButton_toggled(bool checked)
{
    settingsCtx->degreeCelcius = checked;
    if (settingsCtx->degreeCelcius)
        ui->degreesLabel->setText("Temperature in degrees Celsius");
    else
        ui->degreesLabel->setText("Temperature in degrees Fahrenheit");
}

void UiSessionMenuPage::on_sessionFolderButton_clicked()
{
    emit gotoPage(spSessionFolderPage);
}
