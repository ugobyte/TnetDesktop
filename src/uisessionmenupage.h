#ifndef UISESSIONMENUPAGE_H
#define UISESSIONMENUPAGE_H

#include <QWidget>
#include "uipagewidget.h"
#include "confirmdialog.h"
#include "messagedialog.h"
#include "common.h"


namespace Ui {
class UiSessionMenuPage;
}

class UiSessionMenuPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiSessionMenuPage(QWidget *parent = 0, enum WidgetPage pageid = spNotSet, TgGateway * gateway = 0, SettingsContext_t *generalSettings = 0);
    ~UiSessionMenuPage();

    enum SessionAction getSessionAction(void) const;

private slots:
    void on_restartButton_clicked();
    void on_changeResumeButton_clicked();
    void on_createStartNewButton_clicked();

    void on_sessionRestartResult(bool result, QString repr);
    void on_syncDataResult(bool result, QString repr);

    void on_confirmDlgResult(bool result);
    void on_backButton_clicked();

    void on_degreesButton_toggled(bool checked);
    void on_sessionFolderButton_clicked();

private:
    Ui::UiSessionMenuPage *ui;
    ConfirmDialog * cfmDlg;
    MessageDialog * msgDlg;
    enum SessionAction sessionAction;
    SettingsContext_t * settingsCtx;
};

#endif // UISESSIONMENUPAGE_H
