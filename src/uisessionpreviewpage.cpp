#include "uisessionpreviewpage.h"
#include "ui_uisessionpreviewpage.h"
#include "sensorpreviewwidgetitem.h"

UiSessionPreviewPage::UiSessionPreviewPage(QWidget *parent, WidgetPage pageid, TgGateway *gateway, FilterContext *ctx, bool degreesInCelsius) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiSessionPreviewPage),
    filterCtx(ctx),
    degreesCelsius(degreesInCelsius)
{
    ui->setupUi(this);

    // load session
    bool result = loadSession();

    if (!result) {
        ui->sessionNoLabel->setText(filterCtx->sessionNo);
        ui->sessionNameLabel->clear();
        ui->tableWidget->clear();

        // show fail message
        TG_WARNING("Unable to load session");
    } else {

        ui->sessionNoLabel->setText(session.sessionnumber);
        ui->sessionNameLabel->setText(session.sessionname);

        if (filterCtx->sensorList.count() == 0)
            updateFilterList();
        updateTable();
    }

    ui->a1Checkbox->setChecked(filterCtx->a1Checked);
    ui->a2Checkbox->setChecked(filterCtx->a2Checked);

    TG_DEBUG("Constructing session preview page");
}

UiSessionPreviewPage::~UiSessionPreviewPage()
{
    delete ui;

    TG_DEBUG("Destructing session preview page");
}

void UiSessionPreviewPage::on_backButton_clicked()
{
    emit gotoPage(spSessionFolderPage);
}

void UiSessionPreviewPage::on_a1Checkbox_toggled(bool checked)
{
    filterCtx->a1Checked = checked;
    updateFilterList();
    updateTable();
}


void UiSessionPreviewPage::on_a2Checkbox_toggled(bool checked)
{
    filterCtx->a2Checked = checked;
    updateFilterList();
    updateTable();
}

bool UiSessionPreviewPage::loadSession()
{

    TgJson json;
    bool result;
    const QString gatewayPath = gwmodel->getDevicePath();

    TG_DEBUG("Previewing session " << filterCtx->sessionNo);
    // gateway json
    QVariantMap tgTemp = json.parseFile(
                QString(TNET_DEVICE_SESSION_DATA_FILE).arg(gatewayPath, filterCtx->sessionNo, "temperature.json"),
                result).toMap();

    if (!result)
        return false;

    try {

        QVariantMap tgSession = tgTemp["Session"].toMap();
        QVariantMap tgSensors = tgTemp["Sensors"].toMap();

        session.seshcommit = tgTemp["Commit"].toInt();
        session.totalSensors = tgSession["TotalSensors"].toInt();
        session.sessionname = tgSession["Alias"].toString();
        session.sessionnumber = tgSession["Number"].toString();
        session.alarmtype = (TgAlarmInterpretation)tgSession["AlarmType"].toInt();
        session.triggerrate = tgSession["TriggerRate"].toInt();
        filterCtx->totalSensors = session.totalSensors;
        sensors.clear();
        for (int i=0; i<session.totalSensors; i++) {
           QVariantMap tgSensor = tgSensors[QString("%1").arg(i+1)].toMap();
           Sensor_t sensor;
           sensor.pos = i+1;
           sensor.alias = tgSensor["Alias"].toString();
           sensor.serial = tgSensor["Serial"].toString();
           sensor.a1 = tgSensor["A1"].toInt();
           sensor.a2 = tgSensor["A2"].toInt();
           sensor.diffmode = tgSensor["Diffmode"].toInt();
           sensor.a1trig = tgSensor["A1trig"].toBool();
           sensor.a2trig = tgSensor["A2trig"].toBool();
           if (sensor.a2trig)
               sensor.alarmStatus = daPastA2;
           else if (sensor.a1trig)
               sensor.alarmStatus = daPastA1;
           else
               sensor.alarmStatus = daNone;
           sensors.append(sensor);
        }

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        return false;
    }

    return true;

}

void UiSessionPreviewPage::updateTable()
{
    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(filterCtx->sensorList.count());
    ui->tableWidget->setColumnCount(1);

    for (int i=0; i<filterCtx->sensorList.count(); i++){
        SensorPreviewWidgetItem * w = new SensorPreviewWidgetItem(0, sensors.at(filterCtx->sensorList.at(i).pos-1), degreesCelsius);
        ui->tableWidget->setCellWidget(i,0,w);
        ui->tableWidget->setRowHeight(i,50);
    }

}

void UiSessionPreviewPage::updateFilterList()
{
    filterCtx->sensorList.clear();

    for (int i=0; i<session.totalSensors; i++){
        const Sensor_t& sensor = sensors.at(i);

        if (ui->a1Checkbox->isChecked() && ui->a2Checkbox->isChecked()){
            if (sensor.alarmStatus != daNone){
                struct SensorContext sctx;
                sctx.name = sensors.at(i).alias;
                sctx.pos = sensors.at(i).pos;
                quint8 refPos = sensors.at(i).diffmode;
                if (refPos != 0 && refPos != sctx.pos)
                    sctx.refName = sensors.at(refPos-1).alias;
                else
                    sctx.refName = "";
                sctx.refPos = refPos;
                filterCtx->sensorList.append(sctx);
            }
        }

        else if (ui->a2Checkbox->isChecked()){
             if (sensor.alarmStatus == daPastA2){
                 struct SensorContext sctx;
                 sctx.name = sensors.at(i).alias;
                 sctx.pos = sensors.at(i).pos;
                 quint8 refPos = sensors.at(i).diffmode;
                 if (refPos != 0 && refPos != sctx.pos)
                     sctx.refName = sensors.at(refPos-1).alias;
                 else
                     sctx.refName = "";
                 sctx.refPos = refPos;
                 filterCtx->sensorList.append(sctx);
             }
        }

        else if (ui->a1Checkbox->isChecked()){
            if (sensor.alarmStatus != daNone){
                struct SensorContext sctx;
                sctx.name = sensors.at(i).alias;
                sctx.pos = sensors.at(i).pos;
                quint8 refPos = sensors.at(i).diffmode;
                if (refPos != 0 && refPos != sctx.pos)
                    sctx.refName = sensors.at(refPos-1).alias;
                else
                    sctx.refName = "";
                sctx.refPos = refPos;
                filterCtx->sensorList.append(sctx);
            }
        }

    }

    if (!ui->a1Checkbox->isChecked() && !ui->a2Checkbox->isChecked()) {
        for (int i=0; i<session.totalSensors; i++){
            struct SensorContext sctx;
            sctx.name = sensors.at(i).alias;
            sctx.pos = sensors.at(i).pos;
            quint8 refPos = sensors.at(i).diffmode;
            if (refPos != 0 && refPos != sctx.pos)
                sctx.refName = sensors.at(refPos-1).alias;
            else
                sctx.refName = "";
            sctx.refPos = refPos;
            filterCtx->sensorList.append(sctx);
        }
    }

    TG_DEBUG("Filtered sensor list: ");
    for (int i=0; i<filterCtx->sensorList.count(); i++)
        TG_DEBUG(filterCtx->sensorList.at(i).pos);
}

void UiSessionPreviewPage::on_tableWidget_clicked(const QModelIndex &index)
{
    filterCtx->selectedSensorIndex = index.row();

    TG_DEBUG("Selected filter index = " << filterCtx->selectedSensorIndex);

    emit gotoPage(spLogChartPage);
}


