#ifndef UISESSIONPREVIEWPAGE_H
#define UISESSIONPREVIEWPAGE_H

#include <QWidget>
#include <QModelIndex>
#include "uipagewidget.h"
#include "tnetportal.h"
#include "common.h"

namespace Ui {
class UiSessionPreviewPage;
}

class UiSessionPreviewPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiSessionPreviewPage(QWidget *parent = 0, WidgetPage pageid = spNotSet, TgGateway * gateway = 0, struct FilterContext * ctx = 0, bool degreesInCelsius = true);
    ~UiSessionPreviewPage();

private slots:
    void on_backButton_clicked();
    void on_a1Checkbox_toggled(bool checked);
    void on_a2Checkbox_toggled(bool checked);
    void on_tableWidget_clicked(const QModelIndex &index);


private:
    Ui::UiSessionPreviewPage *ui;
    Session_t session;
    QVector<Sensor_t> sensors;
    struct FilterContext * filterCtx;
    bool degreesCelsius;

    bool loadSession();
    void updateTable();
    void updateFilterList();
};

#endif // UISESSIONPREVIEWPAGE_H
