#include "uitabledatapage.h"
#include "ui_uitabledatapage.h"


UiTableDataPage::UiTableDataPage(QWidget *parent, enum WidgetPage pageid, TgGateway *gateway, struct FilterContext * ctx, bool celcius) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiTableDataPage),
    filterCtx(ctx),
    degreesCelcius(celcius)
{
    ui->setupUi(this);
    ui->menuFrame->hide();
    alarmSearchActive = false;
    scrollUpActive = false;
    scrollDownActive = false;

    threadTimeoutTimer.setInterval(10000);
    QObject::connect(&threadTimeoutTimer, SIGNAL(timeout()), this, SLOT(on_threadTimerTimeout()));

    alarmSearchTimer.setInterval(10);
    QObject::connect(&alarmSearchTimer, SIGNAL(timeout()), this, SLOT(on_alarmSearchTimerTimeout()));

    scrollTimer.setInterval(100);
    QObject::connect(&scrollTimer, SIGNAL(timeout()), this, SLOT(on_scrollTimerTimeout()));

    topRowItem = 0;

#if defined(TNET_OLIMEX)
    viewPortMax = 8;
#elif defined(TNET_DESKTOP)
    viewPortMax = 30;
#endif

    // create widgets for the viewport
    createViewport();

    // load data from logchart view
    load();

    // set sensor details
    if (filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos != 0 &&
            filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos != filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos)
        ui->sensorDetailsLabel->setText(QString("Session: %1\t\tSensor %2: %3\tRef Sensor %4: %5").arg(
                                                              filterCtx->sessionNo).arg(
                                                              filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos).arg(
                                                              filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name).arg(
                                                              filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos).arg(
                                                              filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refName));
    else
        ui->sensorDetailsLabel->setText(QString("Session: %1\t\tSensor %2: %3").arg(
                                                   filterCtx->sessionNo).arg(
                                                   filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos).arg(
                                                   filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name));

    TG_DEBUG("Constructing table data page");
}

UiTableDataPage::~UiTableDataPage()
{
    items.clear();

    delete ui;

    TG_DEBUG("Destructing table data page");
}

void UiTableDataPage::on_menuButton_clicked()
{
    //ui->menuFrame->show();
}

void UiTableDataPage::on_tableDataQueryFinished(bool result, bool alarmsFound)
{
    threadTimeoutTimer.stop();

    if (result) {

        // have active search and we found alarm(s) in page just loaded
        if (alarmSearchActive) {
            if (!alarmsFound) {
                alarmSearchTimer.start(30);
                dloader->deleteLater();
                workerThread->quit();
                dloader = 0;
                workerThread = 0;
                return;
            }
        }

        // get vector points from query and fill the plots
        const QVector<double>& temp = dloader->getTemps();
        const QVector<double>& datetime = dloader->getDt();
        const QVector<double>& a1 = dloader->getA1();
        const QVector<double>& a2 = dloader->getA2();
        //const QVector<double>& refTemp = dloader->getRefTemp();
        const QVector<double>& diffTemp = dloader->getDiffTemp();

        // total points
        totalPoints = datetime.count();

        // clear data container
        tableData.clear();

        // row item widget for each point
        for (int i=0; i<totalPoints; i++) {

            TableItemData_t data;
            data.dtutc = (qint64)datetime.at(i);
            // diff or normal temp
            if (filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos != 0 &&
                    filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos != filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos)
                data.temp = (float)diffTemp.at(i);
            else
                data.temp = (float)temp.at(i);

            data.a1 = (quint16)a1.at(i);
            data.a2 = (quint16)a2.at(i);

            // TODO: do alarm interpretation here to get correct alarm state
            if (data.temp >= (float)data.a2)
                data.alarmState = daCurrentA2;
            else if (data.temp >= (float)data.a1)
                data.alarmState = daCurrentA1;
            else
                data.alarmState = daNone;

            tableData.prepend(data);
        }
    }

    // update viewport widgets
    updateViewport();

    // set dialog result for alarm search
    if (alarmSearchActive) {
        alarmSearchActive = false;
        if (alarmsFound)
            msgDlg->setResult("Found alarms", true);
        else
            msgDlg->setResult("No alarms", false);
    } else
        msgDlg->close();

    dloader->deleteLater();
    workerThread->quit();
    dloader = 0;
    workerThread = 0;

    TG_DEBUG("Query finished");
}

void UiTableDataPage::on_threadTimerTimeout()
{
    threadTimeoutTimer.stop();
    TG_ERROR("Thread timer expired");
    dloader->deleteLater();
    msgDlg->setResult("Fault", false);
}

void UiTableDataPage::on_alarmSearchTimerTimeout()
{
    alarmSearchTimer.stop();
    alarmSearchActive = true;
}

bool UiTableDataPage::load()
{
    const QString& rootPath = gwmodel->getDevicePath();

    TG_DEBUG("Tableview load:");
    TG_DEBUG("  Session = " << filterCtx->sessionNo);
    TG_DEBUG("  File = " << filterCtx->currentDataFile);
    TG_DEBUG("  Sensor info:");
    TG_DEBUG("      Position = " << filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos);
    TG_DEBUG("      Name = " << filterCtx->sensorList.at(filterCtx->selectedSensorIndex).name);
    TG_DEBUG("      Reference position = " << filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos);
    TG_DEBUG("      Reference name = " << filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refName);

    QString filePath(QString(TNET_DEVICE_SESSION_DATA_FILE).arg(rootPath, filterCtx->sessionNo, filterCtx->currentDataFile));
    QFile f(filePath);

    workerThread = new QThread;
    dloader = new DataLoader(0,
                             filterCtx->totalSensors,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).pos,
                             filterCtx->sensorList.at(filterCtx->selectedSensorIndex).refPos,
                             f.fileName(),
                             alarmSearchActive);
    dloader->moveToThread(workerThread);

    QObject::connect(workerThread, SIGNAL(started()), dloader, SLOT(loadData()));
    QObject::connect(dloader, SIGNAL(dataLoaded(bool,bool)), this, SLOT(on_tableDataQueryFinished(bool,bool)));
    QObject::connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));

    msgDlg = new MessageDialog(this,"Loading ...", true);
    msgDlg->show();
    threadTimeoutTimer.start();
    workerThread->start();

    return true;
}


void UiTableDataPage::on_goLogChartButton_clicked()
{
    emit gotoPage(spLogChartPage);
}

void UiTableDataPage::on_alarmSearchButton_clicked()
{

}

void UiTableDataPage::on_scrollUpButton_pressed()
{
    scrollUpActive = true;
    elapsedTimer.start();
    scrollTimer.start();
}

void UiTableDataPage::on_scrollUpButton_released()
{
    scrollUpActive = false;
    scrollTimer.stop();

    if (topRowItem > 0) {
        topRowItem--;
        updateViewport();
    }
}

void UiTableDataPage::on_scrollDownButton_pressed()
{
    scrollDownActive = true;
    elapsedTimer.start();
    scrollTimer.start();
}

void UiTableDataPage::on_scrollDownButton_released()
{
    scrollDownActive = false;
    scrollTimer.stop();

    if (topRowItem + viewPortMax < totalPoints) {
        topRowItem++;
        updateViewport();
    }
}

void UiTableDataPage::on_scrollTimerTimeout()
{
    int scrollAmount;

    if (elapsedTimer.elapsed() < 2000)
        return;

    // less than 10 seconds
    else if (elapsedTimer.elapsed() < 10000)
        scrollAmount = 1;

    // greater than 10 seconds
    else
        scrollAmount = 10;


    if (scrollUpActive){
        // can't scroll viewport beyond bounds of data
        if (topRowItem - scrollAmount >= 0) {
            topRowItem -= scrollAmount;
            updateViewport();
        }
    } else if (scrollDownActive) {
        // can't scroll viewport beyond bounds of data
        if (topRowItem + viewPortMax + scrollAmount < totalPoints) {
            topRowItem += scrollAmount;
            updateViewport();
        }
    }
}

void UiTableDataPage::createViewport()
{
    ui->tableWidget->setColumnCount(1);
    ui->tableWidget->setRowCount(viewPortMax);

    // initially these are empty
    for (int i=0; i<viewPortMax; i++){
        TableDataWidgetItem * item = new TableDataWidgetItem;
        items.append(item);
        ui->tableWidget->setRowHeight(i,40);
        ui->tableWidget->setCellWidget(i,0,item);
    }
}

void UiTableDataPage::updateViewport()
{
    // update viewport
    for (int i=0; i<viewPortMax; i++){
        if (topRowItem + i < totalPoints)
            items[i]->updateData(tableData.at(topRowItem + i), degreesCelcius);
        else
            items[i]->clearData();
    }
}

void UiTableDataPage::on_pageTopButton_clicked()
{
    topRowItem = 0;
    updateViewport();
}

void UiTableDataPage::on_pageBottomButton_clicked()
{
    topRowItem = totalPoints - viewPortMax -1;
    updateViewport();
}

void UiTableDataPage::on_imageButton_clicked()
{

}
