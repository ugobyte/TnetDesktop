#ifndef UITABLEDATAPAGE_H
#define UITABLEDATAPAGE_H

#include <QWidget>
#include <QThread>
#include <QTimer>
#include <QElapsedTimer>
#include <QString>
#include <QFile>
#include <QDir>
#include "uihomepage.h"
#include "uipagewidget.h"
#include "common.h"
#include "tnetportal.h"
#include "messagedialog.h"
#include "tabledatawidgetitem.h"

namespace Ui {
class UiTableDataPage;
}

class UiTableDataPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiTableDataPage(QWidget *parent = 0, WidgetPage pageid = spNotSet, TgGateway * gateway = 0, struct FilterContext *ctx = 0, bool celcius = true);
    ~UiTableDataPage();

private slots:
    void on_menuButton_clicked();

    // table data query finished
    void on_tableDataQueryFinished(bool result, bool alarmsFound);
    void on_threadTimerTimeout();
    void on_alarmSearchTimerTimeout();
    void on_scrollTimerTimeout();

    void on_goLogChartButton_clicked();
    void on_alarmSearchButton_clicked();

    void on_scrollUpButton_pressed();
    void on_scrollDownButton_pressed();
    void on_scrollUpButton_released();
    void on_scrollDownButton_released();

    void on_pageTopButton_clicked();
    void on_pageBottomButton_clicked();

    void on_imageButton_clicked();

private:
    Ui::UiTableDataPage *ui;
    struct FilterContext * filterCtx;
    DataLoader * dloader;
    QThread * workerThread;
    MessageDialog * msgDlg;
    QTimer threadTimeoutTimer;
    QTimer alarmSearchTimer;
    QTimer scrollTimer;
    QElapsedTimer elapsedTimer;
    bool degreesCelcius;
    QVector<TableItemData_t> tableData;
    QVector<TableDataWidgetItem *> items;
    int totalPoints;
    int topRowItem;
    int viewPortMax;
    bool alarmSearchActive;
    bool scrollUpActive;
    bool scrollDownActive;
    int scrollTimeElapsed;

    quint16 sensorPos;
    quint16 sensorRefPos;
    QString currentFile;
    QString currentSession;
    quint16 currentFileIndex;
    QString sensorName;
    QString sensorRefName;
    quint16 totalFiles;

    void createViewport();
    void updateViewport();

    void runQuery();
    bool load();
};

#endif // UITABLEDATAPAGE_H
