#include "uiuseraddeditpage.h"
#include "ui_uiuseraddeditpage.h"

UiUserAddEditPage::UiUserAddEditPage(QWidget *parent,
                                     WidgetPage pageid,
                                     TgGateway *gateway,
                                     QVector<UserInfo_t> *users,
                                     int selectedUserToEdit,
                                     VirtualKeypadDialog *keypad) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiUserAddEditPage),
    vKeypad(keypad),
    usersInfo(users),
    selectUserEdit(selectedUserToEdit)
{
    ui->setupUi(this);

    // add new user
    if (selectUserEdit == -1) {

        // create info struct and initialise it
        UserInfo_t userInfo;
        userInfo.name = "";
        userInfo.email = "";
        userInfo.phone = "";
        userInfo.alerts.a1Alarms = false;
        userInfo.alerts.a2Alarms = false;
        userInfo.alerts.lowBattery = false;
        userInfo.alerts.netChange = false;
        userInfo.alerts.newSession = false;
        userInfo.alerts.powerChange = false;
        userInfo.alerts.powerOff = false;
        userInfo.alerts.powerOn = false;
        userInfo.alerts.restartSession = false;
        userInfo.alerts.resumeSession = false;
        userInfo.alerts.stopSession = false;
        usersInfo->append(userInfo);
        selectUserEdit = usersInfo->count() -1;

        TG_DEBUG("User selected index = " << selectUserEdit);
    }

    TG_DEBUG("A1 alarms "<<usersInfo->at(selectUserEdit).alerts.a1Alarms);
    TG_DEBUG("A2 alarms "<<usersInfo->at(selectUserEdit).alerts.a2Alarms);
    TG_DEBUG("Low battery "<<usersInfo->at(selectUserEdit).alerts.lowBattery);
    TG_DEBUG("Power change "<<usersInfo->at(selectUserEdit).alerts.powerChange);
    TG_DEBUG("Session change "<<usersInfo->at(selectUserEdit).alerts.newSession);

    // update the ui
    ui->nameLineedit->setText(usersInfo->at(selectUserEdit).name);
    ui->emailLineedit->setText(usersInfo->at(selectUserEdit).email);
    ui->mobileLineedit->setText(usersInfo->at(selectUserEdit).phone);
    ui->alarm1Alert->setChecked(usersInfo->at(selectUserEdit).alerts.a1Alarms);
    ui->alarm2Alert->setChecked(usersInfo->at(selectUserEdit).alerts.a2Alarms);
    ui->lowBatteryAlert->setChecked(usersInfo->at(selectUserEdit).alerts.lowBattery);
    ui->powerChangeAlert->setChecked(usersInfo->at(selectUserEdit).alerts.powerChange);
    ui->sessionChangeAlert->setChecked(usersInfo->at(selectUserEdit).alerts.newSession);

    TG_DEBUG("Constructing user add/edit page");
}

UiUserAddEditPage::~UiUserAddEditPage()
{
    delete ui;

    TG_DEBUG("Destructing user add/edit page");
}

void UiUserAddEditPage::on_backButton_clicked()
{
    gotoPage(spNotificationPage);
}

void UiUserAddEditPage::on_nameLineedit_textChanged(const QString &arg1)
{
    UserInfo_t& user = usersInfo->operator [](selectUserEdit);
    user.name = arg1;
}

void UiUserAddEditPage::on_emailLineedit_textChanged(const QString &arg1)
{
    UserInfo_t& user = usersInfo->operator [](selectUserEdit);
    user.email = arg1;
}

void UiUserAddEditPage::on_mobileLineedit_textChanged(const QString &arg1)
{
    UserInfo_t& user = usersInfo->operator [](selectUserEdit);
    user.phone = arg1;
}

void UiUserAddEditPage::on_nameLineedit_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1);

    if (arg2 == 0){
#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif
    }
}

void UiUserAddEditPage::on_emailLineedit_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1);
    Q_UNUSED(arg2);

#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif

}

void UiUserAddEditPage::on_mobileLineedit_cursorPositionChanged(int arg1, int arg2)
{
    Q_UNUSED(arg1);
    Q_UNUSED(arg2);

#if defined(TNET_OLIMEX)
    vKeypad->show();
#endif

}

void UiUserAddEditPage::on_sessionChangeAlert_clicked(bool checked)
{
    UserInfo_t& user = usersInfo->operator [](selectUserEdit);
    user.alerts.newSession = checked;
}

void UiUserAddEditPage::on_lowBatteryAlert_clicked(bool checked)
{
    UserInfo_t& user = usersInfo->operator [](selectUserEdit);
    user.alerts.lowBattery = checked;
}

void UiUserAddEditPage::on_powerChangeAlert_clicked(bool checked)
{
    UserInfo_t& user = usersInfo->operator [](selectUserEdit);
    user.alerts.powerChange = checked;
}

void UiUserAddEditPage::on_alarm1Alert_clicked(bool checked)
{
    UserInfo_t& user = usersInfo->operator [](selectUserEdit);
    user.alerts.a1Alarms = checked;
}

void UiUserAddEditPage::on_alarm2Alert_clicked(bool checked)
{
    UserInfo_t& user = usersInfo->operator [](selectUserEdit);
    user.alerts.a2Alarms = checked;
}
