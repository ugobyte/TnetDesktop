#ifndef UIUSERADDEDITPAGE_H
#define UIUSERADDEDITPAGE_H

#include <QWidget>
#include "uipagewidget.h"
#include "virtualkeypaddialog.h"

namespace Ui {
class UiUserAddEditPage;
}



class UiUserAddEditPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiUserAddEditPage(QWidget *parent = 0,
                               enum WidgetPage pageid = spNotSet,
                               TgGateway * gateway = 0,
                               QVector<UserInfo_t> *users = 0,
                               int selectedUserToEdit = -1,
                               VirtualKeypadDialog * keypad =0);
    ~UiUserAddEditPage();

private slots:

    void on_backButton_clicked();
    void on_nameLineedit_textChanged(const QString &arg1);
    void on_emailLineedit_textChanged(const QString &arg1);
    void on_mobileLineedit_textChanged(const QString &arg1);

    void on_nameLineedit_cursorPositionChanged(int arg1, int arg2);
    void on_emailLineedit_cursorPositionChanged(int arg1, int arg2);
    void on_mobileLineedit_cursorPositionChanged(int arg1, int arg2);

    void on_sessionChangeAlert_clicked(bool checked);
    void on_lowBatteryAlert_clicked(bool checked);
    void on_powerChangeAlert_clicked(bool checked);
    void on_alarm1Alert_clicked(bool checked);
    void on_alarm2Alert_clicked(bool checked);

private:
    Ui::UiUserAddEditPage *ui;
    VirtualKeypadDialog * vKeypad;
    QVector<UserInfo_t> * usersInfo;
    int selectUserEdit;
};

#endif // UIUSERADDEDITPAGE_H
