#include "uiusernotificationpage.h"
#include "ui_uiusernotificationpage.h"
#include "userlistitem.h"

UiUserNotificationPage::UiUserNotificationPage(QWidget *parent, enum WidgetPage pageid, TgGateway * gateway, QVector<UserInfo_t> * users) :
    UiPageWidget(parent,pageid,gateway),
    ui(new Ui::UiUserNotificationPage),
    usersList(users),
    selectedUserItem(-1)
{
    ui->setupUi(this);

    updateTable();

    ui->addUserButton->setEnabled(true);
    if (usersList->count() > 0)
        ui->clearListButton->setEnabled(true);

    TG_DEBUG("Constructing user notification page");
}

UiUserNotificationPage::~UiUserNotificationPage()
{
    delete ui;

    TG_DEBUG("Destructing user notification page");
}

int UiUserNotificationPage::getSelectedUserItem()
{
    return selectedUserItem;
}

void UiUserNotificationPage::on_tableWidget_clicked(const QModelIndex &index)
{
    // save item
    selectedUserItem = index.row();

    // go to usersAddPage
    gotoPage(spUserAddEditPage);
}

void UiUserNotificationPage::on_addUserButton_clicked()
{
    // implies add new so initialise with empty template
    selectedUserItem = -1;

    // go to usersAddPage
    gotoPage(spUserAddEditPage);
}

void UiUserNotificationPage::on_clearListButton_clicked()
{
    // confirm dialog
}

void UiUserNotificationPage::on_undoDeleteButton_clicked()
{

}

void UiUserNotificationPage::on_backButton_clicked()
{
    gotoPage(spGeneralSettingsPage);
}

void UiUserNotificationPage::on_saveChangesButton_clicked()
{
    ui->saveChangesButton->setEnabled(false);
    // attach signal/slot for when update result returns
    QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_userUpdateResult(bool,QString)));

    // send request
    gwmodel->requestUpdateUsers(usersList);

    // start spinner dialog
    msgDlg = new MessageDialog(this, "Updating users ...", true);
    msgDlg->show();
}

void UiUserNotificationPage::on_userUpdateResult(bool result, QString repr)
{
    Q_UNUSED(repr);
    msgDlg->close();

    if (result) {
        msgDlg = new MessageDialog(this, "", false);
        msgDlg->setResult("Success", true);
        msgDlg->show();
        TG_DEBUG("Update users success");
    } else {
        msgDlg = new MessageDialog(this, "", false);
        msgDlg->setResult("Failed", false);
        msgDlg->show();
        TG_WARNING("Update users failed");
    }

    ui->saveChangesButton->setEnabled(true);
}

void UiUserNotificationPage::on_userDeleted(QString userName)
{
    QVector<UserInfo_t>::iterator it = usersList->begin();
    while (it != usersList->end()) {
        if (it->name == userName){
            TG_DEBUG("Removing " << it->name << " from user list");
            it = usersList->erase(it);
        } else
            it++;
    }

    TG_DEBUG("User list size = " << usersList->count());

    updateTable();
}

void UiUserNotificationPage::updateTable()
{
    ui->tableWidget->clear();
    ui->tableWidget->setColumnCount(1);
    ui->tableWidget->setRowCount(usersList->count());

    for (int i=0; i<usersList->count(); i++){
        UserListItem * userItem = new UserListItem(0,usersList->at(i).name);
        ui->tableWidget->setCellWidget(i,0,userItem);
        QObject::connect(userItem, SIGNAL(on_deleteUser(QString)), this, SLOT(on_userDeleted(QString)));
        ui->tableWidget->setRowHeight(i,40);
    }
}
