#ifndef UIUSERNOTIFICATIONPAGE_H
#define UIUSERNOTIFICATIONPAGE_H

#include <QWidget>
#include <QModelIndex>
#include "uipagewidget.h"
#include "messagedialog.h"

namespace Ui {
class UiUserNotificationPage;
}

class UiUserNotificationPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiUserNotificationPage(QWidget *parent = 0, WidgetPage pageid = spNotSet, TgGateway *gateway = 0, QVector<UserInfo_t> * users = 0);
    ~UiUserNotificationPage();

    int getSelectedUserItem();

private slots:
    void on_tableWidget_clicked(const QModelIndex &index);
    void on_addUserButton_clicked();
    void on_clearListButton_clicked();
    void on_undoDeleteButton_clicked();
    void on_backButton_clicked();
    void on_saveChangesButton_clicked();

    void on_userUpdateResult(bool result, QString repr);
    void on_userDeleted(QString userName);

private:
    Ui::UiUserNotificationPage *ui;
    QVector<UserInfo_t> * usersList;
    int selectedUserItem;
    MessageDialog * msgDlg;
    void updateTable();
};

#endif // UIUSERNOTIFICATIONPAGE_H
