#include "uiwifipage.h"
#include "ui_uiwifipage.h"
#include "common.h"
#include "wifinetworklistitem.h"


UiWifiPage::UiWifiPage(QWidget *parent, WidgetPage pageid, TgGateway *gateway, VirtualKeypadDialog * keypad) :
    UiPageWidget(parent, pageid, gateway),
    ui(new Ui::UiWifiPage),
    msgDlg(0),
    wifiDlg(0),
    vkeypad(keypad)
{
    ui->setupUi(this);

    QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_wifiScanResult(bool,QString)));
    QObject::connect(gwmodel, SIGNAL(requestResult(bool,QString)), this, SLOT(on_wifiConnectResult(bool,QString)));

    // do scan
    if (!gwmodel->isWifiScanInProgress()) {
        gwmodel->requestWifiScan();
        msgDlg = new MessageDialog(this, "Scanning ...", true);
        msgDlg->show();
        ui->wifiScanButton->setEnabled(false);
    }

    TG_DEBUG("Constructing wifi page");
}


UiWifiPage::~UiWifiPage()
{
    delete ui;

    TG_DEBUG("Destructing wifi page");
}


void UiWifiPage::on_backButton_clicked()
{
    gotoPage(spConnectPage);
}

void UiWifiPage::on_wifiConnectResult(bool result, QString repr)
{
    // hide loader
    msgDlg->close();

    // if success show confirm with success
    if(result) {
        msgDlg = new MessageDialog(this, "Connected", false);
        msgDlg->setResult("Connected", true);
        msgDlg->show();
        TG_INFO("Wifi connect success ipaddress = " << repr);

        updateTable();

    } else {
        msgDlg = new MessageDialog(this, "Failed", false);
        msgDlg->setResult("Connect failed", false);
        msgDlg->show();
        TG_WARNING("Wifi connect failed");
    }
}

void UiWifiPage::on_wifiDisconnectResult(bool result, QString repr)
{
    // hide loader
    msgDlg->close();

    // if success show confirm with success
    if(result) {
        msgDlg = new MessageDialog(this, "Disconnected", false);
        msgDlg->setResult("Disconnected", true);
        msgDlg->show();
        TG_INFO("Wifi connect success ipaddress = " << repr);

        updateTable();

    } else {
        msgDlg = new MessageDialog(this, "Connected", false);
        msgDlg->setResult("Connect failed", false);
        msgDlg->show();
        TG_WARNING("Wifi connect failed");
    }
}

void UiWifiPage::on_wifiOk(const QString ssid, const QString psk, const WifiActionContext ctx)
{
    if (ctx == waConnect) {
        gwmodel->connectWifiNetwork(ssid, psk);
        wifiFormClose();
        msgDlg = new MessageDialog(this, "Joining ...", true);
        msgDlg->show();

    }  else if (ctx == waDisconnect) {

        gwmodel->disconnectWifiNetwork(ssid);
        wifiFormClose();
        msgDlg = new MessageDialog(this, "Disconnecting ...", true);
        msgDlg->show();
    }
}


void UiWifiPage::on_wifiCancel()
{
#if defined(TNET_OLIMEX)
    vkeypad->hide();
#endif
}

void UiWifiPage::on_wifiScanResult(bool result, QString repr)
{
    TG_DEBUG("Wifi repr " << repr);
    msgDlg->close();
    ui->wifiScanButton->setEnabled(true);

    if (!result) {
        TG_WARNING("Wifi scan result failed");
        msgDlg = new MessageDialog(this, "Scan failed", false);
        msgDlg->setResult("Scan failed", false);
        msgDlg->show();
        return;
    }

    updateTable();
}


void UiWifiPage::wifiFormOpen(const QString &ssid, const WifiActionContext ctx)
{
    wifiDlg = new WifiDialog(this, ssid, ctx);
    QObject::connect(wifiDlg, SIGNAL(onOk(QString,QString,WifiActionContext)), this, SLOT(on_wifiOk(QString,QString,WifiActionContext)));
    QObject::connect(wifiDlg, SIGNAL(onCancel()), this, SLOT(on_wifiCancel()));
    wifiDlg->show();
#if defined(TNET_OLIMEX)
    vkeypad->show();
#endif

}

void UiWifiPage::wifiFormClose()
{
#if defined(TNET_OLIMEX)
    vkeypad->hide();
#endif
    wifiDlg->close();
}

void UiWifiPage::updateTable()
{
    ui->tableWidget->clear();
    ui->tableWidget->setColumnCount(1);

    const QVector<WifiNetwork_t>& wifiNetworks = gwmodel->getWifiNetworks();
    int total = wifiNetworks.count();
    if (total) {
        const WifiNetwork_t * wifiNetwork;

        ui->tableWidget->setRowCount(total);

        for (int i=0; i<total; i++){
            wifiNetwork = &wifiNetworks.at(i);
            WifiNetworkListItem * w = new WifiNetworkListItem(0,wifiNetwork->ssid, wifiNetwork->signal, wifiNetwork->security);
            ui->tableWidget->setCellWidget(i,0,w);
            ui->tableWidget->setRowHeight(i, 50);
            if (wifiNetwork->state == wsOnline)
                w->setConnectState(true);
            else
                w->setConnectState(false);
        }
    }
}

void UiWifiPage::on_tableWidget_clicked(const QModelIndex &index)
{
    TG_DEBUG("Wifi network table clicked item.row=" << index.row() << " item.col=" << index.column());
    try {
        const QVector<WifiNetwork_t>& wifiNetworks = gwmodel->getWifiNetworks();
        WifiNetwork_t net = wifiNetworks.at(index.row());

        // disconnect of offline ?
        if (net.state == wsOnline)
            wifiFormOpen(net.ssid, waDisconnect);
        // connect if offline ?
        else
            wifiFormOpen(net.ssid, waConnect);

    } catch (std::exception &e) {
        qWarning() << "Widget: Wifi network table click exception e=" << e.what();
    }
}

void UiWifiPage::on_wifiScanButton_clicked()
{
    // do scan
    if (!gwmodel->isWifiScanInProgress()) {
        gwmodel->requestWifiScan();
        msgDlg = new MessageDialog(this, "Scanning ...", true);
        msgDlg->show();
        ui->wifiScanButton->setEnabled(false);
    }
}
