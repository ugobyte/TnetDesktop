#ifndef UIWIFIPAGE_H
#define UIWIFIPAGE_H

#include <QWidget>
#include <QModelIndex>
#include "uipagewidget.h"
#include "messagedialog.h"
#include "wifidialog.h"
#include "virtualkeypaddialog.h"


namespace Ui {
class UiWifiPage;
}

class UiWifiPage : public UiPageWidget
{
    Q_OBJECT

public:
    explicit UiWifiPage(QWidget *parent = 0, WidgetPage pageid = spNotSet, TgGateway * gateway = 0, VirtualKeypadDialog * keypad = 0);
    ~UiWifiPage();

private slots:

    void on_wifiScanButton_clicked();
    void on_backButton_clicked();

    void on_wifiScanResult(bool result, QString repr);
    void on_wifiConnectResult(bool result, QString repr);
    void on_wifiDisconnectResult(bool result, QString repr);
    void on_wifiOk(const QString ssid, const QString psk, const WifiActionContext ctx);
    void on_wifiCancel();
    void on_tableWidget_clicked(const QModelIndex &index);

private:
    Ui::UiWifiPage *ui;
    MessageDialog * msgDlg;
    WifiDialog * wifiDlg;
    VirtualKeypadDialog * vkeypad;


    void wifiFormOpen(const QString& ssid, const WifiActionContext ctx);
    void wifiFormClose();
    void updateTable();
};

#endif // UIWIFIPAGE_H
