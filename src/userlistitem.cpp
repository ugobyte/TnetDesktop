#include "userlistitem.h"
#include "ui_userlistitem.h"
#include "tnetportal.h"

UserListItem::UserListItem(QWidget *parent, const QString userName) :
    QWidget(parent),
    ui(new Ui::UserListItem),
    username(userName)
{
    ui->setupUi(this);

    ui->userNameLabel->setText(userName);

    TG_DEBUG("Constructing user list item");
}

UserListItem::~UserListItem()
{
    delete ui;

    TG_DEBUG("Destructing user list item");
}

void UserListItem::on_userDeleteButton_clicked()
{
    emit on_deleteUser(username);
}
