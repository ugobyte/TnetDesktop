#ifndef USERLISTITEM_H
#define USERLISTITEM_H

#include <QWidget>

namespace Ui {
class UserListItem;
}

class UserListItem : public QWidget
{
    Q_OBJECT

public:
    explicit UserListItem(QWidget *parent = 0, const QString userName = "");
    ~UserListItem();

private slots:
    void on_userDeleteButton_clicked();

signals:
    void on_deleteUser(QString userName);

private:
    Ui::UserListItem *ui;
    QString username;
};

#endif // USERLISTITEM_H
