#include "virtualkeypaddialog.h"
#include "ui_virtualkeypaddialog.h"
#include <QLineEdit>
#include "tnetportal.h"

VirtualKeypadDialog::VirtualKeypadDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VirtualKeypadDialog),
    shiftActive(false),
    lastFocusedWidget(0)
{
    ui->setupUi(this);
    ui->VirtualKeyboard->setCurrentIndex(0);

    QObject::connect(qApp, SIGNAL(focusChanged(QWidget*,QWidget*)),this, SLOT(saveFocusWidget(QWidget*,QWidget*)));

    TG_DEBUG("Virtual keyboard constructing");
}

VirtualKeypadDialog::~VirtualKeypadDialog()
{
    delete ui;
    TG_DEBUG("Virtual keyboard destructing");
}

/**
 * @brief VirtualKeypadDialog::event
 * @param e
 * @return
 * @details This ensures that focus is always with the input widget we are trying to
 *          input characters with from this virtual keybpad
 */
bool VirtualKeypadDialog::event(QEvent *e)
{
    switch (e->type()) {
        case QEvent::WindowActivate:
            if (lastFocusedWidget){
                TG_DEBUG("Keypad receives focus, saving previous focused widget");
                lastFocusedWidget->activateWindow();
            }
            break;
        default:
            break;
        }

    return QWidget::event(e);
}

void VirtualKeypadDialog::on_vkNum_clicked()
{
    // go to number display
    ui->VirtualKeyboard->setCurrentIndex(vkNumerical);
}

void VirtualKeypadDialog::on_vkAbc_clicked()
{
    ui->VirtualKeyboard->setCurrentIndex(vkAlpha);
}

void VirtualKeypadDialog::on_vkNumExit_clicked()
{
    lastFocusedWidget = NULL;
    emit closingWidget();
    close();
}

/**
 * @brief Widget::on_vkExit_clicked
 */
void VirtualKeypadDialog::on_vkExit_clicked()
{
    lastFocusedWidget = NULL;
    emit closingWidget();
    close();
}

void VirtualKeypadDialog::vkKeyPressed(QString key)
{
    TG_DEBUG("Key pressed " << key);

    if (lastFocusedWidget){
        QLineEdit *lineEdit = qobject_cast<QLineEdit*>(lastFocusedWidget);
        lineEdit->setText(lineEdit->text() + key);
    }


   /* QLineEdit *lineEdit = qobject_cast<QLineEdit*>(QApplication::focusWidget());
    if (lineEdit) {
        lineEdit->setText(lineEdit->text() + key);
    }*/
}

void VirtualKeypadDialog::on_vkLeft_clicked()
{
    TG_DEBUG("Move left");

    QLineEdit *lineEdit = qobject_cast<QLineEdit*>(QApplication::focusWidget());
    if (lineEdit) {
        lineEdit->cursorBackward(false,1);
    }
}

void VirtualKeypadDialog::on_vkRight_clicked()
{
    TG_DEBUG("Move right");

    QLineEdit *lineEdit = qobject_cast<QLineEdit*>(QApplication::focusWidget());
    if (lineEdit) {
        lineEdit->cursorForward(false,1);
    }
}

void VirtualKeypadDialog::on_vkDelete_clicked()
{
    TG_DEBUG("Delete one");

    QLineEdit *lineEdit = qobject_cast<QLineEdit*>(QApplication::focusWidget());
    if (lineEdit) {
        lineEdit->backspace();
    }
}



void VirtualKeypadDialog::on_vkE_clicked()
{
    if (shiftActive)
        vkKeyPressed("E");
    else
        vkKeyPressed("e");
}

void VirtualKeypadDialog::on_vkI_clicked()
{
    if (shiftActive)
        vkKeyPressed("I");
    else
        vkKeyPressed("i");
}

void VirtualKeypadDialog::on_vkO_clicked()
{
    if (shiftActive)
        vkKeyPressed("O");
    else
        vkKeyPressed("o");
}

void VirtualKeypadDialog::on_vkP_clicked()
{
    if (shiftActive)
        vkKeyPressed("P");
    else
        vkKeyPressed("p");
}

void VirtualKeypadDialog::on_vkQ_clicked()
{
    if (shiftActive)
        vkKeyPressed("Q");
    else
        vkKeyPressed("q");
}

void VirtualKeypadDialog::on_vkR_clicked()
{
    if (shiftActive)
        vkKeyPressed("R");
    else
        vkKeyPressed("r");
}

void VirtualKeypadDialog::on_vkT_clicked()
{
    if (shiftActive)
        vkKeyPressed("T");
    else
        vkKeyPressed("t");
}

void VirtualKeypadDialog::on_vkU_clicked()
{
    if (shiftActive)
        vkKeyPressed("U");
    else
        vkKeyPressed("u");
}

void VirtualKeypadDialog::on_vkW_clicked()
{
    if (shiftActive)
        vkKeyPressed("W");
    else
        vkKeyPressed("w");
}

void VirtualKeypadDialog::on_vkY_clicked()
{
    if (shiftActive)
        vkKeyPressed("Y");
    else
        vkKeyPressed("y");
}

void VirtualKeypadDialog::on_vkA_clicked()
{
    if (shiftActive)
        vkKeyPressed("A");
    else
        vkKeyPressed("a");
}

void VirtualKeypadDialog::on_vkD_clicked()
{
    if (shiftActive)
        vkKeyPressed("D");
    else
        vkKeyPressed("d");
}

void VirtualKeypadDialog::on_vkF_clicked()
{
    if (shiftActive)
        vkKeyPressed("F");
    else
        vkKeyPressed("f");
}

void VirtualKeypadDialog::on_vkG_clicked()
{
    if (shiftActive)
        vkKeyPressed("G");
    else
        vkKeyPressed("g");
}

void VirtualKeypadDialog::on_vkH_clicked()
{
    if (shiftActive)
        vkKeyPressed("H");
    else
        vkKeyPressed("h");
}

void VirtualKeypadDialog::on_vkJ_clicked()
{
    if (shiftActive)
        vkKeyPressed("J");
    else
        vkKeyPressed("j");
}

void VirtualKeypadDialog::on_vkK_clicked()
{
    if (shiftActive)
        vkKeyPressed("K");
    else
        vkKeyPressed("k");
}

void VirtualKeypadDialog::on_vkL_clicked()
{
    if (shiftActive)
        vkKeyPressed("L");
    else
        vkKeyPressed("l");
}

void VirtualKeypadDialog::on_vkS_clicked()
{
    if (shiftActive)
        vkKeyPressed("S");
    else
        vkKeyPressed("s");
}

void VirtualKeypadDialog::on_vkB_clicked()
{
    if (shiftActive)
        vkKeyPressed("B");
    else
        vkKeyPressed("b");
}

void VirtualKeypadDialog::on_vkC_clicked()
{
    if (shiftActive)
        vkKeyPressed("C");
    else
        vkKeyPressed("c");
}

void VirtualKeypadDialog::on_vkComma_clicked()
{
    vkKeyPressed(",");
}

void VirtualKeypadDialog::on_vkDot_clicked()
{
    vkKeyPressed(".");
}

void VirtualKeypadDialog::on_vkM_clicked()
{
    if (shiftActive)
        vkKeyPressed("M");
    else
        vkKeyPressed("m");
}

void VirtualKeypadDialog::on_vkN_clicked()
{
    if (shiftActive)
        vkKeyPressed("N");
    else
        vkKeyPressed("n");
}

void VirtualKeypadDialog::on_vkV_clicked()
{
    if (shiftActive)
        vkKeyPressed("V");
    else
        vkKeyPressed("v");
}

void VirtualKeypadDialog::on_vkX_clicked()
{
    if (shiftActive)
        vkKeyPressed("X");
    else
        vkKeyPressed("x");
}

void VirtualKeypadDialog::on_vkZ_clicked()
{
    if (shiftActive)
        vkKeyPressed("Z");
    else
        vkKeyPressed("z");
}

void VirtualKeypadDialog::on_vkSpace_clicked()
{
    vkKeyPressed(" ");
}

void VirtualKeypadDialog::on_vkHash_clicked()
{
    vkKeyPressed("#");
}

void VirtualKeypadDialog::on_vk1_clicked()
{
    vkKeyPressed("1");
}

void VirtualKeypadDialog::on_vk2_clicked()
{
    vkKeyPressed("2");
}

void VirtualKeypadDialog::on_vk3_clicked()
{
    vkKeyPressed("3");
}

void VirtualKeypadDialog::on_vk4_clicked()
{
    vkKeyPressed("4");
}

void VirtualKeypadDialog::on_vk5_clicked()
{
    vkKeyPressed("5");
}

void VirtualKeypadDialog::on_vk6_clicked()
{
    vkKeyPressed("6");
}

void VirtualKeypadDialog::on_vk7_clicked()
{
    vkKeyPressed("7");
}

void VirtualKeypadDialog::on_vk8_clicked()
{
    vkKeyPressed("8");
}

void VirtualKeypadDialog::on_vk9_clicked()
{
    vkKeyPressed("9");
}

void VirtualKeypadDialog::on_vk0_clicked()
{
    vkKeyPressed("0");
}

void VirtualKeypadDialog::on_vkMinus_clicked()
{
    vkKeyPressed("-");
}

void VirtualKeypadDialog::on_vkForwardSlash_clicked()
{
    vkKeyPressed("/");
}

void VirtualKeypadDialog::on_vkColon_clicked()
{
    vkKeyPressed(":");
}

void VirtualKeypadDialog::on_vkSemiColon_clicked()
{
    vkKeyPressed(";");
}

void VirtualKeypadDialog::on_vkOpenBracket_clicked()
{
    vkKeyPressed("(");
}

void VirtualKeypadDialog::on_vkCloseBracket_clicked()
{
    vkKeyPressed(")");
}

void VirtualKeypadDialog::on_vkDollar_clicked()
{
    vkKeyPressed("$");
}

void VirtualKeypadDialog::on_vkAmpersand_clicked()
{
    vkKeyPressed("&");
}

void VirtualKeypadDialog::on_vkAt_clicked()
{
    vkKeyPressed("@");
}

void VirtualKeypadDialog::on_vkQuote_clicked()
{
    vkKeyPressed("\"");
}

void VirtualKeypadDialog::on_vkSingleApostrophe_clicked()
{
    vkKeyPressed("'");
}

void VirtualKeypadDialog::on_vkBackSlash_clicked()
{
    vkKeyPressed("\\");
}

void VirtualKeypadDialog::on_vkQuestion_clicked()
{
    vkKeyPressed("?");
}

void VirtualKeypadDialog::on_vkExclamation_clicked()
{
    vkKeyPressed("!");
}

void VirtualKeypadDialog::on_vkTilda_clicked()
{
    vkKeyPressed("~");
}

void VirtualKeypadDialog::on_vkPercentage_clicked()
{
    vkKeyPressed("%");
}

void VirtualKeypadDialog::on_vkStar_clicked()
{
    vkKeyPressed("*");
}

void VirtualKeypadDialog::on_vkEqual_clicked()
{
    vkKeyPressed("=");
}

void VirtualKeypadDialog::on_vkPlus_clicked()
{
    vkKeyPressed("+");
}

void VirtualKeypadDialog::on_vkOpenCurlyBracket_2_clicked()
{
    vkKeyPressed("{");
}

void VirtualKeypadDialog::on_vkCloseCurlyBracket_clicked()
{
    vkKeyPressed("}");
}

void VirtualKeypadDialog::on_vkOpenSquareBracket_clicked()
{
    vkKeyPressed("[");
}

void VirtualKeypadDialog::on_vkCloseSquareBracket_clicked()
{
    vkKeyPressed("]");
}

void VirtualKeypadDialog::on_vkUnderscore_clicked()
{
    vkKeyPressed("_");
}

void VirtualKeypadDialog::on_vkPipe_clicked()
{
    vkKeyPressed("|");
}

void VirtualKeypadDialog::on_vkCarot_clicked()
{
    vkKeyPressed("^");
}

void VirtualKeypadDialog::on_vkApostrophe_clicked()
{
    vkKeyPressed("`");
}

void VirtualKeypadDialog::on_vkShift_clicked()
{
    if (shiftActive) {
       ui->vkQ->setText("q");
       ui->vkW->setText("w");
       ui->vkE->setText("e");
       ui->vkR->setText("r");
       ui->vkT->setText("t");
       ui->vkY->setText("y");
       ui->vkU->setText("u");
       ui->vkI->setText("i");
       ui->vkO->setText("o");
       ui->vkP->setText("p");
       ui->vkA->setText("a");
       ui->vkS->setText("s");
       ui->vkD->setText("d");
       ui->vkF->setText("f");
       ui->vkG->setText("g");
       ui->vkH->setText("h");
       ui->vkJ->setText("j");
       ui->vkK->setText("k");
       ui->vkL->setText("l");
       ui->vkZ->setText("z");
       ui->vkX->setText("x");
       ui->vkC->setText("c");
       ui->vkV->setText("v");
       ui->vkB->setText("b");
       ui->vkN->setText("n");
       ui->vkM->setText("m");
       shiftActive = false;
    } else {
        ui->vkQ->setText("Q");
        ui->vkW->setText("W");
        ui->vkE->setText("E");
        ui->vkR->setText("R");
        ui->vkT->setText("T");
        ui->vkY->setText("Y");
        ui->vkU->setText("U");
        ui->vkI->setText("I");
        ui->vkO->setText("O");
        ui->vkP->setText("P");
        ui->vkA->setText("A");
        ui->vkS->setText("S");
        ui->vkD->setText("D");
        ui->vkF->setText("F");
        ui->vkG->setText("G");
        ui->vkH->setText("H");
        ui->vkJ->setText("J");
        ui->vkK->setText("K");
        ui->vkL->setText("L");
        ui->vkZ->setText("Z");
        ui->vkX->setText("X");
        ui->vkC->setText("C");
        ui->vkV->setText("V");
        ui->vkB->setText("B");
        ui->vkN->setText("N");
        ui->vkM->setText("M");
        shiftActive = true;
    }
}

/**
 * @brief VirtualKeypadDialog::saveFocusWidget
 * @param oldFocus
 * @param newFocus
 * @details Save last widget that had focus
 */
void VirtualKeypadDialog::saveFocusWidget(QWidget *oldFocus, QWidget *newFocus)
{
    if (newFocus != 0 && !this->isAncestorOf(newFocus)) {
        lastFocusedWidget = newFocus;
    }
}

void VirtualKeypadDialog::on_vkNumBack_clicked()
{
    TG_DEBUG("Num delete one");
    QLineEdit *lineEdit = qobject_cast<QLineEdit*>(QApplication::focusWidget());
    if (lineEdit) {
        lineEdit->backspace();
    }
}

void VirtualKeypadDialog::on_vkNumLeft_clicked()
{
    TG_DEBUG("Num move left");

    QLineEdit *lineEdit = qobject_cast<QLineEdit*>(QApplication::focusWidget());
    if (lineEdit) {
        lineEdit->cursorBackward(false,1);
    }
}

void VirtualKeypadDialog::on_vkNumRight_clicked()
{
    TG_DEBUG("Num move right");

    QLineEdit *lineEdit = qobject_cast<QLineEdit*>(QApplication::focusWidget());
    if (lineEdit) {
        lineEdit->cursorForward(false,1);
    }
}
