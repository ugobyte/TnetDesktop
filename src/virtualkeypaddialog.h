#ifndef VIRTUALKEYPADDIALOG_H
#define VIRTUALKEYPADDIALOG_H

#include <QWidget>
#include <QKeyEvent>
#include <QEvent>

enum VkStack {vkAlpha=0, vkNumerical};


namespace Ui {
class VirtualKeypadDialog;
}

class VirtualKeypadDialog : public QWidget
{
    Q_OBJECT

public:
    explicit VirtualKeypadDialog(QWidget *parent = 0);
    ~VirtualKeypadDialog();

   // void setFocusedWidget(QWidget * focusedWidget);

protected:
    bool event(QEvent * e);

signals:
    void closingWidget();

private slots:
    void on_vkNum_clicked();
    void on_vkAbc_clicked();
    void on_vkNumExit_clicked();
    void on_vkExit_clicked();

    void on_vkE_clicked();
    void on_vkI_clicked();
    void on_vkO_clicked();
    void on_vkP_clicked();
    void on_vkQ_clicked();
    void on_vkR_clicked();
    void on_vkT_clicked();
    void on_vkU_clicked();
    void on_vkW_clicked();
    void on_vkY_clicked();

    void on_vkA_clicked();
    void on_vkD_clicked();
    void on_vkF_clicked();
    void on_vkG_clicked();
    void on_vkH_clicked();
    void on_vkJ_clicked();
    void on_vkK_clicked();
    void on_vkL_clicked();
    void on_vkS_clicked();

    void on_vkB_clicked();
    void on_vkC_clicked();
    void on_vkComma_clicked();
    void on_vkDot_clicked();
    void on_vkM_clicked();
    void on_vkN_clicked();
    void on_vkV_clicked();
    void on_vkX_clicked();
    void on_vkZ_clicked();

    void on_vkHash_clicked();
    void on_vk1_clicked();
    void on_vk2_clicked();
    void on_vk3_clicked();
    void on_vk4_clicked();
    void on_vk5_clicked();
    void on_vk6_clicked();
    void on_vk7_clicked();
    void on_vk8_clicked();
    void on_vk9_clicked();
    void on_vk0_clicked();
    void on_vkMinus_clicked();
    void on_vkForwardSlash_clicked();
    void on_vkColon_clicked();
    void on_vkSemiColon_clicked();
    void on_vkOpenBracket_clicked();
    void on_vkCloseBracket_clicked();
    void on_vkDollar_clicked();
    void on_vkAmpersand_clicked();
    void on_vkAt_clicked();
    void on_vkQuote_clicked();
    void on_vkSingleApostrophe_clicked();
    void on_vkBackSlash_clicked();
    void on_vkQuestion_clicked();
    void on_vkExclamation_clicked();
    void on_vkTilda_clicked();
    void on_vkPercentage_clicked();
    void on_vkStar_clicked();
    void on_vkEqual_clicked();
    void on_vkPlus_clicked();
    void on_vkOpenCurlyBracket_2_clicked();
    void on_vkCloseCurlyBracket_clicked();
    void on_vkOpenSquareBracket_clicked();
    void on_vkCloseSquareBracket_clicked();
    void on_vkUnderscore_clicked();
    void on_vkPipe_clicked();
    void on_vkCarot_clicked();
    void on_vkApostrophe_clicked();
    void on_vkSpace_clicked();
    void on_vkLeft_clicked();
    void on_vkRight_clicked();
    void on_vkDelete_clicked();
    void on_vkShift_clicked();


    void saveFocusWidget(QWidget *oldFocus, QWidget *newFocus);

    void on_vkNumBack_clicked();

    void on_vkNumLeft_clicked();

    void on_vkNumRight_clicked();

private:
    void vkKeyPressed(QString key);

private:
    Ui::VirtualKeypadDialog *ui;
    bool shiftActive;
    QWidget *lastFocusedWidget;


};


#endif // VIRTUALKEYPADDIALOG_H
