#include "wifidialog.h"
#include "ui_wifidialog.h"
#include "tnetportal.h"

/**
 * @brief WifiDialog::WifiDialog
 * @param parent
 */
WifiDialog::WifiDialog(QWidget *parent, const QString ssid, const WifiActionContext ctx) :
    QWidget(parent),
    ui(new Ui::WifiDialog),
    waCtx(ctx)
{
    ui->setupUi(this);

    // delete on close
    setAttribute( Qt::WA_DeleteOnClose, true );

    // widget position
    setGeometry(0,0,parent->geometry().width(),parent->geometry().height());

    // selected ssid from wifi list, so pre-fill
    if (ssid.count()) {
        ui->ssidLineEdit->setText(ssid);
        ui->ssidLineEdit->setReadOnly(true);
        ui->pskLineEdit->setFocus(Qt::ActiveWindowFocusReason);

    // ssid not in list, let user enter
    } else {
        ui->ssidLineEdit->clear();
        ui->ssidLineEdit->setFocus(Qt::ActiveWindowFocusReason);
    }

    ui->pskLineEdit->clear();

    // set the ok button to connect or disconnect depending on action
    if (waCtx == waConnect){
        ui->pskLineEdit->setVisible(true);
        ui->wifiOkButton->setText("Connect");
    } else if (waCtx == waDisconnect){
        ui->pskLineEdit->setVisible(false);
        ui->wifiOkButton->setText("Disconnect");
    }

    //ui->wifiOkButton->setEnabled(false);
    TG_DEBUG("Wifi dialog constructing");
}

/**
 * @brief WifiDialog::~WifiDialog
 */
WifiDialog::~WifiDialog()
{
    delete ui;
    TG_DEBUG("Wifi dialog destructing");
}

/**
 * @brief WifiDialog::on_wifiOkButton_clicked
 */
void WifiDialog::on_wifiOkButton_clicked()
{
    emit onOk(ui->ssidLineEdit->text(), ui->pskLineEdit->text(), waCtx);
}

/**
 * @brief WifiDialog::on_wifiCancelButton_clicked
 */
void WifiDialog::on_wifiCancelButton_clicked()
{
    close();
}

/**
 * @brief WifiDialog::on_ssidLineEdit_returnPressed
 */
void WifiDialog::on_ssidLineEdit_returnPressed()
{
    emit onCancel();
    ui->pskLineEdit->setFocus(Qt::OtherFocusReason);
}

/**
 * @brief WifiDialog::on_pskLineEdit_returnPressed
 */
void WifiDialog::on_pskLineEdit_returnPressed()
{
   emit onOk(ui->ssidLineEdit->text(), ui->pskLineEdit->text(), waCtx);
}

