#ifndef WIFIDIALOG_H
#define WIFIDIALOG_H

#include <QWidget>

namespace Ui {
class WifiDialog;
}

enum WifiActionContext {waConnect, waDisconnect, waCancel};

class WifiDialog : public QWidget
{
    Q_OBJECT

public:
    explicit WifiDialog(QWidget *parent = 0, const QString ssid = "", const WifiActionContext ctx = waCancel);
    ~WifiDialog();
signals:
    void onOk(const QString ssid, const QString psk, const WifiActionContext ctx);
    void onCancel();

private slots:
    void on_wifiOkButton_clicked();
    void on_wifiCancelButton_clicked();
    void on_ssidLineEdit_returnPressed();
    void on_pskLineEdit_returnPressed();

private:
    Ui::WifiDialog *ui;
    WifiActionContext waCtx;
};

#endif // WIFIDIALOG_H
