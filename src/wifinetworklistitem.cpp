#include "wifinetworklistitem.h"
#include "ui_wifinetworklistitem.h"
#include "common.h"



WifiNetworkListItem::WifiNetworkListItem(QWidget *parent, QString ssid, int signal, TgWifiSecurity security) :
    QWidget(parent),
    ui(new Ui::WifiNetworkListItem)
{
    ui->setupUi(this);

    ui->ssidLabel->setText(ssid);

    // signal
    if (signal > 50)
        ui->signalImage->setPixmap(QPixmap(ICON_WIFI));
    else if (signal > 40)
        ui->signalImage->setPixmap(QPixmap(ICON_WIFI_MED));
    else
        ui->signalImage->setPixmap(QPixmap(ICON_WIFI_LOW));

    // security
    if (security == wsWpa || security == wsWpa2)
        ui->securityImage->setPixmap(QPixmap(ICON_SECURITY));
    else
        ui->securityImage->clear();

    TG_DEBUG("Wifi list item constructing");
}

WifiNetworkListItem::~WifiNetworkListItem()
{
    delete ui;
    TG_DEBUG("Wifi list item destructing");
}

void WifiNetworkListItem::setConnectState(bool connected)
{
    if (connected)
        ui->connectedImage->setPixmap(QPixmap(ICON_STAT_A0));
    else
        ui->connectedImage->clear();
}

bool WifiNetworkListItem::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        TG_DEBUG("Wifi item pressed" << keyEvent->key());
        return true;
    } else {
        // standard event processing
        return QObject::eventFilter(watched, event);
    }
}
