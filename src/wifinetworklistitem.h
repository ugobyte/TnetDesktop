#ifndef WIFINETWORKLISTITEM_H
#define WIFINETWORKLISTITEM_H

#include <QWidget>
#include "tnetportal.h"

namespace Ui {
class WifiNetworkListItem;
}

class WifiNetworkListItem : public QWidget
{
    Q_OBJECT


protected:
    bool eventFilter(QObject *watched, QEvent *event);

public:
    explicit WifiNetworkListItem(QWidget *parent = 0, QString ssid = "", int signal = 0, TgWifiSecurity security = wsNone);
    ~WifiNetworkListItem();

    void setConnectState(bool connected);

private:
    Ui::WifiNetworkListItem *ui;
};

#endif // WIFINETWORKLISTITEM_H
